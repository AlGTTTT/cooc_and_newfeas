#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include "image.h"
//#include "median_filter.h"

#include <dirent.h>

//#include "Cooc_Multifrac_Lacunar_Optimizer_FunctOnly.h"
#include "Cooc_And_NewFeas_FunctOnly.h"

//#define USING_HISTOGRAM_EQUALIZATION

//#define PRINT_HISTOGRAM_STATISTICS
//#define PRINT_ORIGINAL_FEAS

#define INCLUDE_OneBest_SeparableRatioOf_2Feas 

#define PRINT_BEST_FEAS

///////////////////////////////////////////////////////////
#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)
#define RETURN_BY_THE_SAME_INTENSITY (-3)

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define MASK_OUTPUT
//	#define COMMENT_OUT_ALL_PRINTS

#define nLenMax_La (2048) //2048
#define nWidMax_La (2048) //2048 

#define nLenMin_La 50 
#define nWidMin_La 50

//#define nImageAreaMax (nLenMax_La*nWidMax_La)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535
//////////////////////////////////////////////////////

	#define COMMENT_OUT_ALL_PRINTS

#define nLenMax 6000
#define nWidMax 6000

#define nLenMin 50
#define nWidMin 50

#define nImageAreaMax (nLenMax*nWidMax)

#define nNumVecsForPositionsOfMeaningfulFeasFinal 3

#define CoefOfIncreaseInRatiosOf_2feasFromAnArr 1.1
#define fRatioBest_ByRatioOf_2FeasMin_ToPrint (-1.6)

#define nNumOfPairCurOfInterest 276453
//////////////////////////////////////////////////////
//#define nLenSubimageToPrintMin  0 //342
//#define nLenSubimageToPrintMax  4000  //2500

//#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470

//#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970

///////////////////////////////////////////////////////////////////////
//#define nIntensityStatMin 10 //85
//#define nIntensityStatMinForEqualization 0 //85
/////////////////////////////////////////////////
#define nDistToCoocPointsMin 1
//#define nDistToCoocPointsMax 2 //test
#define nDistToCoocPointsMax 4
//#define nDistToCoocPointsMax 5
//#define nDistToCoocPointsMax 7
//#define nDistToCoocPointsMax 8
//#define nDistToCoocPointsMax 10
//#define nDistToCoocPointsMax 15

#define nNumOfGrayLevelIntensities 256 // (nGrayLevelIntensityMax + 1)

#define nCoocSizeMax (nNumOfGrayLevelIntensities * nNumOfGrayLevelIntensities)

#define nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot (2*nCoocSizeMax*((nDistToCoocPointsMin + nDistToCoocPointsMax)*( nDistToCoocPointsMax - nDistToCoocPointsMin + 1)))
// == (4*nCoocSizeMax*((nDistToCoocPointsMin + nDistToCoocPointsMax)*( nDistToCoocPointsMax - nDistToCoocPointsMin + 1))/2)

#define nDim (nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot)

#define nIntensityStatForReadMax 65535

#define nIntensityStatBlack 0 //85
#define nIntensityStatMax 255

#define nIntensityStatWhite (nIntensityStatMax)
/////////////////////////////////////////////////////////////
#define nPositionOf_TotFeaToPrint (-1)

//#define nThresholdForIntensitiesMin 20 //5 //1 //110
//#define nThresholdForIntensitiesMin 70 //150 // all colors

//#define nThresholdForIntensitiesMax 255

#define fWeightOfRed_InStr 1.0 // 0.0
#define fWeightOfGreen_InStr 1.0
#define fWeightOfBlue_InStr 1.0 //0.0 //

/////////////////////////////////////////////////////
//test
//for Victor's test image -- 5 copies
//#define nNumOfVecs_Normal 5 

//#define nNumOfVecs_Normal 60  // for D:\Imago\Images\spleen\Anna_March11\RawImages\test\normal
//#define nNumOfVecs_Malignant 15 // for D:\Imago\Images\spleen\Anna_March11\RawImages\test\malignant

//#define nNumOfVecs_Normal 397 //155  // for D:\Imago\Images\spleen\Normal_Tot
//#define nNumOfVecs_Malignant 76 //60 // for D:\Imago\Images\spleen\Malignant_Tot

//#define nNumOfVecs_Normal 10 // for D:\Imago\Images\spleen\Normal_Tot_Small
//#define nNumOfVecs_Malignant 10 // for D:\Imago\Images\spleen\Malignant_Tot_Small

///////////////////////////////
//#define nNumOfVecs_Normal 20 // for D:\Imago\Images\spleen\Normals_TestSet

//#define nNumOfVecs_Normal 3 // fD:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\train\\normal\\3CopiesOfOneNormal\\
//#define nNumOfVecs_Normal 3 // for D:\Imago\Images\spleen\VictorsWholeImages
//#define nNumOfVecs_Malignant 20 // for D:\Imago\Images\spleen\Malignants_TestSet

/////////////////////////////////////////////////////////////////////
//train
//#define nNumOfVecs_Normal 313 //155  // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Normal\\AOI\\

//benign as nornmal
//#define nNumOfVecs_Normal 477 // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\

//benign as nornmal
#define nNumOfVecs_Normal 472 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\

//#define nNumOfVecs_Normal 473 //after removing AoIs with dim > nDim_2pN_Max_La == 2048

//#define nNumOfVecs_Normal 50 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\

//#define nNumOfVecs_Normal 2 ////D:\Imago\Images\spleen\Genas_Images_June26\Benign\AoI_2_Test

//#define nNumOfVecs_Normal 501 //D:\Imago\Images\WoodyBreast\AOIs\Normal-Panoramic
//////////////////////////////
//#define nNumOfVecs_Malignant 576 //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

#define nNumOfVecs_Malignant 575 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

//#define nNumOfVecs_Malignant 50 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

//#define nNumOfVecs_Malignant 298 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 //D:\Imago\Images\WoodyBreast\AOIs\WB-Panoramic

////////////////////////////////////////////////////////////
//watch for '#define HISTOGRAM_EQUALIZATION_APPLIED'


//////////////////////////////////////////////////////////////
//#define nNumOfSelectedFea 1
//#define nNumOfSelectedFea 1100 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
//#define nNumOfSelectedFea 1102 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelectedFea (-1) //2247 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
//////////////////////////////////////////////////////

//#define nLenSubimageToPrintMin  0 //342 
//#define nLenSubimageToPrintMax  4000  //2500

//#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470 

//#define nOneWidToPrint  226 //56 //371 //2476 //
//#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970 

///////////////////////////////////////////////////////////////////////////////////////
//object

const int nGrayLevel_HighIntensityThreshold = 190;

///////////////////////////////////////////////////////////////////
//histograms
	//#define HISTOGRAM_EQUALIZATION_APPLIED

#define nIntensityOfBackground 255 // or 0 --check it up

//#define nNumOfHistogramIntervals_ForCooc 16
#define nNumOfHistogramIntervals_ForCooc 64
///////////////////////////////////////////////////

//#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

#define nIntensityStatForReadMax 65535

//#define nNumOfHistogramBinsStat (nIntensityStatMax) //(nIntensityStatMax - nIntensityStatMin + 1)
#define nNumOfHistogramBinsStat (nIntensityStatMax + 1) //(nIntensityStatMax - nIntensityStatMin + 1)

//#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax
//#define fHueUndefinedValue (-1.0)
//////////////////////////////////////

//#define nIntensityOfBackground 254
//////////////////////////////////////////////

typedef struct
{
	float 
		fWeightOfRed; //<= 1.0
	float
		fWeightOfGreen; //<= 1.0
	float
		fWeightOfBlue; //<= 1.0

} WEIGHTES_OF_RGB_COLORS;

/////////////////////////////////////////////////////////

typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nIntensityOfBackground_Red; //
	int nIntensityOfBackground_Green; //
	int nIntensityOfBackground_Blue; //

	int nWidth;
	int nLength;

	int *nLenObjectBoundary_Arr;

	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

	int *nIsAPixelBackground_Arr;

} COLOR_IMAGE;

////////////////////////////////////////////////////////////////
//cooc feas
typedef struct
{
	int nIndicOfClass;

	int nLength; // nLenCur; //<= nLenMax
	int nWidth; // nWidCur; //<= nWidMax

	//int nPixelArr[nImageAreaMax];
	int *nPixelArr; //Grayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = (nRedf + nGreenf + nBluef)/3;

} GRAYSCALE_IMAGE;

///////////////////////////////

typedef struct
{
	int nIndicOfClass;

	int nLenShift;
	int nWidShift;

	////////////////////////////////////////////////
	//Output
	float fCoocOfImageByLenWidShift_Arr[nCoocSizeMax];

} COOC_OF_IMAGE_BY_LEN_WID_SHIFT;

//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 1 + (nNumOfDirecAndDistAndScaleTot * 11) + (nNumOfScalesAndDistTot*11) )
//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (4 + nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (nNumOfDirecAndDistAndScaleTot * 10) + (nNumOfScalesAndDistTot*10) )

//
/////////////////////////////////////////////////////////////





