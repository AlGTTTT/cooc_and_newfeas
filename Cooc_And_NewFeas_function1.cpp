
//delete 'return SUCCESSFUL_RETURN;' after //premature exit for testing!

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
//#include "Multifractal_SelectedFeas_2.h"
#include "Cooc_And_NewFeas1.h"

using namespace imago;

FILE *fout_lr;
FILE *fout_PrintFeatures;

int
	nNumOfOneFea_Spectrum_AdjustedGlob = -1,
		nNumOfSpectrumFea_Glob = -1,

	nNumOfPairCur_Glob,
	iFea_Glob,

	nNumfPixelsWithIntensityLessThanZero_AfterConvolutionInOneDirection_Glob = 0,
	nNumfPixelsWithIntensityMoreThan255_AfterConvolutionInOneDirection_Glob = 0,

	iIntensity_Interval_1_Glob,
	iIntensity_Interval_2_Glob;

float
	fPercentageOfPixelsWithIntensityOffTheLimits_Glob;

//#include "Cooc_Multifrac_Lacunar_Optimizer_FunctOnly.cpp"
#include "Cooc_And_NewFeas_FunctOnly1.cpp"

////////////////////////////////////////////////

int Reading_AColorImage(
	const Image& image_in,

	COLOR_IMAGE *sColor_Image)
{
	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

#ifdef HISTOGRAM_EQUALIZATION_APPLIED
	int HistogramEqualization_ForColorFormatOfGray(

		//GRAYSCALE_IMAGE *sGrayscale_Image)
		COLOR_IMAGE *sColor_Imagef);
#endif //#ifdef HISTOGRAM_EQUALIZATION_APPLIED


	int
		nRes,
		i,
		j,

		iFeaf,
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		nImageWidth,
		nImageHeight;
	////////////////////////////////////////////////////////////////////////

//initialization
	  // size of image
	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'Reading_AColorImage': nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n  'Reading_AColorImage': nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d > nLenMax = %d  ...", nImageWidth, nLenMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image width: nImageWidth = %d > nLenMax = %d  ...", nImageWidth, nLenMax);
		getchar(); exit(1);

		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "\n\n An error in reading the image height:nImageHeight = %d > nWidMax = %d", nImageHeight, nWidMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image height: nImageHeight = %d > nWidMax = %d...", nImageHeight, nWidMax);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	//Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	//COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		//&sColor_Image); // COLOR_IMAGE *sColor_Imagef);
		sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in'Initializing_Color_To_CurSize':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in'Initializing_Color_To_CurSize':");
		getchar(); exit(1);

		delete[] sColor_Image->nLenObjectBoundary_Arr;
		delete[] sColor_Image->nRed_Arr;
		delete[] sColor_Image->nGreen_Arr;
		delete[] sColor_Image->nBlue_Arr;
		delete[] sColor_Image->nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

	nSizeOfImage = nImageWidth * nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)
/////////////////////////////////////////////////////////////////////////////

//	WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;
	//sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr;
	//sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr;
	//sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr;

	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int Reading_AColorImage(...
////////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = nIntensityStatMax; // -1;
	sColor_Imagef->nIntensityOfBackground_Green = nIntensityStatMax; // -1;
	sColor_Imagef->nIntensityOfBackground_Blue = nIntensityStatMax; // -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
		sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
	{
		return UNSUCCESSFUL_RETURN;
	} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...
/////////////////////////////////////////////////////////////////////////////

float PowerOfAFloatNumber(
	const int nIntOrFloatf, //1 -- int, 0 -- float
	const int nPowerf,
	const float fPowerf,

	const float fFloatInitf) //fFloatInitf != 0.0
{
	int
		iPowf;

	float
		fPowerCurf = fFloatInitf;

	if (nIntOrFloatf == 1)
	{
		if (nPowerf == 0)
		{
			return 0.0; //instead of 1.0
			//return SUCCESSFUL_RETURN; -- should return a float number
		} //
		else if (nPowerf > 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

		}//else if (nPowerf > 0)
		else if (nPowerf < 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

			fPowerCurf = (float)(1.0) / fPowerCurf;
		} //else if (nPowerf < 0)

	}//if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		fPowerCurf = powf(fFloatInitf, fPowerf);

	}//else if (nIntOrFloatf == 0)

	return fPowerCurf;
}//float PowerOfAFloatNumber(
//////////////////////////////////////////////////////////

int Initializing_Grayscale_Image(
	//const int nImageWidthf,
	//const int nImageLengthf,

	const COLOR_IMAGE *sColor_Imagef,
	GRAYSCALE_IMAGE *sGrayscale_Imagef)
{
	int
		nIndexOfPixelCurf,

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,
		iWidf,
		iLenf,

		nRedf,
		nGreenf,
		nBluef,

		nImageSizeCurf = nImageWidthf * nImageLengthf;

	sGrayscale_Imagef->nWidth = nImageWidthf;
	sGrayscale_Imagef->nLength = nImageLengthf;

	//printf("\n Initializing_Grayscale_Image: 1, please press any key"); getchar();

	////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n ///////////////////////////////////////////////////////////////////");
	printf("\n 'Initializing_Grayscale_Image': nImageWidthf = %d, nImageLengthf = %d\n", nImageWidthf, nImageLengthf);

	//fprintf(fout_lr, "\n\n ///////////////////////////////////////////////////////////////////");
	//fprintf(fout_lr, "\n 'Initializing_Grayscale_Image':\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	sGrayscale_Imagef->nPixelArr = new int[nImageSizeCurf];

	if (sGrayscale_Imagef->nPixelArr == nullptr)
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'Initializing_Grayscale_Image'");
		//fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'Initializing_Grayscale_Image'");
		printf("\n\n Please press any key:"); getchar();
		//#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (sGrayscale_Imagef->nPixelArr == nullptr)

	//printf("\n Initializing_Grayscale_Image: 2, please press any key"); getchar();

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//printf("\n iWidf = %d, nImageWidthf = %d", iWidf, nImageWidthf); getchar();
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			//the average of colors
			sGrayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = (nRedf + nGreenf + nBluef) / 3;
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	//printf("\n Initializing_Grayscale_Image: the end, please press any key"); getchar();
	return SUCCESSFUL_RETURN;
}//int Initializing_Grayscale_Image(...
//////////////////////////////////////////////////////////////////////////////

int Histogram_Statistics_ColorActuallyGrayscale_Image(

	//const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageAreaMax]
	const COLOR_IMAGE *sColor_Imagef,

	float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

	float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

	float &fMeanf,
	float &fStDevf,
	float &fSkewnessf,
	float &fKurtosisf)
{
	int
		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nHistogramArrf[nNumOfGrayLevelIntensities], //[nNumOfHistogramBinsStat]

		nHistInBinsArrf[nNumOfHistogramIntervals_ForCooc],
		nAboveThresholds_InHistogramArrf[nNumOfHistogramIntervals_ForCooc - 2], // -1]

		nAreaOfTheWholeImagef,

		//nDenseAreaf = 0,
		//nWidthOfABinInHistogramf = (nNumOfGrayLevelIntensities - nIntensityStatMin) / nNumOfHistogramBinsStat,
		nWidthOfABinInHistogramf = nNumOfGrayLevelIntensities / nNumOfHistogramIntervals_ForCooc, //4

		nNumOfHistogramBinCurf,

		nNumOfValidObjectPixelsf = 0,
		nIntensityCurf,
		nIndexOfPixelf,

		iHistf,
		iHistogramBinf,

		iHistThresholdf,

		nSumOfHistBinsf,
		iLenf,
		iWidf;

	float
		fTempf,
		fVariancef = 0.0,
		fHistogramProbabArrf[nNumOfGrayLevelIntensities];
	/////////////////////////////////////////////////

	nAreaOfTheWholeImagef = nImageWidthf * nImageLengthf;

	if (nAreaOfTheWholeImagef <= 0)
	{
		printf("\n\n An error in 'Histogram_Statistics_ColorActuallyGrayscale_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n An error in 'Histogram_Statistics_ColorActuallyGrayscale_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);
		fflush(fout_lr); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nAreaOfTheWholeImagef <= 0)

	fMeanf = 0.0;
	fStDevf = 0.0;
	fSkewnessf = 0.0;
	fKurtosisf = 0.0;

	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		nHistogramArrf[iHistf] = 0;

	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

	//memset(nHistogramArrf, 0, sizeof(nHistogramArrf));
	//memset(fPercentagesInHistogramArrf, 0.0, sizeof(fPercentagesInHistogramArrf));

	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramIntervals_ForCooc; iHistogramBinf++)
	{
		nHistInBinsArrf[iHistogramBinf] = 0;
		fPercentsOfHistogramIntervalsArrf[iHistogramBinf] = 0.0;

		if (iHistogramBinf < nNumOfHistogramIntervals_ForCooc - 2)
		{
			nAboveThresholds_InHistogramArrf[iHistogramBinf] = 0;

			fPercentagesAboveThresholds_InHistogramArrf[iHistogramBinf] = 0.0;
		} //if (iHistogramBinf < nNumOfHistogramIntervals_ForCooc - 2)

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramIntervals_ForCooc; iHistogramBinf++)
	  //////////////////////////////////////////////////////

	//printf( "\n\n nIntensityOfBackground = %d ", nIntensityOfBackground);
	//fprintf(fout_PrintFeatures, "\n\n nIntensityOfBackground = %d ", nIntensityOfBackground);

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//fprintf(fout_PrintFeatures, "\n\n iWidf = %d: ", iWidf);

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelf = iLenf + (iWidf*nImageLengthf);

			//nIntensityCurf = sGrayscale_Imagef->nPixelArr[nIndexOfPixelf];
			nIntensityCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelf];

			//if (nIntensityCurf == nIntensityOfBackground)
			if (nIntensityCurf == nIntensityOfBackground)
				continue;

			nNumOfValidObjectPixelsf += 1;
			//fprintf(fout_PrintFeatures, " %d:%d %d,", iWidf, iLenf, nIntensityCurf);

			nHistogramArrf[nIntensityCurf] += 1;

			nNumOfHistogramBinCurf = nIntensityCurf / nWidthOfABinInHistogramf;

			nHistInBinsArrf[nNumOfHistogramBinCurf] += 1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
////////////////////////////////////////////////////////////////////////

	if (nNumOfValidObjectPixelsf <= 0)
	{
		printf("\n\n Exit by 'RETURN_BY_THE_SAME_INTENSITY' in  'Histogram_Statistics_ColorActuallyGrayscale_Image': nNumOfValidObjectPixelsf = %d <= 0", nNumOfValidObjectPixelsf);
		//printf("\n\n Please press any key to exit:"); getchar(); exit(1);

//#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_PrintFeatures, "\n\n Exit by 'RETURN_BY_THE_SAME_INTENSITY' in  'Histogram_Statistics_ColorActuallyGrayscale_Image': nNumOfValidObjectPixelsf = %d <= 0", nNumOfValidObjectPixelsf);
		//printf("\n\n Please press any key to exit:");	fflush(fout_lr); getchar(); exit(1);
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//return UNSUCCESSFUL_RETURN;
		return RETURN_BY_THE_SAME_INTENSITY;
	} // if (nNumOfValidObjectPixelsf <= 0)

	//////////////////////////////////////////////////////////////////////////////
//I.Pitas
//Digital image processing algorithms and applications, p.304

//fMeanf
	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		fHistogramProbabArrf[iHistf] = (float)(nHistogramArrf[iHistf]) / (float)(nNumOfValidObjectPixelsf);

		fMeanf += fHistogramProbabArrf[iHistf] * (float)(iHistf);
	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

//fMeanf
	if (fMeanf <= 0.0 || fMeanf > (float)(nNumOfGrayLevelIntensities - 1))
	{
		printf("\n\n Exit by 'RETURN_BY_THE_SAME_INTENSITY' in 'Histogram_Statistics_ColorActuallyGrayscale_Image': fMeanf = %E, nNumOfGrayLevelIntensities - 1 = %d",
			fMeanf, nNumOfGrayLevelIntensities - 1);
		//printf("\n\n Please press any key to exit:"); getchar(); exit(1);

//#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "\n\n Exit by 'RETURN_BY_THE_SAME_INTENSITY' in 'Histogram_Statistics_ColorActuallyGrayscale_Image': fMeanf = %E, nNumOfGrayLevelIntensities - 1 = %d",
			fMeanf, nNumOfGrayLevelIntensities - 1);
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//return UNSUCCESSFUL_RETURN;
		return RETURN_BY_THE_SAME_INTENSITY;
	} // if (fMeanf <= 0.0 || fMeanf > (float)(nNumOfGrayLevelIntensities - 1))

///////////////////////////////////////////////////////////////////////////////	  
//fVariancef
	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		fTempf = (float)(iHistf)-fMeanf;
		fVariancef += fTempf * fTempf*fHistogramProbabArrf[iHistf];
	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

//fVariancef
	if (fVariancef <= 0.0)
	{
		printf("\n\n An error in 'Histogram_Statistics_ColorActuallyGrayscale_Image': fVariancef = %E <= 0.0, fMeanf = %E, nNumOfValidObjectPixelsf = %d, nAreaOfTheWholeImagef = %d",
			fVariancef, fMeanf, nNumOfValidObjectPixelsf, nAreaOfTheWholeImagef);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n An error in 'Histogram_Statistics_ColorActuallyGrayscale_Image': fVariancef = %E <= 0.0, fMeanf = %E, nNumOfValidObjectPixelsf = %d, nAreaOfTheWholeImagef = %d",
			fVariancef, fMeanf, nNumOfValidObjectPixelsf, nAreaOfTheWholeImagef);
		printf("\n\n Please press any key to exit:");
		fflush(fout_lr); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (fVariancef <= 0.0)

	fStDevf = sqrtf(fVariancef);
	///////////////////////////////////////////////////////////////////////

	//fSkewnessf
	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		fTempf = (float)(iHistf)-fMeanf;
		fSkewnessf += fTempf * fTempf*fTempf*fHistogramProbabArrf[iHistf];
	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

	fSkewnessf = fSkewnessf / (fVariancef * fStDevf);
	///////////////////////////////////////////////////////////////////////

	//fKurtosisf
	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		fTempf = (float)(iHistf)-fMeanf;
		fKurtosisf += fTempf * fTempf*fTempf*fTempf*fHistogramProbabArrf[iHistf];
	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

	fKurtosisf = (fKurtosisf / (fVariancef * fVariancef)) - 3.0;
	///////////////////////////////////////////////////////////////////////////////////
	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramIntervals_ForCooc; iHistogramBinf++)
	{
		fPercentsOfHistogramIntervalsArrf[iHistogramBinf] = (float)(nHistInBinsArrf[iHistogramBinf]) / (float)(nNumOfValidObjectPixelsf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n 'Histogram_Statistics_ColorActuallyGrayscale_Image': fPercentsOfHistogramIntervalsArrf[%d] = %E", iHistogramBinf, fPercentsOfHistogramIntervalsArrf[iHistogramBinf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramIntervals_ForCooc; iHistogramBinf++)

	for (iHistogramBinf = 1; iHistogramBinf < nNumOfHistogramIntervals_ForCooc - 1; iHistogramBinf++)
	{
		nSumOfHistBinsf = 0;

		for (iHistThresholdf = iHistogramBinf; iHistThresholdf < nNumOfHistogramIntervals_ForCooc; iHistThresholdf++)
		{
			nSumOfHistBinsf += nHistInBinsArrf[iHistThresholdf];

		} //for (iHistThresholdf = iHistThresholdf; iHistThresholdf < nNumOfHistogramIntervals_ForCooc; iHistThresholdf++)

		nAboveThresholds_InHistogramArrf[iHistogramBinf - 1] = nSumOfHistBinsf;

		fPercentagesAboveThresholds_InHistogramArrf[iHistogramBinf - 1] = (float)(nSumOfHistBinsf) / (float)(nNumOfValidObjectPixelsf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n 'Histogram_Statistics_ColorActuallyGrayscale_Image': fPercentagesAboveThresholds_InHistogramArrf[%d] = %E", iHistogramBinf - 1, fPercentagesAboveThresholds_InHistogramArrf[iHistogramBinf - 1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} //for (iHistogramBinf = 1; iHistogramBinf < nNumOfHistogramIntervals_ForCooc - 1; iHistogramBinf++)

	return SUCCESSFUL_RETURN;
} //Histogram_Statistics_ColorActuallyGrayscale_Image(...
/////////////////////////////////////////////////////////////////

int CoocFeasOf_Image_ByOne_LenWidShift(
	//the image is supposed to be grayscale
	const COLOR_IMAGE *sColor_Imagef,

	const int nLenShiftf,
	const int nWidShiftf,

	COOC_OF_IMAGE_BY_LEN_WID_SHIFT *sCoocOfImageByOne_LenWidShiftf)
{
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int
		nResf,

		nAreaOfTheWholeImagef, // = nImageWidth*nImageHeight,

		nIndexOfIntensityf,

		nIndexOfPixel_1f,
		nIndexOfPixel_2f,

		nIndexOfPixelMaxf,
		iIntensity_1f,
		iIntensity_2f,

		nIntensity_1f,
		nIntensity_2f,

		nTempIntensityf,

		nTempPixel_1f,
		nTempPixel_2f,

		iLenf,
		iWidf,

		nLenBeginf,
		nLenEndf,

		nWidBeginf,
		nWidEndf,

		nNumOfValidObjectPixelsf = 0,
		nRedf,

		nImageWidthf,
		nImageHeightf;
	/////////////

	sCoocOfImageByOne_LenWidShiftf->nLenShift = nLenShiftf;
	sCoocOfImageByOne_LenWidShiftf->nWidShift = nWidShiftf;

	for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)
	{
		nTempIntensityf = iIntensity_1f * nNumOfGrayLevelIntensities;

		for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)
		{
			nIndexOfIntensityf = iIntensity_2f + nTempIntensityf;

			sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] = 0.0;
		} //for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)

	} //for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)
	////////////////////////////////////////////////////////////////////////
	  // size of image
	nImageWidthf = sColor_Imagef->nWidth; //image_in.width();
	nImageHeightf = sColor_Imagef->nLength; //image_in.height();

	nAreaOfTheWholeImagef = nImageWidthf * nImageHeightf;

	nIndexOfPixelMaxf = nAreaOfTheWholeImagef;
	if (nLenShiftf <= 0)
	{
		nLenBeginf = -nLenShiftf;
		nLenEndf = sColor_Imagef->nLength;

	} // if (nLenShiftf <= 0)
	else if (nLenShiftf > 0)
	{
		nLenBeginf = 0;
		nLenEndf = sColor_Imagef->nLength - nLenShiftf;

	} // if (nLenShiftf < 0)
///////////////////

	if (nWidShiftf <= 0)
	{
		nWidBeginf = -nWidShiftf;
		nWidEndf = sColor_Imagef->nWidth;

	} // if (nWidShiftf <= 0)
	else if (nWidShiftf > 0)
	{
		nWidBeginf = 0;
		nWidEndf = sColor_Imagef->nWidth - nWidShiftf;

	} // if (nWidShiftf < 0)
///////////////////////////

	for (iWidf = nWidBeginf; iWidf < nWidEndf; iWidf++)
	{
		nTempPixel_1f = iWidf * sColor_Imagef->nLength;

		nTempPixel_2f = (iWidf + nWidShiftf) * sColor_Imagef->nLength;

		for (iLenf = nLenBeginf; iLenf < nLenEndf; iLenf++)
		{
			nIndexOfPixel_1f = iLenf + nTempPixel_1f;

			if (nIndexOfPixel_1f < 0 || nIndexOfPixel_1f >= nIndexOfPixelMaxf)
			{
				printf("\n\n An error in 'CoocFeasOf_Image_ByOne_LenWidShift': nIndexOfPixel_1f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_1f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);
				printf("\n\n nLenShiftf = %d, nWidShiftf = %d", nLenShiftf, nWidShiftf);
				printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

				printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_ByOne_LenWidShift': nIndexOfPixel_1f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_1f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfPixel_1f < 0 || nIndexOfPixel_1f >= nIndexOfPixelMaxf)
///////////////////////////////////
			nIndexOfPixel_2f = (iLenf + nLenShiftf) + nTempPixel_2f;

			if (nIndexOfPixel_2f < 0 || nIndexOfPixel_2f >= nIndexOfPixelMaxf)
			{
				printf("\n\n An error in 'CoocFeasOf_Image_ByOne_LenWidShift': nIndexOfPixel_2f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_2f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);
				printf("\n\n nLenShiftf = %d, nWidShiftf = %d", nLenShiftf, nWidShiftf);
				printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

				printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures);  getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_ByOne_LenWidShift': nIndexOfPixel_2f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_2f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfPixel_2f < 0 || nIndexOfPixel_2f >= nIndexOfPixelMaxf)
			//////////////////////
			nIntensity_1f = sColor_Imagef->nRed_Arr[nIndexOfPixel_1f];

			nIntensity_2f = sColor_Imagef->nRed_Arr[nIndexOfPixel_2f];

			if (nIntensity_1f == nIntensityOfBackground || nIntensity_2f == nIntensityOfBackground)
				continue;

			nNumOfValidObjectPixelsf += 1;

			nTempIntensityf = nIntensity_1f * nNumOfGrayLevelIntensities;

			nIndexOfIntensityf = iIntensity_2f + nTempIntensityf;
			sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] += 1.0;
		} //for (iLenf = nLenBeginf; iLenf < nLenEndf; iLenf++)

	} //for (iWidf = nWidBeginf; iWidf < nWidEndf; iWidf++)
////////////////////////////////////////
	if (nNumOfValidObjectPixelsf <= 1)
	{
		printf("\n\n An error in 'CoocFeasOf_Image_ByOne_LenWidShift': nNumOfValidObjectPixelsf = %d, nLenShiftf = %d, nWidShiftf = %d, nAreaOfTheWholeImagef = %d",
			nNumOfValidObjectPixelsf, nLenShiftf, nWidShiftf, nAreaOfTheWholeImagef);
		printf("\n\n nLenShiftf = %d, nWidShiftf = %d", nLenShiftf, nWidShiftf);
		printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

		printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_ByOne_LenWidShift': nNumOfValidObjectPixelsf = %d, nLenShiftf = %d, nWidShiftf = %d, nAreaOfTheWholeImagef = %d",
			nNumOfValidObjectPixelsf, nLenShiftf, nWidShiftf, nAreaOfTheWholeImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfValidPixelsTotf <= 1)
/////////////////////

	for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)
	{
		nTempIntensityf = iIntensity_1f * nNumOfGrayLevelIntensities;

		for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)
		{
			nIndexOfIntensityf = iIntensity_2f + nTempIntensityf;

			sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] =
				sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] / (float)(nNumOfValidObjectPixelsf);

			if (sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] < 0.0 ||
				sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] > 1.0)
			{
				printf("\n\n An error in 'CoocFeasOf_Image_ByOne_LenWidShift': sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] = %E, nLenShiftf = %d, nWidShiftf = %d, nAreaOfTheWholeImagef = %d",
					sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf], nLenShiftf, nWidShiftf, nAreaOfTheWholeImagef);
				printf("\n\n nLenShiftf = %d, nWidShiftf = %d", nLenShiftf, nWidShiftf);
				printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

				printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_ByOne_LenWidShift': sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] = %E, nLenShiftf = %d, nWidShiftf = %d, nAreaOfTheWholeImagef = %d",
					sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf], nLenShiftf, nWidShiftf, nAreaOfTheWholeImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] < 0.0 || ...

		} //for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)

	} //for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'CoocFeasOf_Image_ByOne_LenWidShift': nLenShiftf = %d, nWidShiftf = %d", nLenShiftf, nWidShiftf);
	printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

	fprintf(fout_PrintFeatures, "\n\n 'CoocFeasOf_Image_ByOne_LenWidShift': nLenShiftf = %d, nWidShiftf = %d", nLenShiftf, nWidShiftf);
	fprintf(fout_PrintFeatures, "\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} //int CoocFeasOf_Image_ByOne_LenWidShift(...
//////////////////////////////////////////////

int CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist(
	//the image is supposed to be grayscale
	const COLOR_IMAGE *sColor_Imagef,

	const int nDistToCoocPointsf,

	float fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[]) //4*nDistToCoocPointsf*nCoocSizeMax
{
	int CoocFeasOf_Image_ByOne_LenWidShift(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nLenShiftf,
		const int nWidShiftf,

		COOC_OF_IMAGE_BY_LEN_WID_SHIFT *sCoocOfImageByOne_LenWidShiftf);
	/////////////////
	COOC_OF_IMAGE_BY_LEN_WID_SHIFT sCoocOfImageByOne_LenWidShiftf;

	int
		iLenf,
		iWidf,

		nNumOfShiftsTotf = 0,
		iCoocf,
		nIndexOfCoocf,

		nIndexOfCoocAtAFixedDistMaxf = 4 * nDistToCoocPointsf*nCoocSizeMax,
		nTempf,

		nLenBeginf,
		nLenEndf,

		nWidBeginf,
		nWidEndf,

		nLenShiftf,
		nWidShiftf,

		nResf;
	////////////////
	for (iCoocf = 0; iCoocf < nIndexOfCoocAtAFixedDistMaxf; iCoocf++)
	{
		fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[iCoocf] = 0.0;
	} //for (iCoocf = 0; iCoocf < nIndexOfCoocAtAFixedDistMaxf; iCoocf++)

///////////////////////
//the top 
	nWidShiftf = nDistToCoocPointsf;

	nLenBeginf = -nDistToCoocPointsf;
	nLenEndf = nDistToCoocPointsf;

	for (iLenf = nLenBeginf; iLenf <= nLenEndf; iLenf++)
	{
		nNumOfShiftsTotf += 1;

		nLenShiftf = iLenf;

		nResf = CoocFeasOf_Image_ByOne_LenWidShift(
			//the image is supposed to be grayscale
			sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

			nLenShiftf, //const int nLenShiftf,
			nWidShiftf, //const int nWidShiftf,

			&sCoocOfImageByOne_LenWidShiftf); // COOC_OF_IMAGE_BY_LEN_WID_SHIFT *sCoocOfImageByOne_LenWidShiftf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'CoocFeasOf_Image_ByOne_LenWidShift' 1");
			printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'CoocFeasOf_Image_ByOne_LenWidShift' 1");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

		nTempf = (nNumOfShiftsTotf - 1)*nCoocSizeMax;

		for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)
		{
			nIndexOfCoocf = iCoocf + nTempf;

			if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)
			{
				printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' 1: nIndexOfCoocf = %d >= nIndexOfCoocAtAFixedDistMaxf = %d, nLenShiftf = %d, nWidShiftf = %d, nDistToCoocPointsf = %d",
					nIndexOfCoocf, nIndexOfCoocAtAFixedDistMaxf, nLenShiftf, nWidShiftf, nDistToCoocPointsf);
				printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n  An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' 1: nIndexOfCoocf = %d >= nIndexOfCoocAtAFixedDistMaxf = %d, nLenShiftf = %d, nWidShiftf = %d, nDistToCoocPointsf = %d",
					nIndexOfCoocf, nIndexOfCoocAtAFixedDistMaxf, nLenShiftf, nWidShiftf, nDistToCoocPointsf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)

			fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[nIndexOfCoocf] = sCoocOfImageByOne_LenWidShiftf.fCoocOfImageByLenWidShift_Arr[iCoocf];
		} //for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)

	} //for (iLenf = nLenBeginf; iLenf <= nLenEndf; iLenf++)

	////////////////
//the right 
	nLenShiftf = nDistToCoocPointsf;

	nWidBeginf = 0;
	nWidEndf = nDistToCoocPointsf - 1;

	for (iWidf = nWidBeginf; iWidf <= nWidEndf; iWidf++)
	{
		nNumOfShiftsTotf += 1;

		nWidShiftf = iWidf;

		nResf = CoocFeasOf_Image_ByOne_LenWidShift(
			//the image is supposed to be grayscale
			sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

			nLenShiftf, //const int nLenShiftf,
			nWidShiftf, //const int nWidShiftf,

			&sCoocOfImageByOne_LenWidShiftf); // COOC_OF_IMAGE_BY_LEN_WID_SHIFT *sCoocOfImageByOne_LenWidShiftf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'CoocFeasOf_Image_ByOne_LenWidShift' 2");
			printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'CoocFeasOf_Image_ByOne_LenWidShift' 2");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

		nTempf = (nNumOfShiftsTotf - 1)*nCoocSizeMax;

		for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)
		{
			nIndexOfCoocf = iCoocf + nTempf;

			if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)
			{
				printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' 2: nIndexOfCoocf = %d >= nIndexOfCoocAtAFixedDistMaxf = %d, nLenShiftf = %d, nWidShiftf = %d, nDistToCoocPointsf = %d",
					nIndexOfCoocf, nIndexOfCoocAtAFixedDistMaxf, nLenShiftf, nWidShiftf, nDistToCoocPointsf);
				printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n  An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' 2: nIndexOfCoocf = %d >= nIndexOfCoocAtAFixedDistMaxf = %d, nLenShiftf = %d, nWidShiftf = %d, nDistToCoocPointsf = %d",
					nIndexOfCoocf, nIndexOfCoocAtAFixedDistMaxf, nLenShiftf, nWidShiftf, nDistToCoocPointsf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)

			fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[nIndexOfCoocf] = sCoocOfImageByOne_LenWidShiftf.fCoocOfImageByLenWidShift_Arr[iCoocf];
		} //for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)

	} //for (iWidf = nWidBeginf; iWidf <= nWidEndf; iWidf++)

		////////////////
//the left 
	nLenShiftf = -nDistToCoocPointsf;

	nWidBeginf = 1; // not 0; -- symmetry with the right
	nWidEndf = nDistToCoocPointsf - 1;

	for (iWidf = nWidBeginf; iWidf <= nWidEndf; iWidf++)
	{
		nNumOfShiftsTotf += 1;

		nWidShiftf = iWidf;

		nResf = CoocFeasOf_Image_ByOne_LenWidShift(
			//the image is supposed to be grayscale
			sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

			nLenShiftf, //const int nLenShiftf,
			nWidShiftf, //const int nWidShiftf,

			&sCoocOfImageByOne_LenWidShiftf); // COOC_OF_IMAGE_BY_LEN_WID_SHIFT *sCoocOfImageByOne_LenWidShiftf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'CoocFeasOf_Image_ByOne_LenWidShift' 3");
			printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'CoocFeasOf_Image_ByOne_LenWidShift' 3");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)

		nTempf = (nNumOfShiftsTotf - 1)*nCoocSizeMax;

		for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)
		{
			nIndexOfCoocf = iCoocf + nTempf;
			if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)
			{
				printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' 3: nIndexOfCoocf = %d >= nIndexOfCoocAtAFixedDistMaxf = %d, nLenShiftf = %d, nWidShiftf = %d, nDistToCoocPointsf = %d",
					nIndexOfCoocf, nIndexOfCoocAtAFixedDistMaxf, nLenShiftf, nWidShiftf, nDistToCoocPointsf);
				printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n  An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist' 3: nIndexOfCoocf = %d >= nIndexOfCoocAtAFixedDistMaxf = %d, nLenShiftf = %d, nWidShiftf = %d, nDistToCoocPointsf = %d",
					nIndexOfCoocf, nIndexOfCoocAtAFixedDistMaxf, nLenShiftf, nWidShiftf, nDistToCoocPointsf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)

			fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[nIndexOfCoocf] = sCoocOfImageByOne_LenWidShiftf.fCoocOfImageByLenWidShift_Arr[iCoocf];
		} //for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)

	} //for (iWidf = nWidBeginf; iWidf <= nWidEndf; iWidf++)
/////////////////////////////////
	if (nNumOfShiftsTotf != 4 * nDistToCoocPointsf)
	{
		printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist': nNumOfShiftsTotf = %d != 4 * nDistToCoocPointsf = %d",
			nNumOfShiftsTotf, 4 * nDistToCoocPointsf);
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist': nNumOfShiftsTotf = %d != 4 * nDistToCoocPointsf = %d",
			nNumOfShiftsTotf, 4 * nDistToCoocPointsf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfShiftsTotf != 4 * nDistToCoocPointsf)

	return SUCCESSFUL_RETURN;
} //int CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist(...
/////////////////////////////////////////////////////////

int CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess(
	//the image is supposed to be grayscale
	const COLOR_IMAGE *sColor_Imagef,

	const int nDistToCoocPointsMinf,
	const int nDistToCoocPointsMaxf,

	float fCoocFeasOf_Image_By_LenWidShift_At_AllDist_Arrf[])
	//4*nCoocSizeMax*[(nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*( nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1)]/2
{
	int CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nDistToCoocPointsf,

		float fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[]); //4*nDistToCoocPointsf*nCoocSizeMax

	int
		iDistf,
		iFeaf,

		nIndexOfFeaCurf,
		nNumOfShiftsTotf = 0,
		iCoocf,
		nIndexOfCoocf,

		nSizeOfCoocFeasCurf,
		nIndexOfCoocAt_AllDistMaxf = 2 * nCoocSizeMax*(nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*(nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1),
		// == 4 * nCoocSizeMax*( (nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*(nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1) ) / 2,

		nTempf,

		nNumOfFeaCurf,
		nResf;
	////////////////

	for (iFeaf = 0; iFeaf < nIndexOfCoocAt_AllDistMaxf; iFeaf++)
	{
		fCoocFeasOf_Image_By_LenWidShift_At_AllDist_Arrf[iFeaf] = 0.0;
	} //for (iFeaf = 0; iFeaf < nIndexOfCoocAt_AllDistMaxf; iFeaf++)

///////////////////////////////////
	nNumOfFeaCurf = 0;
	for (iDistf = nDistToCoocPointsMinf; iDistf <= nDistToCoocPointsMaxf; iDistf++)
	{
		nSizeOfCoocFeasCurf = 4 * iDistf*nCoocSizeMax;

		float *fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf = new float[nSizeOfCoocFeasCurf]; //0 -- 1 //[nDimSelecMaxf]

		if (fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf == nullptr)
		{
			printf("\n\n An error in dynamic memory allocation for 'fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf'");
			printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (fMomentsOfSquaresArrf == nullptr)

		nResf = CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist(
			//the image is supposed to be grayscale
			sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

			iDistf, //const int nDistToCoocPointsf,

			fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf); // float fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[]); //4*nDistToCoocPointsf*nCoocSizeMax

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess' by 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist'");
			printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_PrintFeatures, "\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess' by 'CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)
////////////////////////////

		for (iFeaf = 0; iFeaf < nSizeOfCoocFeasCurf; iFeaf++)
		{
			nIndexOfFeaCurf = nNumOfFeaCurf + iFeaf;
			if (nIndexOfFeaCurf < 0 || nIndexOfFeaCurf >= nIndexOfCoocAt_AllDistMaxf)
			{
				printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess': nIndexOfFeaCurf = %d >= nIndexOfCoocAt_AllDistMaxf = %d, iDistf = %d, nDistToCoocPointsMinf = %d, nDistToCoocPointsMaxf = %d",
					nIndexOfFeaCurf, nIndexOfCoocAt_AllDistMaxf, iDistf, nDistToCoocPointsMinf, nDistToCoocPointsMaxf);
				printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n  An error in 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess': nIndexOfFeaCurf = %d >= nIndexOfCoocAt_AllDistMaxf = %d, iDistf = %d, nDistToCoocPointsMinf = %d, nDistToCoocPointsMaxf = %d",
					nIndexOfFeaCurf, nIndexOfCoocAt_AllDistMaxf, iDistf, nDistToCoocPointsMinf, nDistToCoocPointsMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfFeaCurf < 0 || nIndexOfFeaCurf >= nIndexOfCoocAt_AllDistMaxf)

			fCoocFeasOf_Image_By_LenWidShift_At_AllDist_Arrf[nIndexOfFeaCurf] = fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[iFeaf];
		} //for (iFeaf = 0; iFeaf < nSizeOfCoocFeasCurf; iFeaf++)

		nNumOfFeaCurf += nSizeOfCoocFeasCurf;

		delete[] fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf;
	} //for (iDistf = nDistToCoocPointsMinf; iDistf <= nDistToCoocPointsMaxf; iDistf++)

	if (nNumOfFeaCurf != nIndexOfCoocAt_AllDistMaxf)
	{
		printf("\n\n An error in 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess': nNumOfFeaCurf = %d != nIndexOfCoocAt_AllDistMaxf = %d, nDistToCoocPointsMinf = %d, nDistToCoocPointsMaxf = %d",
			nNumOfFeaCurf, nIndexOfCoocAt_AllDistMaxf, nDistToCoocPointsMinf, nDistToCoocPointsMaxf);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "\n\n  An error in 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess': nNumOfFeaCurf = %d != nIndexOfCoocAt_AllDistMaxf = %d, nDistToCoocPointsMinf = %d, nDistToCoocPointsMaxf = %d",
			nNumOfFeaCurf, nIndexOfCoocAt_AllDistMaxf, nDistToCoocPointsMinf, nDistToCoocPointsMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfFeaCurf != nIndexOfCoocAt_AllDistMaxf)

	return SUCCESSFUL_RETURN;
} //int CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess(...
///////////////////////////////////////////////////

void Initializing_AFloatVec_To_Zero(
	const int nDimf,
	float fVecArrf[]) //[nDimf]
{
	int
		iFeaf;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fVecArrf[iFeaf] = 0.0;
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
}//void Initializing_AFloatVec_To_Zero(
////////////////////////////////////////////////////////////////////////

int	Converting_All_Feas_To_ARange_0_1(
	const int nDimf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	int &nNumOfFeasWithUnusualValuesf,
	int &nNumOfFeasWith_TheSameValuesf,

	int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
	float fFeas_InitMinArrf[], //[nDimf]
	float fFeas_InitMaxArrf[], //[nDimf]

	float fArr1stf[], //[nDimf*nNumVec1stf]
	float fArr2ndf[]) //[nDimf*nNumVec2ndf]
{

	int
		nNumOfFeasFilledf = 0,

		nIndexf,
		nTempf,
		iVecf,
		iFeaf;

	float
		fRangeCurf,
		fNormalizedRangeMaxf = 1.0 + feps,
		fFeaCurf;

	nNumOfFeasWithUnusualValuesf = 0;
	nNumOfFeasWith_TheSameValuesf = 0;
	//////////////////////////////////////

	float *fFeaRangeArrf;
	fFeaRangeArrf = new float[nDimf];

	if (fFeaRangeArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeaRangeArrf' in 'Converting_All_Feas_To_ARange_0_1'");
		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeaRangeArrf == nullptr)
	
		fprintf(fout_PrintFeatures, "\n\n 'Converting_All_Feas_To_ARange_0_1': nDimf = %d, nNumVec1stf = %d, nNumVec2ndf = %d", nDimf, nNumVec1stf, nNumVec2ndf);
		fflush(fout_PrintFeatures);

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nFeaTheSameOrNotArrf[iFeaf] = 0; //not the same initially
		fFeaRangeArrf[iFeaf] = 0.0;

		fFeas_InitMinArrf[iFeaf] = fLarge;
		fFeas_InitMaxArrf[iFeaf] = -fLarge;
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

//printing all fea vecs+

	/*
	fprintf(fout_PrintFeatures, "\n\n Normal:");

for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
{
	fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': 1 (init feas):  iVecf = %d, ", iVecf);
	nTempf = iVecf * nDimf;
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		fFeaCurf = fArr1stf[nIndexf];

		fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

//fprintf(fout_PrintFeatures, "\n\n Malignant:");

for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
{
	fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': 2 (init feas):  iVecf = %d, ", iVecf);
	nTempf = iVecf * nDimf;
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{

		nIndexf = iFeaf + nTempf;
		fFeaCurf = fArr2ndf[nIndexf];

		fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
*/

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
		{
			nTempf = iVecf * nDimf;
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr1stf[nIndexf];

			if (fFeaCurf < -fLarge || fFeaCurf > fLarge)
			{
				printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fFeaCurf = %E, fLarge = %E, iFeaf = %d, iVecf = %d", fFeaCurf, fLarge, iFeaf, iVecf);
				printf("\n\n Please press any key to exit"); getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} //if (fFeaCurf < -fLarge || fFeaCurf > fLarge)

			if (iFeaf == nNumOfSelectedFea)
			{
				//fprintf(fout_PrintFeatures, "\n 1: iFeaf = %d, iVecf = %d, fFeaCurf = %E", iFeaf, iVecf, fFeaCurf);
			} //if (iFeaf == nNumOfSelectedFea)

			if (fFeaCurf < fFeas_InitMinArrf[iFeaf])
			{
				fFeas_InitMinArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//fprintf(fout_PrintFeatures, "\n\n 1: fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf < fFeas_InitMinArrf[iFeaf])

			if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
			{
				fFeas_InitMaxArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//fprintf(fout_PrintFeatures, "\n\n 1: fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])

		} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
//////////////////////////////////////////////////////////
		//fprintf(fout_PrintFeatures, "\n\n");
		for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
		{
			nTempf = iVecf * nDimf;
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr2ndf[nIndexf];

			if (fFeaCurf < -fLarge || fFeaCurf > fLarge)
			{
				printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fFeaCurf = %E, fLarge = %E, iFeaf = %d, iVecf = %d", fFeaCurf, fLarge, iFeaf, iVecf);
				printf("\n\n Please press any key to exit"); getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} //if (fFeaCurf < -fLarge || fFeaCurf > fLarge)

			if (iFeaf == nNumOfSelectedFea)
			{
				//	fprintf(fout_PrintFeatures, "\n 2: iFeaf = %d, iVecf = %d, fFeaCurf = %E", iFeaf, iVecf, fFeaCurf);
			} //if (iFeaf == nNumOfSelectedFea)

			if (fFeaCurf < fFeas_InitMinArrf[iFeaf])
			{
				fFeas_InitMinArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//	fprintf(fout_PrintFeatures, "\n\n 2: fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)

			} //if (fFeaCurf < fFeas_InitMinArrf[iFeaf])

			if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
			{
				fFeas_InitMaxArrf[iFeaf] = fFeaCurf;

				if (iFeaf == nNumOfSelectedFea)
				{
					//		fprintf(fout_PrintFeatures, "\n\n 2: fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
		} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

		if (fFeas_InitMinArrf[iFeaf] < -fFeaValueThreshold_ForWarning)
		{
			nNumOfFeasWithUnusualValuesf += 1;
			//printf("\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
			//fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %d", iFeaf, fFeas_InitMinArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (fFeas_InitMinArrf[iFeaf] < -fFeaValueThreshold_ForWarning)

		if (fFeas_InitMaxArrf[iFeaf] > fFeaValueThreshold_ForWarning)
		{
			if (fFeas_InitMinArrf[iFeaf] >= -fFeaValueThreshold_ForWarning)
			{
				nNumOfFeasWithUnusualValuesf += 1;
				//printf("\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				//fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %d", iFeaf, fFeas_InitMaxArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (fFeas_InitMinArrf[iFeaf] >= -fFeaValueThreshold_ForWarning)

		}//if (fFeas_InitMaxArrf[iFeaf] > fFeaValueThreshold_ForWarning)
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
////////////////////////////////////////////////////////////

//#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n 'Converting_All_Feas_To_ARange_0_1': the total # of unusual feas, nNumOfFeasWithUnusualValuesf = %d", nNumOfFeasWithUnusualValuesf);
	fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': the total # of unusual feas, nNumOfFeasWithUnusualValuesf = %d", nNumOfFeasWithUnusualValuesf);
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fRangeCurf = fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf];

		if (iFeaf == nNumOfSelectedFea)
		{
			//		printf( "\n\n fRangeCurf = %E, fFeas_InitMaxArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
				//		fRangeCurf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

					//fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': fRangeCurf = %E, fFeas_InitMaxArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//fRangeCurf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

		} //if (iFeaf == nNumOfSelectedFea)

		if (fRangeCurf < feps)
		{
			nFeaTheSameOrNotArrf[iFeaf] = 1; //the same 
			nNumOfFeasWith_TheSameValuesf += 1;

			fprintf(fout_PrintFeatures, "\n The next nNumOfFeasWith_TheSameValuesf = %d,  (fFeas_InitMaxArrf[%d] = %E - fFeas_InitMinArrf[%d]) = %E = %E",
				nNumOfFeasWith_TheSameValuesf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf], fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf]);

		} //if (fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf] < feps)
		else
		{
			fFeaRangeArrf[iFeaf] = fRangeCurf;
		} //else

	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	printf("\n\n 'Converting_All_Feas_To_ARange_0_1': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);
	fprintf(fout_PrintFeatures, "\n\n 'Converting_All_Feas_To_ARange_0_1': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);

	fflush(fout_PrintFeatures);
	/////////////////////////////////////
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		if (nFeaTheSameOrNotArrf[iFeaf] == 0)  //the feas are different 
		{
			//	fprintf(fout_PrintFeatures, "\n");
			for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
			{
				nTempf = iVecf * nDimf;
				nIndexf = iFeaf + nTempf;

				fArr1stf[nIndexf] = (fArr1stf[nIndexf] - fFeas_InitMinArrf[iFeaf]) / fFeaRangeArrf[iFeaf];

				if (iFeaf == nNumOfSelectedFea)
				{
					//		printf( "\n\n fArr1stf[nIndexf] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//		fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

							//fprintf(fout_PrintFeatures, "\n  'Converting_All_Feas_To_ARange_0_1' 1: iFeaf = %d, iVecf = %d, fArr1stf[%d] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
								//iFeaf, iVecf,nIndexf,fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

				} //if (iFeaf == nNumOfSelectedFea)

				if (fArr1stf[nIndexf] < -feps || fArr1stf[nIndexf] > fNormalizedRangeMaxf)
				{
					printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fArr1stf[%d] = %E is out of the range", nIndexf, fArr1stf[nIndexf]);
					printf("\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fArr1stf[%d] = %E is out of the range", nIndexf, fArr1stf[nIndexf]);
					fprintf(fout_lr, "\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					fflush(fout_PrintFeatures);

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					delete[] fFeaRangeArrf;
					return UNSUCCESSFUL_RETURN;
				}//if (fArr1stf[nIndexf] < -feps || fArr1stf[nIndexf] > fNormalizedRangeMaxf)

			} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

			//fprintf(fout_PrintFeatures, "\n");
			for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
			{
				nTempf = iVecf * nDimf;
				nIndexf = iFeaf + nTempf;

				fArr2ndf[nIndexf] = (fArr2ndf[nIndexf] - fFeas_InitMinArrf[iFeaf]) / fFeaRangeArrf[iFeaf];

				if (fArr2ndf[nIndexf] < -feps || fArr2ndf[nIndexf] > fNormalizedRangeMaxf)
				{
					printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fArr2ndf[%d] = %E is out of the range", nIndexf, fArr2ndf[nIndexf]);
					printf("\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fArr2ndf[%d] = %E is out of the range", nIndexf, fArr2ndf[nIndexf]);
					fprintf(fout_lr, "\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					delete[] fFeaRangeArrf;

					return UNSUCCESSFUL_RETURN;
				}//if (fArr2ndf[nIndexf] < -feps || fArr2ndf[nIndexf] > fNormalizedRangeMaxf)

				if (iFeaf == nNumOfSelectedFea)
				{
					//		printf( "\n\n fArr1stf[nIndexf] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//		fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

					//fprintf(fout_PrintFeatures, "\n  'Converting_All_Feas_To_ARange_0_1' 2: iFeaf = %d, iVecf = %d, fArr2ndf[%d] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//iFeaf, iVecf, nIndexf, fArr2ndf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

				} //if (iFeaf == nNumOfSelectedFea)
			} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

			fflush(fout_PrintFeatures);
		} //if (nFeaTheSameOrNotArrf[iFeaf] == 0)  //the feas are different 

	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	delete[] fFeaRangeArrf;

	return SUCCESSFUL_RETURN;
} // int Converting_All_Feas_To_ARange_0_1(...
////////////////////////////////////////////////////////////////////////////////////////////

int	Converting_One_Fea_To_ARange_0_1(
	const int nDim1f,
	const int nDim2f,

	int &nFeaTheSameOrNotf,
	float &fFeaMinf,
	float &fFeaMaxf,

	float fOneDimArr1stf[], //[nDim1f]
	float fOneDimArr2ndf[]) //[nDim2f]
{

	int
		iFeaf;

	float
		fRangeCurf,
		fNormalizedRangeMaxf = 1.0 + feps,
		fFeaCurf;

	//////////////////////////////////////

		//fprintf(fout_PrintFeatures, "\n\n 'Converting_One_Fea_To_ARange_0_1': nDim1f = %d, nDim2f = %d", nDim1f, nDim2f);
	nFeaTheSameOrNotf = 0; //not the same initially

	fFeaMinf = fLarge;
	fFeaMaxf = -fLarge;

	for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
	{
		fFeaCurf = fOneDimArr1stf[iFeaf];

		if (fFeaCurf < fFeaMinf)
		{
			fFeaMinf = fFeaCurf;
		} //if (fFeaCurf < fFeaMinf)

		if (fFeaCurf > fFeaMaxf)
		{
			fFeaMaxf = fFeaCurf;
		} //if (fFeaCurf > fFeaMinf)

	} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)

//////////////////////////////////////////////////////////
	for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
	{
		fFeaCurf = fOneDimArr2ndf[iFeaf];

		if (fFeaCurf < fFeaMinf)
		{
			fFeaMinf = fFeaCurf;
		} //if (fFeaCurf < fFeaMinf)

		if (fFeaCurf > fFeaMaxf)
		{
			fFeaMaxf = fFeaCurf;
		} //if (fFeaCurf > fFeaMinf)

	} // for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
////////////////////////////////////////////////////////////

	fRangeCurf = fFeaMaxf - fFeaMinf;

	if (fRangeCurf < feps)
	{
		nFeaTheSameOrNotf = 1;

		return (-2);
	} //if (fRangeCurf < feps)

/////////////////////////////////////
	if (nFeaTheSameOrNotf == 0)  //the feas are different 
	{
		//	fprintf(fout_PrintFeatures, "\n");
		for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
		{
			fOneDimArr1stf[iFeaf] = (fOneDimArr1stf[iFeaf] - fFeaMinf) / fRangeCurf;

			if (fOneDimArr1stf[iFeaf] < -feps || fOneDimArr1stf[iFeaf] > fNormalizedRangeMaxf)
			{
				printf("\n\n An error in 'Converting_One_Fea_To_ARange_0_1' 1: fOneDimArr1stf[%d] = %E is out of the range", iFeaf, fOneDimArr1stf[iFeaf]);

				printf("\n\n Please press any key to exit:");	fflush(fout_PrintFeatures);  getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			}//if (fOneDimArr1stf[nIndexf] < -feps || fOneDimArr1stf[nIndexf] > fNormalizedRangeMaxf)

		} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
////////////////////////////////////////////
		for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
		{
			fOneDimArr2ndf[iFeaf] = (fOneDimArr2ndf[iFeaf] - fFeaMinf) / fRangeCurf;


			if (fOneDimArr2ndf[iFeaf] < -feps || fOneDimArr2ndf[iFeaf] > fNormalizedRangeMaxf)
			{
				printf("\n\n An error in 'Converting_One_Fea_To_ARange_0_1' 1: fOneDimArr2ndf[%d] = %E is out of the range", iFeaf, fOneDimArr2ndf[iFeaf]);

				printf("\n\n Please press any key to exit:");	fflush(fout_PrintFeatures);  getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			}//if (fOneDimArr2ndf[nIndexf] < -feps || fOneDimArr2ndf[nIndexf] > fNormalizedRangeMaxf)

		} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)

		fflush(fout_PrintFeatures);
	} //if (nFeaTheSameOrNotf == 0)  //the feas are different 

	return SUCCESSFUL_RETURN;
} // int Converting_One_Fea_To_ARange_0_1(...
////////////////////////////////////////////////////

int ConvertingVecs_ToBest_SeparableFeas(
	const float fLargef,
	const  float fepsf,

	const int nDimf,
	const int nDimSelecMaxf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	const float fPrecisionOf_G_Const_Searchf,

	const  float fBorderBelf,
	const  float fBorderAbof,

	const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
	const float fEfficBestSeparOfOneFeaAcceptMaxf,

	float fArr1stf[], //[nDimf*nNumVec1stf] // will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	float fArr2ndf[], //[nDimf*nNumVec2ndf] //will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
///////////////////////////////////

	int &nDimSelecf,

	float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
	float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

	float &fRatioBestMinf,

	int &nPosOneFeaBestMaxf,

	int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

	float &fNumArr1stSeparBestMaxf,
	float &fNumArr2ndSeparBestMaxf,

	float fRatioBestArrf[], //nDimSelecMaxf

	int nPosFeaSeparBestArrf[])	//nDimSelecMaxf
{
	int	Converting_All_Feas_To_ARange_0_1(
		const int nDimf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		int &nNumOfFeasWithUnusualValuesf,
		int &nNumOfFeasWith_TheSameValuesf,

		int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
		float fFeas_InitMinArrf[], //[nDimf]
		float fFeas_InitMaxArrf[], //[nDimf]

		float fArr1stf[], //[nDimf*nNumVec1stf]
		float fArr2ndf[]); //[nDimf*nNumVec2ndf]

	int SelectingBestFeas(
		const float fLargef,
		const  float fepsf,

		const int nDimf,
		const int nDimSelecMaxf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		const float fEfficBestSeparOfOneFeaAcceptMaxf,

		const float fArr1stf[], //0 -- 1
		const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

int &nDimSelecf,
//float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf]
//float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf]

		float &fRatioBestMinf,

		int &nPosOneFeaBestMaxf,

		int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparBestMaxf,
		float &fNumArr2ndSeparBestMaxf,

		float fRatioBestArrf[], //nDimSelecMaxf

		int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

//////////////////////////////////////////
	int
		nResf,
		iFeaf,
		iVecf,
		nIndexf,
		nTempf,
		nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf;

	//nFeaTheSameOrNotArrf[nDim]; //[nDimf] // 1-- the same, 0-- not

	float
		fFeaCurf;
	//fFeas_InitMinArrf[nDim],
	//fFeas_InitMaxArrf[nDim];

	fprintf(fout_PrintFeatures, "\n\n ConvertingVecs_ToBest_SeparableFeas': begin");
	fflush(fout_PrintFeatures);

////////////////////////////////////////////
	int *nFeaTheSameOrNotArrf = new int[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (nFeaTheSameOrNotArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'nFeaTheSameOrNotArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaTheSameOrNotArrf == nullptr)

	float *fFeas_InitMinArrf = new float[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (fFeas_InitMinArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeas_InitMinArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeas_InitMinArrf == nullptr)

	float *fFeas_InitMaxArrf = new float[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (fFeas_InitMaxArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeas_InitMaxArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeas_InitMaxArrf == nullptr)


/////////////////////////////////////////
	printf("\n\n ConvertingVecs_ToBest_SeparableFeas': nDimf = %d, nDimSelecMaxf = %d, nNumVec1stf = %d, nNumVec2ndf = %d",
		nDimf, nDimSelecMaxf, nNumVec1stf, nNumVec2ndf);

	fprintf(fout_PrintFeatures, "\n\n 'ConvertingVecs_ToBest_SeparableFeas': nDimf = %d, nDimSelecMaxf = %d, nNumVec1stf = %d, nNumVec2ndf = %d",
		nDimf, nDimSelecMaxf, nNumVec1stf, nNumVec2ndf);
	fflush(fout_PrintFeatures);

	/*
		fprintf(fout_PrintFeatures, "\n\n 'ConvertingVecs_ToBest_SeparableFeas': entry data");
		for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
		{
			fprintf(fout_PrintFeatures, "\n1, ");
			nTempf = iVecf * nDimf;
			for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;
				fFeaCurf = fArr1stf[nIndexf];

				fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);
			} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

		for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
		{
			fprintf(fout_PrintFeatures, "\n0, ");
			nTempf = iVecf * nDimf;
			for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;
				fFeaCurf = fArr2ndf[nIndexf];

				fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);
			} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

		fflush(fout_PrintFeatures);
	*/
	//printf("\n\n Please press any key:"); getchar();

	nResf = Converting_All_Feas_To_ARange_0_1(
		nDimf, //const int nDimf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		nNumOfFeasWithUnusualValuesf, //int &nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf, //int &nNumOfFeasWith_TheSameValuesf,

		nFeaTheSameOrNotArrf, //int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
		fFeas_InitMinArrf, //float fFeas_InitMinArrf[], //[nDimf]
		fFeas_InitMaxArrf, //float fFeas_InitMaxArrf[], //[nDimf]

		fArr1stf, //float fArr1stf[], //[nDimf*nNumVec1stf]  //0 -- 1 now
		fArr2ndf); // float fArr2ndf[]); //[nDimf*nNumVec2ndf]  //0 -- 1 now

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_All_Feas_To_ARange_0_1'");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_All_Feas_To_ARange_0_1'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	fprintf(fout_PrintFeatures,"\n\n ConvertingVecs_ToBest_SeparableFeas': after 'Converting_All_Feas_To_ARange_0_1'");
	fflush(fout_PrintFeatures);

	//printf("\n\n Please press any key:"); getchar();

	nResf = SelectingBestFeas(
		fLargef, //const float fLargef,
		fepsf, //const  float fepsf,

		nDimf, //const int nDimf,
		nDimSelecMaxf, //const int nDimSelecMaxf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		fPrecisionOf_G_Const_Searchf, //const float fPrecisionOf_G_Const_Searchf,

		0.0, //const  float fBorderBelf,
		1.0, //const  float fBorderAbof,

		nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		fEfficBestSeparOfOneFeaAcceptMaxf, //const float fEfficBestSeparOfOneFeaAcceptMaxf,

		fArr1stf, //const float fArr1stf[], //0 -- 1
		fArr2ndf, //const  float fArr2ndf[], //0 -- 1
		///////////////////////////////////

		nDimSelecf, //int &nDimSelecf,

		fRatioBestMinf, //float &fRatioBestMinf,

		nPosOneFeaBestMaxf, //int &nPosOneFeaBestMaxf,

		nSeparDirectionBestMaxf, //int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestMaxf, //float &fNumArr1stSeparBestMaxf,
		fNumArr2ndSeparBestMaxf, //float &fNumArr2ndSeparBestMaxf,

		fRatioBestArrf, //float fRatioBestArrf[], //nDimSelecMaxf

		nPosFeaSeparBestArrf); // int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

	fprintf(fout_PrintFeatures, "\n\n ConvertingVecs_ToBest_SeparableFeas': after 'SelectingBestFeas, nResf = %d'", nResf);
	fflush(fout_PrintFeatures);


	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n 'ConvertingVecs_ToBest_SeparableFeas' after 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		printf("\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		fprintf(fout_lr, "\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'UNSUCCESSFUL_RETURN': please press any key:"); getchar(); // exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	else if (nResf == SUCCESSFUL_RETURN)
	{
		printf("\n\n 'ConvertingVecs_ToBest_SeparableFeas' after 'SelectingBestFeas': the number of selected features  nDimSelecf = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelecf, nDimSelecMax);
		printf("\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax' results to get a larger number of the selected features; the range is (-2.0, -1.0) ");
		printf("\n\n 'UNSUCCESSFUL_RETURN': please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas':  the number of selected features  nDimSelec = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelec, nDimSelecMax);
		fprintf(fout_lr, "\n\n  Increasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a larger number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//else if (nResf == SUCCESSFUL_RETURN)
	else if (nResf == 2)
	{
		printf("\n\n After 'SelectingBestFeas': the number of selected features nDimSelecf = %d is greater than the specified number nDimSelecMax = %d that is Ok",
			nDimSelecf, nDimSelecMax);
		printf("\n\n Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas':the number of selected features nDimSelecf = %d is greater than the specified number nDimSelecMax = %d that is Ok",
			nDimSelecf, nDimSelecMax);

		fprintf(fout_lr, "\n\n  Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	}//else if (nResf == 2)

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	printf("\n\n After 'SelectingBestFeas': fRatioBestMinf = %E, nPosOneFeaBestMaxf = %d, initial nDimSelecf = %d, nDimSelecMax = %d",
		fRatioBestMinf, nPosOneFeaBestMaxf, nDimSelecf, nDimSelecMax);
	//printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout_lr, "\n\n After 'SelectingBestFeas':fRatioBestMinf = %E, nPosOneFeaBestMaxf = %d, iniatal nDimSelecf = %d, nDimSelecMax = %d",
		fRatioBestMinf, nPosOneFeaBestMaxf, nDimSelecf, nDimSelecMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/*
	if (nDimSelecf > nDimSelecMax)
	{
		printf("\n\n  Changing nDimSelecf = %d to nDimSelecMax = %d", nDimSelecf, nDimSelecMax);

	#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n  Changing nDimSelec = %d to nDimSelecMax = %d", nDimSelec, nDimSelecMax);
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nDimSelecf = nDimSelecMax;
	}//if (nDimSelec > nDimSelecMax)
	*/

	fprintf(fout_PrintFeatures, "\n\n //////////////////////////////////////////////////////////////////////");

	//for (int iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
	for (iFeaf = 0; iFeaf < nDimSelecMax; iFeaf++)
	{
		printf("\n\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);
		//fprintf(fout_lr, "\n\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			//iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);

		fprintf(fout_PrintFeatures, "\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);
	} //for (iFea = 0; iFea < nDimSelecMax; iFea++)


	//printf("\n\n Before 'Converting_Arr_To_Selec': please press any key"); fflush(fout_PrintFeatures); getchar();
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////
	fprintf(fout_PrintFeatures, "\n\n ConvertingVecs_ToBest_SeparableFeas': before 'Converting_Arr_To_Selec' (normal)");
	fflush(fout_PrintFeatures);

	//float *fVecTrainSelec_1st = new float[nNumVecTrain_1st*nDimSelecf]; //0 -- 1 //[nDimSelecMaxf]
	nResf = Converting_Arr_To_Selec(
		nDimf, //const int nDimf,
		nDimSelecMax, //const int nDimSelecf,

		nNumOfVecs_Normal, //const int nNumVecf,

		nPosFeaSeparBestArrf, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

		fArr1stf, //const float fVecArr[],[nProdTrain_1st]
		fSelecArr1stf); //float fVecSelecArr[]) //[nNumVecTrain_1st*nDimSelec]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 1:");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 1:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)
/////////////////////////////
	fprintf(fout_PrintFeatures, "\n\n ConvertingVecs_ToBest_SeparableFeas': before 'Converting_Arr_To_Selec' (malignant)");
	fflush(fout_PrintFeatures);

	nResf = Converting_Arr_To_Selec(
		nDimf, //const int nDimf,
		nDimSelecMax, //const int nDimSelecf,

		nNumOfVecs_Malignant, //const int nNumVecf,

		nPosFeaSeparBestArrf, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

		fArr2ndf, //const float fVecArr[],[nProdTrain_2nd]
		fSelecArr2ndf); //float fVecSelecArr[]) //[nNumVecTrain_2nd*nDimSelec]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 2:");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 2:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);
		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)

	//printf("\n\n The end in 'ConvertingVecs_ToBest_SeparableFeas'");
	//printf("\n\n Please press any key"); getchar();

	delete[] fFeas_InitMinArrf;
	delete[] fFeas_InitMaxArrf;

	delete[] nFeaTheSameOrNotArrf;

	return SUCCESSFUL_RETURN;
} //int ConvertingVecs_ToBest_SeparableFeas(...
////////////////////////////////////////////////////////////////////////////////////////////

void MaxRatioOf_2NonZeroFeasFromAnArr(
	const float fLargef,
	const  float fepsf,

	const int nDimf,

	const int nNumVecs_1f,
	const int nNumVecs_2f,

	const int nFea1stf,
	const int nFea2ndf,

	const float f2Dim_1Arrf[], //0 -- 1
	const float f2Dim_2Arrf[], //0 -- 1
	///////////////////////////////////

	int &nNumOfZerosInDenom_1f,
	int &nNumOfZerosInDenom_2f,
	float &fRatioOf_2NonZeroFeasMaxf) //
{
	int
		iVecf,
		nTempf,
		nIndex1stf,
		nIndex2ndf;
	float
		fRatioMaxf = -fLargef,
		fRatioCurf,
		fFea_1stf,
		fFea_2ndf;
	//////////////////////////

	fRatioOf_2NonZeroFeasMaxf = -fLargef;

	nNumOfZerosInDenom_1f = 0;
	for (iVecf = 0; iVecf < nNumVecs_1f; iVecf++)
	{
		nTempf = iVecf * nDimf;

		nIndex1stf = nFea1stf + nTempf;
		nIndex2ndf = nFea2ndf + nTempf;

		fFea_1stf = f2Dim_1Arrf[nIndex1stf];
		fFea_2ndf = f2Dim_1Arrf[nIndex2ndf];

		if (fFea_2ndf > -fepsf && fFea_2ndf < fepsf)
		{
			nNumOfZerosInDenom_1f += 1;
			continue;
		} //if (fFea_2ndf > -fepsf && fFea_2ndf < fepsf)

		fRatioCurf = fFea_1stf / fFea_2ndf;

		if (fRatioCurf > fRatioMaxf)
			fRatioMaxf = fRatioCurf;

	} //	for (iVecf = 0; iVecf < nNumVecs_1f; iVecf++)
//////////////////////////////////

	nNumOfZerosInDenom_2f = 0;

	for (iVecf = 0; iVecf < nNumVecs_2f; iVecf++)
	{
		nTempf = iVecf * nDimf;

		nIndex1stf = nFea1stf + nTempf;
		nIndex2ndf = nFea2ndf + nTempf;

		fFea_1stf = f2Dim_2Arrf[nIndex1stf];
		fFea_2ndf = f2Dim_2Arrf[nIndex2ndf];

		if (fFea_2ndf > -fepsf && fFea_2ndf < fepsf)
		{
			nNumOfZerosInDenom_2f += 1;
			continue;
		} //if (fFea_2ndf > -fepsf && fFea_2ndf < fepsf)

		fRatioCurf = fFea_1stf / fFea_2ndf;

		if (fRatioCurf > fRatioMaxf)
			fRatioMaxf = fRatioCurf;

	} //	for (iVecf = 0; iVecf < nNumVecs_1f; iVecf++)

	fRatioOf_2NonZeroFeasMaxf = fRatioMaxf;
}//void MaxRatioOf_2NonZeroFeasFromAnArr(...

///////////////////////////////////////////////////

void RatiosOf_2feasFromAnArr(
	const float fLargef,
	const  float fepsf,

	const int nDimf,

	const int nNumVecsf,

	const int nFea1stf,
	const int nFea2ndf,

	const float f2DimArrf[], //0 -- 1

	const float fRatioOf_2NonZeroFeasMaxf, //
	///////////////////////////////////

	float fRatiosOf_2feasArrf[]) //[nNumVecsf]
{
	int
		nNumOfZerosInDenomf = 0,

		iVecf,
		nTempf,
		nIndex1stf,
		nIndex2ndf;

	float
		//fRatioMaxf = -fLarge,

		fRatioForDivisionByZerof = fRatioOf_2NonZeroFeasMaxf * CoefOfIncreaseInRatiosOf_2feasFromAnArr,

		fRatioCurf,
		fFea_1stf,
		fFea_2ndf;
	//////////////////////////

	for (iVecf = 0; iVecf < nNumVecsf; iVecf++)
	{
		nTempf = iVecf * nDimf;

		nIndex1stf = nFea1stf + nTempf;
		nIndex2ndf = nFea2ndf + nTempf;

		fFea_1stf = f2DimArrf[nIndex1stf];
		fFea_2ndf = f2DimArrf[nIndex2ndf];

		if (fFea_2ndf > -fepsf && fFea_2ndf < fepsf)
		{
			if (fFea_1stf > -fepsf && fFea_1stf < fepsf)
			{
				fRatiosOf_2feasArrf[iVecf] = 1.0;
				continue;
			}//if (fFea_1stf > -feps && fFea_1stf < feps)
			else
			{
				fRatiosOf_2feasArrf[iVecf] = fRatioForDivisionByZerof; //		
				continue;
			} //else

		} //if (fFea_2ndf > -fepsf && fFea_2ndf < fepsf)

		fRatioCurf = fFea_1stf / fFea_2ndf;

		fRatiosOf_2feasArrf[iVecf] = fRatioCurf;

		//if (fRatioCurf > fRatioMaxf)
			//fRatioMaxf = fRatioCurf;

	} //	for (iVecf = 0; iVecf < nNumVecsf; iVecf++)

//////////////////////////////////
}//void RatiosOf_2feasFromAnArr(...
//////////////////////////////////////////////

int OneBest_SeparableRatioOf_2Feas(
	const float fLargef,
	const  float fepsf,

	const int nDimf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	const float fPrecisionOf_G_Const_Searchf,

	const  float fBorderBelf,
	const  float fBorderAbof,

	const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
	const float fEfficBestSeparOfOneFeaAcceptMaxf,

	float fArr1stf[], //[nDimf*nNumVec1stf] // will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	float fArr2ndf[], //[nDimf*nNumVec2ndf] //will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
///////////////////////////////////

float &fRatioBest_ByRatioOf_2FeasMinf)

{
	void RatiosOf_2feasFromAnArr(
		const float fLargef,
		const  float fepsf,

		const int nDimf,

		const int nNumVecsf,

		const int nFea1stf,
		const int nFea2ndf,

		const float f2DimArrf[], //0 -- 1

		const float fRatioOf_2NonZeroFeasMaxf, //

		///////////////////////////////////

		float fRatiosOf_2feasArrf[]); //[nNumVecsf]

	int	Converting_All_Feas_To_ARange_0_1(
		const int nDimf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		int &nNumOfFeasWithUnusualValuesf,
		int &nNumOfFeasWith_TheSameValuesf,

		int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
		float fFeas_InitMinArrf[], //[nDimf]
		float fFeas_InitMaxArrf[], //[nDimf]

		float fArr1stf[], //[nDimf*nNumVec1stf]
		float fArr2ndf[]); //[nDimf*nNumVec2ndf]

	int	Converting_One_Fea_To_ARange_0_1(
		const int nDim1f,
		const int nDim2f,

		int &nFeaTheSameOrNotf,
		float &fFeaMinf,
		float &fFeaMaxf,

		float fOneDimArr1stf[], //[nDim1f]
		float fOneDimArr2ndf[]); //[nDim2f]

	int FindingBestSeparByRatioOfOneFea(
		const float fLargef,
		const int nDim1stf,
		const int nDim2ndf,

		const  float fepsf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,

		const float fArr1stf[], //0 -- 1
		const  float fArr2ndf[], //0 -- 1

		float &fEfficBestSeparOfOneFeaf,

		float &fPosSeparOfOneFeaBestf,

		int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparBestf,
		float &fNumArr2ndSeparBestf,

		float &fDiffSeparOfArr1stAndArr2ndBestf,
		float &fRatioOfSeparOfArr1stAndArr2ndBestf);

	//////////////////////////////////////////

	int
		nResf,
		iFeaf,
		iFea1f,
		iFea2f,
		iVecf,
		nIndexf,
		nTempf,

		nNumOfCombinationsOfPairsMaxf = (nDimf*(nDimf - 1)) / 2,
		nNumOfPairCurf = 0,
		nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		nNumOfZerosInDenom_1f,
		nNumOfZerosInDenom_2f,

		nFeaByRatioTheSameOrNotf,
		nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf;

	float
		fEfficBestSeparOfOneFeaf,
		fPosSeparOfOneFeaBestf,

		fNumArr1stSeparBestf,
		fNumArr2ndSeparBestf,

		fRatioOf_2NonZeroFeasMaxf,

		fDiffSeparOfArr1stAndArr2ndBestf,
		fRatioOfSeparOfArr1stAndArr2ndBestf,

		fFeaByRatioMinf,
		fFeaByRatioMaxf,

		fFeaCurf;
	///////////////////////////////////////

	fRatioBest_ByRatioOf_2FeasMinf = fLargef;
	////////////////////////////////////////////
	int *nFeaTheSameOrNotArrf = new int[nDimf]; //0 -- 1 //[nDimSelecMaxf]
	if (nFeaTheSameOrNotArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'nFeaTheSameOrNotArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaTheSameOrNotArrf == nullptr)

	float *fFeas_InitMinArrf = new float[nDimf]; //0 -- 1 //[nDimSelecMaxf]
	if (fFeas_InitMinArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeas_InitMinArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeas_InitMinArrf == nullptr)

	float *fFeas_InitMaxArrf = new float[nDimf]; //0 -- 1 //[nDimSelecMaxf]
	if (fFeas_InitMaxArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeas_InitMaxArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeas_InitMaxArrf == nullptr)
//////////////////////////////////////////////

	float *fRatiosOf_2Feas_1stArrf = new float[nNumVec1stf]; //0 -- 1 //[nDimSelecMaxf]
	if (fRatiosOf_2Feas_1stArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fRatiosOf_2Feas_1stArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fRatiosOf_2Feas_1stArrf == nullptr)

	float *fRatiosOf_2Feas_2ndArrf = new float[nNumVec2ndf]; //0 -- 1 //[nDimSelecMaxf]
	if (fRatiosOf_2Feas_2ndArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fRatiosOf_2Feas_2ndArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fRatiosOf_2Feas_2ndArrf == nullptr)


/////////////////////////////////////////
	printf("\n\n OneBest_SeparableRatioOf_2Feas': nDimf = %d, nNumVec1stf = %d, nNumVec2ndf = %d",
		nDimf, nNumVec1stf, nNumVec2ndf);

	fprintf(fout_PrintFeatures, "\n\n 'OneBest_SeparableRatioOf_2Feas': nDimf = %d, nNumVec1stf = %d, nNumVec2ndf = %d",
		nDimf, nNumVec1stf, nNumVec2ndf);

	/*
		fprintf(fout_PrintFeatures, "\n\n 'OneBest_SeparableRatioOf_2Feas': entry data");
		for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
		{
			fprintf(fout_PrintFeatures, "\n1, ");
			nTempf = iVecf * nDimf;
			for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;
				fFeaCurf = fArr1stf[nIndexf];

				fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);
			} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

		for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
		{
			fprintf(fout_PrintFeatures, "\n0, ");
			nTempf = iVecf * nDimf;
			for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;
				fFeaCurf = fArr2ndf[nIndexf];

				fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);
			} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

		fflush(fout_PrintFeatures);
	*/
	//printf("\n\n Please press any key:"); getchar();

/*

'fArr1stf[]' and 'fArr2ndf[]' have already been normalized

	nResf = Converting_All_Feas_To_ARange_0_1(
		nDimf, //const int nDimf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		nNumOfFeasWithUnusualValuesf, //int &nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf, //int &nNumOfFeasWith_TheSameValuesf,

		nFeaTheSameOrNotArrf, //int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
		fFeas_InitMinArrf, //float fFeas_InitMinArrf[], //[nDimf]
		fFeas_InitMaxArrf, //float fFeas_InitMaxArrf[], //[nDimf]

		fArr1stf, //float fArr1stf[], //[nDimf*nNumVec1stf]  //0 -- 1 now
		fArr2ndf); // float fArr2ndf[]); //[nDimf*nNumVec2ndf]  //0 -- 1 now

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'OneBest_SeparableRatioOf_2Feas' by 'Converting_All_Feas_To_ARange_0_1' 1");
		printf("\n\n Please press any key to exit");

		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)
*/
//printf("\n\n OneBest_SeparableRatioOf_2Feas': after 'Converting_All_Feas_To_ARange_0_1'");
//printf("\n\n Please press any key:"); getchar();

	for (iFea1f = 0; iFea1f < nDimf - 1; iFea1f++)
	{
		for (iFea2f = iFea1f + 1; iFea2f < nDimf - 1; iFea2f++)
		{
			nNumOfPairCurf += 1;
			nNumOfPairCur_Glob = nNumOfPairCurf;

			MaxRatioOf_2NonZeroFeasFromAnArr(
				fLargef, //const float fLargef,
				fepsf, //const  float fepsf,

				nDimf, //const int nDimf,

				nNumVec1stf, //const int nNumVecs_1f,
				nNumVec2ndf, //const int nNumVecs_2f,

				iFea1f, //const int nFea1stf,
				iFea2f, //const int nFea2ndf,

				fArr1stf, //const float f2Dim_1Arrf[], //0 -- 1
				fArr2ndf, //const float f2Dim_2Arrf[], //0 -- 1
				///////////////////////////////////

				nNumOfZerosInDenom_1f, //int &nNumOfZerosInDenom_1f,
				nNumOfZerosInDenom_2f, //int &nNumOfZerosInDenom_2f,
				fRatioOf_2NonZeroFeasMaxf); // float &fRatioOf_2NonZeroFeasMaxf); //

			if ((nNumOfPairCurf / 50000) * 50000 == nNumOfPairCurf)
			{
				printf("\n\n nNumOfZerosInDenom_1f = %d, nNumOfZerosInDenom_2f = %d, fRatioOf_2NonZeroFeasMaxf = %E, iFea1f = %d, iFea2f = %d",
					nNumOfZerosInDenom_1f, nNumOfZerosInDenom_2f, fRatioOf_2NonZeroFeasMaxf, iFea1f, iFea2f);

				printf("\n nNumOfPairCurf = %d, nNumOfCombinationsOfPairsMaxf = %d", nNumOfPairCurf, nNumOfCombinationsOfPairsMaxf);

				fprintf(fout_PrintFeatures, "\n\n nNumOfZerosInDenom_1f = %d, nNumOfZerosInDenom_2f = %d, fRatioOf_2NonZeroFeasMaxf = %E, iFea1f = %d, iFea2f = %d",
					nNumOfZerosInDenom_1f, nNumOfZerosInDenom_2f, fRatioOf_2NonZeroFeasMaxf, iFea1f, iFea2f);

				fprintf(fout_PrintFeatures, "\n nNumOfPairCurf = %d, nNumOfCombinationsOfPairsMaxf = %d", nNumOfPairCurf, nNumOfCombinationsOfPairsMaxf);
				fflush(fout_PrintFeatures);

			} //if ((nNumOfPairCurf / 50000) * 50000 == nNumOfPairCurf)

			RatiosOf_2feasFromAnArr(
				fLargef, //const float fLargef,
				fepsf, //const  float fepsf,

				nDimf, //const int nDimf,

				nNumVec1stf, //const int nNumVecsf,

				iFea1f, //const int nFea1stf,
				iFea2f, //const int nFea2ndf,

				fArr1stf, //const float f2DimArrf[], //0 -- 1
				fRatioOf_2NonZeroFeasMaxf, //const float fRatioOf_2NonZeroFeasMaxf, //
				///////////////////////////////////

				fRatiosOf_2Feas_1stArrf); // float fRatiosOf_2feasArrf[]) //[nNumVecsf]

			RatiosOf_2feasFromAnArr(
				fLargef, //const float fLargef,
				fepsf, //const  float fepsf,

				nDimf, //const int nDimf,

				nNumVec2ndf, //const int nNumVecsf,

				iFea1f, //const int nFea1stf,
				iFea2f, //const int nFea2ndf,

				fArr2ndf, //const float f2DimArrf[], //0 -- 1
				fRatioOf_2NonZeroFeasMaxf, //const float fRatioOf_2NonZeroFeasMaxf, //
				///////////////////////////////////

				fRatiosOf_2Feas_2ndArrf); // float fRatiosOf_2feasArrf[]) //[nNumVecsf]
///////////////////////////////////////////////////////////
			if (nNumOfPairCur_Glob == nNumOfPairCurOfInterest) //276453)
			{
				fprintf(fout_PrintFeatures, "\n\n Before 'Converting_One_Fea_To_ARange_0_1' at nNumOfPairCur_Glob == nNumOfPairCurOfInterest");

				for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
				{
					fprintf(fout_PrintFeatures, "\n Before 1: fRatiosOf_2Feas_1stArrf[%d] = %E", iVecf, fRatiosOf_2Feas_1stArrf[iVecf]);
				} //for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

				fprintf(fout_PrintFeatures, "\n 2:");
				for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
				{
					fprintf(fout_PrintFeatures, "\n Before 2: fRatiosOf_2Feas_2ndArrf[%d] = %E", iVecf, fRatiosOf_2Feas_2ndArrf[iVecf]);
				} //for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
				fflush(fout_PrintFeatures);

			} //if (nNumOfPairCur_Glob == nNumOfPairCurOfInterest)

			nResf = Converting_One_Fea_To_ARange_0_1(
				nNumVec1stf, //const int nDim1f,
				nNumVec2ndf, //const int nDim2f,

				nFeaByRatioTheSameOrNotf, //int &nFeaTheSameOrNotf,
				fFeaByRatioMinf, //float &fFeaMinf,
				fFeaByRatioMaxf, //float &fFeaMaxf,

				fRatiosOf_2Feas_1stArrf, //float fOneDimArr1stf[], //[nDim1f]
				fRatiosOf_2Feas_2ndArrf); // float fOneDimArr2ndf[]); //[nDim2f]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'OneBest_SeparableRatioOf_2Feas' by 'Converting_All_Feas_To_ARange_0_1' 2");
				printf("\n\n Please press any key to exit");

				getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			if (nNumOfPairCur_Glob == nNumOfPairCurOfInterest) //276453)
			{
				fprintf(fout_PrintFeatures, "\n\n After 'Converting_One_Fea_To_ARange_0_1': fFeaByRatioMinf = %E, fFeaByRatioMaxf = %E",
					fFeaByRatioMinf, fFeaByRatioMaxf);

			} //if (nNumOfPairCur_Glob == nNumOfPairCurOfInterest)

///////////////////////////////////

			if (nFeaByRatioTheSameOrNotf == 1)
			{

				if (nNumOfPairCur_Glob == nNumOfPairCurOfInterest) //276453)
				{
					fprintf(fout_PrintFeatures, "\n\n 'nFeaByRatioTheSameOrNotf == 1 in 'OneBest_SeparableRatioOf_2Feas' at nNumOfPairCur_Glob == nNumOfPairCurOfInterest");
					printf("\n\n 'nFeaByRatioTheSameOrNotf == 1 in 'OneBest_SeparableRatioOf_2Feas' at nNumOfPairCur_Glob == nNumOfPairCurOfInterest");
					//printf("\n\n Please press any key to exit"); getchar(); exit(1);
					fflush(fout_PrintFeatures);

				} //if (nNumOfPairCur_Glob == 276453)

				continue;
			} //if (nFeaByRatioTheSameOrNotf == 1)
			else
			{
				if (nNumOfPairCur_Glob == nNumOfPairCurOfInterest) //276453)
				{
					fprintf(fout_PrintFeatures, "\n\n Before 'FindingBestSeparByRatioOfOneFea' in 'OneBest_SeparableRatioOf_2Feas' at nNumOfPairCur_Glob == nNumOfPairCurOfInterest");

					for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
					{
						fprintf(fout_PrintFeatures, "\n 1: fRatiosOf_2Feas_1stArrf[%d] = %E", iVecf, fRatiosOf_2Feas_1stArrf[iVecf]);
					} //for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

					fprintf(fout_PrintFeatures, "\n 2:");
					for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
					{
						fprintf(fout_PrintFeatures, "\n 2: fRatiosOf_2Feas_2ndArrf[%d] = %E", iVecf, fRatiosOf_2Feas_2ndArrf[iVecf]);
					} //for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
					fflush(fout_PrintFeatures);

				} //if (nNumOfPairCur_Glob == nNumOfPairCurOfInterest)

				nResf = FindingBestSeparByRatioOfOneFea(
					fLargef, //const float fLargef,
					nNumVec1stf, //const int nDim1f,
					nNumVec2ndf, //const int nDim2f,

					fepsf, //const  float fepsf,

					fPrecisionOf_G_Const_Searchf, //const float fPrecisionOf_G_Const_Searchf,

					0.0, //const  float fBorderBelf,
					1.0, //const  float fBorderAbof,

					nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,

					fRatiosOf_2Feas_1stArrf, //const float fArr1stf[], //0 -- 1
					fRatiosOf_2Feas_2ndArrf, //const  float fArr2ndf[], //0 -- 1

					fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaf,

					fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestf,

					nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparBestf, //float &fNumArr1stSeparBestf,
					fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestf,

					fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestf,
					fRatioOfSeparOfArr1stAndArr2ndBestf); // float &fRatioOfSeparOfArr1stAndArr2ndBestf)

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'OneBest_SeparableRatioOf_2Feas' by 'FindingBestSeparByRatioOfOneFea', iFea1f = %d, iFea2f = %d", iFea1f, iFea2f);
					printf("\n\n Please press any key to exit");

					getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				}//if (nResf == UNSUCCESSFUL_RETURN)

				if (fEfficBestSeparOfOneFeaf < fRatioBest_ByRatioOf_2FeasMinf)
				{
					fRatioBest_ByRatioOf_2FeasMinf = fEfficBestSeparOfOneFeaf;

					printf("\n\n 'OneBest_SeparableRatioOf_2Feas': a new fRatioBest_ByRatioOf_2FeasMinf = %E, iFea1f = %d, iFea2f = %d",
						fRatioBest_ByRatioOf_2FeasMinf, iFea1f, iFea2f);
					printf("\n nNumOfPairCurf = %d, nNumOfCombinationsOfPairsMaxf = %d", nNumOfPairCurf, nNumOfCombinationsOfPairsMaxf);

					fprintf(fout_PrintFeatures, "\n\n 'OneBest_SeparableRatioOf_2Feas': a new fRatioBest_ByRatioOf_2FeasMinf = %E, iFea1f = %d, iFea2f = %d",
						fRatioBest_ByRatioOf_2FeasMinf, iFea1f, iFea2f);
					fprintf(fout_PrintFeatures, "\n nNumOfPairCurf = %d, nNumOfCombinationsOfPairsMaxf = %d", nNumOfPairCurf, nNumOfCombinationsOfPairsMaxf);

					fflush(fout_PrintFeatures);
				} // if (fEfficBestSeparOfOneFeaf < fRatioBest_ByRatioOf_2FeasMinf)

				if (fEfficBestSeparOfOneFeaf < fRatioBest_ByRatioOf_2FeasMin_ToPrint)
				{
					//	printf("\n\n 'OneBest_SeparableRatioOf_2Feas': a next fRatioBest_ByRatioOf_2FeasMinf = %E < fRatioBest_ByRatioOf_2FeasMin_ToPrint = %E, iFea1f = %d, iFea2f = %d",
						//	fRatioBest_ByRatioOf_2FeasMinf, fRatioBest_ByRatioOf_2FeasMin_ToPrint, iFea1f, iFea2f);
					//	printf("\n nNumOfPairCurf = %d, nNumOfCombinationsOfPairsMaxf = %d", nNumOfPairCurf, nNumOfCombinationsOfPairsMaxf);

					fprintf(fout_PrintFeatures, "\n\n 'OneBest_SeparableRatioOf_2Feas': a next fRatioBest_ByRatioOf_2FeasMinf = %E < fRatioBest_ByRatioOf_2FeasMin_ToPrint = %E, iFea1f = %d, iFea2f = %d",
						fRatioBest_ByRatioOf_2FeasMinf, fRatioBest_ByRatioOf_2FeasMin_ToPrint, iFea1f, iFea2f);

					fprintf(fout_PrintFeatures, "\n nNumOfPairCurf = %d, nNumOfCombinationsOfPairsMaxf = %d", nNumOfPairCurf, nNumOfCombinationsOfPairsMaxf);

					fflush(fout_PrintFeatures);
				} // if (fEfficBestSeparOfOneFeaf < fRatioBest_ByRatioOf_2FeasMin_ToPrint)

				if ((nNumOfPairCurf / 50000) * 50000 == nNumOfPairCurf)
				{
					printf("\n\n So far fRatioBest_ByRatioOf_2FeasMinf = %E, iFea1f = %d, iFea2f = %d",
						fRatioBest_ByRatioOf_2FeasMinf, iFea1f, iFea2f);

					printf("\n nNumOfPairCurf = %d, nNumOfCombinationsOfPairsMaxf = %d", nNumOfPairCurf, nNumOfCombinationsOfPairsMaxf);
				} //if ((nNumOfPairCurf / 100) * 100 == nNumOfPairCurf)

			} //else

			if (nNumOfPairCur_Glob == nNumOfPairCurOfInterest) //276453)
			{
				printf("\n\n The end in 'OneBest_SeparableRatioOf_2Feas' at nNumOfPairCur_Glob == nNumOfPairCurOfInterest, fEfficBestSeparOfOneFeaf = %E",
					fEfficBestSeparOfOneFeaf);
				fprintf(fout_PrintFeatures, "\n\n The end in 'OneBest_SeparableRatioOf_2Feas' at nNumOfPairCur_Glob == nNumOfPairCurOfInterest, fEfficBestSeparOfOneFeaf = %E",
					fEfficBestSeparOfOneFeaf);

				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
				fflush(fout_PrintFeatures);

			} //if (nNumOfPairCur_Glob == 276453)

		}//for (iFea2f = iFea1f + 1; iFea2f < nDimf - 1; iFea2f++)

	}//for (iFea1f = 0; iFea1f < nDimf -1; iFea1f++)
//////////////////////////////////////////

	delete[] fRatiosOf_2Feas_1stArrf;
	delete[] fRatiosOf_2Feas_2ndArrf;

	delete[] fFeas_InitMinArrf;
	delete[] fFeas_InitMaxArrf;

	delete[] nFeaTheSameOrNotArrf;

	return SUCCESSFUL_RETURN;
} //int OneBest_SeparableRatioOf_2Feas(...
//////////////////////////////////////////////////////////////

int HistogramEqualization_ForColorFormatOfGray(

	//GRAYSCALE_IMAGE *sGrayscale_Image)
	COLOR_IMAGE *sColor_Imagef)
{
	int
		nNumOfValidPixelsTotf = 0,

		nLookUptableArrf[nNumOfHistogramBinsStat],
		nNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat],

		nCumulatNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat],

		nWidthImagef = sColor_Imagef->nWidth,
		nLenImagef = sColor_Imagef->nLength,

		nIndexOfPixelCurf,

		nIntensityCurf,

		nIntensityMinf = nLarge,
		nIntensityMaxf = -nLarge,

		iIntensityf,
		iWidf,
		iLenf;

	float
		fCumulProbOverIntensitiesArrf[nNumOfHistogramBinsStat];

	for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		nNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;
		nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;

		fCumulProbOverIntensitiesArrf[iIntensityf] = 0.0;

		nLookUptableArrf[iIntensityf] = 0;

	} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	printf("\n\n Equalization: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);
	fprintf(fout_PrintFeatures, "\n\n Equalization: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);

	//printf("\n\n Please press any key to continue:");
	//fflush(fout_PrintFeatures); getchar();
	//////////////////////////////////////////////////////////////////////////////
	for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLenMax);

			if (nIndexOfPixelCurf >= nImageAreaMax)
			{
				printf("\n\n An error: nIndexOfPixelCurf = %d >= nImageAreaMax = %d", nIntensityCurf, nImageAreaMax);

				fprintf(fout_PrintFeatures, "\n\n An error:  nIndexOfPixelCurf = %d >= nImageAreaMax = %d", nIntensityCurf, nImageAreaMax);
				printf("\n\n Please press any key to exit:");
				fflush(fout_PrintFeatures); getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			} // if (nIndexOfPixelCurf >= nImageAreaMax)

//all colors have the same intensity
			//if (sColor_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
			if (sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] >= nIntensityOfBackground)
				continue;

			nIntensityCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat)
			{
				printf("\n\n An error: the grayscale intensity nIntensityCurf = %d", nIntensityCurf);

				fprintf(fout_PrintFeatures, "\n\n An error: the grayscale intensity nIntensityCurf = %d", nIntensityCurf);
				printf("\n\n Please press any key to exit:");
				fflush(fout_PrintFeatures); getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			} //if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat)

			if (nIntensityCurf < nIntensityMinf)
				nIntensityMinf = nIntensityCurf;

			if (nIntensityCurf > nIntensityMaxf)
				nIntensityMaxf = nIntensityCurf;

			nNumOfValidPixelsForIntensitiesArrf[nIntensityCurf] += 1;

		} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
	} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

	for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] += nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf - 1] +
			nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

		nNumOfValidPixelsTotf += nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

	} // for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	printf("\n\n Equalization: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);
	fprintf(fout_PrintFeatures, "\n\n Equalization: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);

	//printf("\n\n Please press any key to continue:");
//	fflush(fout_PrintFeatures); getchar();

	if (nNumOfValidPixelsTotf <= 0)
	{
		printf("\n\n An error: the number of valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);
		fprintf(fout_PrintFeatures, "\n\n An error: the number  valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);

		printf("\n\n Please press any key to exit:");
		fflush(fout_PrintFeatures); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfValidPixelsTotf <= 0)

	for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		fCumulProbOverIntensitiesArrf[iIntensityf] = (float)(nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf]) / (float)(nNumOfValidPixelsTotf);

		nLookUptableArrf[iIntensityf] = (int)((fCumulProbOverIntensitiesArrf[iIntensityf])*(float)(nIntensityStatMax));

		if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)
		{
			printf("\n\n An error in HistogramEqualization_ForColorFormatOfGray: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);
			fprintf(fout_PrintFeatures, "\n\n An error in HistogramEqualization_ForColorFormatOfGray: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);

			printf("\n\n Please press any key to exit:");
			fflush(fout_PrintFeatures); getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} // if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)

		//printf("\n\n Equalization: nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);
		//fprintf(fout_PrintFeatures, "\n Equalization: nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);

	} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	//printf("\n\n Please press any key to continue:");
//	fflush(fout_PrintFeatures); getchar();
///////////////////////////////////////////////////////////////////////////////////////////////////
	//printf( "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);
	//fprintf(fout_PrintFeatures, "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);


	for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLenMax);

			if (nIndexOfPixelCurf >= nImageAreaMax)
			{
				printf("\n\n An error 2: nIndexOfPixelCurf = %d >= nImageAreaMax = %d", nIntensityCurf, nImageAreaMax);

				fprintf(fout_PrintFeatures, "\n\n An error 2:  nIndexOfPixelCurf = %d >= nImageAreaMax = %d", nIntensityCurf, nImageAreaMax);
				printf("\n\n Please press any key to exit:");
				fflush(fout_PrintFeatures); getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			} // if (nIndexOfPixelCurf >= nImageAreaMax)

			//if (sColor_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
			if (sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] >= nIntensityOfBackground)
				continue;

			nIntensityCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nLookUptableArrf[nIntensityCurf];

			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			//fprintf(fout_PrintFeatures, "\n\n iWidf = %d, iLenf = %d, sColor_Imagef->nRed_Arr[%d] = %d",
				//iWidf, iLenf, nIndexOfPixelCurf, sColor_Imagef->nRed_Arr[nIndexOfPixelCurf]);

			//fprintf(fout_PrintFeatures, "\n nIntensityCurf = %d, nLookUptableArrf[%d] = %d", nIntensityCurf, nIntensityCurf, nLookUptableArrf[nIntensityCurf]);

		} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
	} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

	//printf("\n\n The end of 'HistogramEqualization_ForColorFormatOfGray': please press any key to continue");getchar();
	fflush(fout_PrintFeatures);

	return SUCCESSFUL_RETURN;
} //int HistogramEqualization_ForColorFormatOfGray(....
///////////////////////////////////////////////////////////////////////////

int	PositionsOfMeaningfulFeas_OneImage(
	const int nDimf,

	const float fArr_1Dimf[], //[nDimf*nNumVec1stf]

	int &nNumOfFeasWithMeaningfulValuesf,

	int nFeaTheSameOrNotArrf[]) //[nDimf] // 1-- the same, 0-- not
{

	int
		nNumOfFeasWith_TheSameValuesf,
		iFeaf;

	float
		fFeaCurf;

	//////////////////////////////////////
	nNumOfFeasWith_TheSameValuesf = 0;

	//fprintf(fout_PrintFeatures, "\n\n 'PositionsOfMeaningfulFeas_OneImage': nDimf = %d, nNumVec1stf = %d, nNumVec2ndf = %d", nDimf, nNumVec1stf, nNumVec2ndf);
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nFeaTheSameOrNotArrf[iFeaf] = 0; //not the same initially
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

//printing all fea vecs+

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeaCurf = fArr_1Dimf[iFeaf];

		if (fFeaCurf > -feps && fFeaCurf < feps)
		{
			nNumOfFeasWith_TheSameValuesf += 1;
			nFeaTheSameOrNotArrf[iFeaf] = 1; //0
		} //if (fFeaCurf > -feps && fFeaCurf < feps)

	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
////////////////////////////////////////////////////////////

	nNumOfFeasWithMeaningfulValuesf = nDimf - nNumOfFeasWith_TheSameValuesf;

	printf("\n\n 'PositionsOfMeaningfulFeas_OneImage': the total nNumOfFeasWithMeaningfulValuesf = %d, nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d",
		nNumOfFeasWithMeaningfulValuesf, nNumOfFeasWith_TheSameValuesf, nDimf);
	//fprintf(fout_PrintFeatures, "\n\n 'PositionsOfMeaningfulFeas_OneImage': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);

	return SUCCESSFUL_RETURN;
} // int PositionsOfMeaningfulFeas_OneImage(...
////////////////////////////////////////////////

int	PositionsOfMeaningfulFeasForRegularOrTripleCooc(
	const int nDimf,

	const int nNumVecf, //nNumVecsForPositionsOfMeaningfulFeasForRegularOrTripleCooc == 3

	int &nNumOfFeasWithMeaningfulValues_Finf,

	int nFeaTheSameOrNot_FinArrf[]) //[nDimf] // 1-- the same, 0-- not
{

	int Reading_AColorImage(
		const Image& image_in,

		COLOR_IMAGE *sColor_Image);

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
	int CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nDistToCoocPointsMinf,
		const int nDistToCoocPointsMaxf,

		float fCoocFeasOf_Image_By_LenWidShift_At_AllDist_Arrf[]);
	//4*nCoocSizeMax*[(nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*( nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1)]/2
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	int TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nDistToCoocPointsMinf,
		const int nDistToCoocPointsMaxf,

		float fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf[]);// [nNumOfTripleCoocFeasOf_ImageTot]

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

	int PositionsOfMeaningfulFeas_OneImage(
		const int nDimf,

		const float fArr_1Dimf[], //[nDimf*nNumVec1stf]

		int &nNumOfFeasWithMeaningfulValuesf,

		int nFeaTheSameOrNotArrf[]); //[nDimf] // 1-- the same, 0-- not
	////////////////////////////////////////////////

	int
		nResf,
		nNumOfFeasWith_TheSameValuesf,
		nIndexf,
		iVecf,
		iFeaf;

	int
		nIndexOfPixelCur,

		nIndex_AllVecs,
		nInitFea_ForAllVecs,

		////////////////////////
		nNumOfFeasWithUnusualValuef,

		nNumOfFeasWithMeaningfulValuesf,

#ifndef USING_TRIPLE_COOCURRENCE_ONLY

		nFeaTheSameOrNotArrf[nDim], //[nDimf] // 1-- the same, 0-- not
		nNumOfFeas_ForAll_VecsTotf = nDim * nNumVecsForPositionsOfMeaningfulFeasFinal,

#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

/////////////////
		nNumOfProcessedFilesTot;

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	int *nFeaTheSameOrNotArrf;
	nFeaTheSameOrNotArrf = new int[nNumOfTripleCoocFeasOf_ImageTot];

	if (nFeaTheSameOrNotArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'nFeaTheSameOrNotArrf' in 'main'");

		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaTheSameOrNotArrf == nullptr)

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

	///////////////////////

	float
#ifndef USING_TRIPLE_COOCURRENCE_ONLY
		fCoocFeasOf_OneImage_By_LenWidShift_At_AllDist_Arrf[nDim],
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

			fFeaCurf;

#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		float *fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf;
	fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf = new float[nNumOfTripleCoocFeasOf_ImageTot];

	if (nFeaTheSameOrNotArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf' in 'main'");

		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);
#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNotArrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		return UNSUCCESSFUL_RETURN;
	} //if (fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf == nullptr)
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY


	///////////////////////////////
	Image image_PositionsOfMeaningfulFeas;

	//image_in.read("output_ForMultifractal.png");
/////////////////////////////////////////////////////////////
	DIR *pDIR_Posit;
	struct dirent *entry_Posit;
	/////////////////////////////////////////

	nNumOfFeasWithMeaningfulValues_Finf = 0;

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
	for (iFeaf = 0; iFeaf < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFeaf++)
	{
		nFeaTheSameOrNot_FinArrf[iFeaf] = 1; //all feas are 0s
	} //for (iFeaf = 0; iFeaf < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFeaf++)
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	for (iFeaf = 0; iFeaf < nNumOfTripleCoocFeasOf_ImageTot; iFeaf++)
	{
		nFeaTheSameOrNot_FinArrf[iFeaf] = 1; //all feas are 0s
	} //for (iFeaf = 0; iFeaf < nNumOfTripleCoocFeasOf_ImageTot; iFeaf++)
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//char cInput_DirectoryOf_Images[150] = "D:\\Imago\\Images\\spleen\\VictorsTestImage\\"; //copy to if (pDIR_Posit = opendir(.. as well
	char cInput_DirectoryOf_Images[200] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\3_ImagesForFindingMeaningfulFeas\\"; //copy to if (pDIR_Posit = opendir(.. as well
	char cFileName_inDirectoryOf_Images[200]; //150

////////////////////////////////////
	COLOR_IMAGE sColor_Imagef; //

	nNumOfProcessedFilesTot = 0;

	/*
		char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI_50_Selec\\"; //copy to if (pDIR = opendir(.. as well

				 if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI_50_Selec\\"))
				{
					while (entry = readdir(pDIR))
					{
						if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
						{

							memset(cFileName_iinDirectoryOf_Normal_Images, '\0', sizeof(cFileName_iinDirectoryOf_Normal_Images));

							strcpy(cFileName_iinDirectoryOf_Normal_Images, cInput_DirectoryOf_Normal_Images);

							strcat(cFileName_iinDirectoryOf_Normal_Images, entry->d_name);

							printf("\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);
							//fprintf(fout_PrintFeatures,"\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);
	*/
	//check out '#define nNumVecsForPositionsOfMeaningfulFeasForRegularOrTripleCooc'!
	//if (pDIR_Posit = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\"))
	if (pDIR_Posit = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\3_ImagesForFindingMeaningfulFeas\\"))
	{
		while (entry_Posit = readdir(pDIR_Posit))
		{
			if (strcmp(entry_Posit->d_name, ".") != 0 && strcmp(entry_Posit->d_name, "..") != 0)
			{
				nNumOfProcessedFilesTot += 1;

				if (nNumOfProcessedFilesTot > nNumOfVecs_Normal)
				{
					printf("\n\n 'PositionsOfMeaningfulFeasForRegularOrTripleCooc', an error in counting the number of normal images: nNumOfProcessedFilesTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFilesTot, nNumOfVecs_Normal);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFilesTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFilesTot, nNumOfVecs_Normal);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNotArrf;
					delete[] fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFilesTot > nNumOfVecs_Normal)

			//	Initializing_AFloatVec_To_Zero(
				//	nNumOf_CoocExtendedFeas_OneDimTot, //const int nDimf,
					//fOneDim_CoocExtendedArr); // float fVecArrf[]); //[nDimf]

//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n %s\n", entry_Posit->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_inDirectoryOf_Images, '\0', sizeof(cFileName_inDirectoryOf_Images));

				strcpy(cFileName_inDirectoryOf_Images, cInput_DirectoryOf_Images);

				strcat(cFileName_inDirectoryOf_Images, entry_Posit->d_name);

				printf("\n 'PositionsOfMeaningfulFeasForRegularOrTripleCooc', concatenated input file: %s, nNumOfProcessedFilesTot = %d\n", cFileName_inDirectoryOf_Images, nNumOfProcessedFilesTot);
				//fprintf(fout_PrintFeatures,"\n Concatenated input file: %s, nNumOfProcessedFilesTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFilesTot);
////////////////////////////////////////
				image_PositionsOfMeaningfulFeas.read(cFileName_inDirectoryOf_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n Concatenated input file: %s\n", cFileName_iinDirectoryOf_Normal_Images);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n 'PositionsOfMeaningfulFeasForRegularOrTripleCooc', press any key 1:");	fflush(fout_lr);  getchar();
//////////////////////////////////////////////////////////////

				nResf = Reading_AColorImage(
					image_PositionsOfMeaningfulFeas, //const Image& image_in,

					&sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PositionsOfMeaningfulFeasForRegularOrTripleCooc' for 'Reading_AColorImage' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNotArrf;
					delete[] fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					return UNSUCCESSFUL_RETURN;
				} // if (nResf == UNSUCCESSFUL_RETURN)

				printf("\n\n So far nNumOfProcessedFilesTot = %d", nNumOfProcessedFilesTot);

#ifdef USING_HISTOGRAM_EQUALIZATION

#ifdef PRINT_HISTOGRAM_STATISTICS
				nRes = Histogram_Statistics_ColorActuallyGrayscale_Image(

					//const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageAreaMax]
					&sColor_Imagef, //const COLOR_IMAGE *sColor_Imageff,

					fPercentsOfHistogramIntervalsArrf, //float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

					fPercentagesAboveThresholds_InHistogramArrf, //float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

					fMeanOfHistf, //float &fMeanf,
					fStDevOfHistf, //float &fStDevf,
					fSkewnessOfHistf, //float &fSkewnessf,
					fKurtosisfOfHistf); // float &fKurtosisf);

				printf("\n\n Histogram statistics before 'HistogramEqualization': fMeanOfHistf = %E, fStDevOfHistf = %E", fMeanOfHistf, fStDevOfHistf);

				for (iHistf = 0; iHistf < nNumOfHistogramIntervals_ForCooc; iHistf++)
				{
					printf("\n Before: fPercentsOfHistogramIntervalsArrf[%d] = %E", iHistf, fPercentsOfHistogramIntervalsArrf[iHistf]);
				} //for (iHistf = 0; iHistf < nNumOfHistogramIntervals_ForCooc; iHistf++)

#endif //#ifdef PRINT_HISTOGRAM_STATISTICS

				nRes = HistogramEqualization_ForColorFormatOfGray(

					//GRAYSCALE_IMAGE *sGrayscale_Image)
					&sColor_Imagef);// COLOR_IMAGE *sColor_Imageff);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PositionsOfMeaningfulFeasForRegularOrTripleCooc' for 'HistogramEqualization_ForColorFormatOfGray' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					delete[] fAllFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fAllFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fAllFeasForAll_NormalVecs_Copy_Arr;
					delete[] fAllFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

#ifdef PRINT_HISTOGRAM_STATISTICS

				nRes = Histogram_Statistics_ColorActuallyGrayscale_Image(

					//const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageAreaMax]
					&sColor_Imagef, //const COLOR_IMAGE *sColor_Imageff,

					fPercentsOfHistogramIntervalsArrf, //float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

					fPercentagesAboveThresholds_InHistogramArrf, //float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

					fMeanOfHistf, //float &fMeanf,
					fStDevOfHistf, //float &fStDevf,
					fSkewnessOfHistf, //float &fSkewnessf,
					fKurtosisfOfHistf); // float &fKurtosisf);

				printf("\n\n Histogram statistics after 'HistogramEqualization': fMeanOfHistf = %E, fStDevOfHistf = %E", fMeanOfHistf, fStDevOfHistf);

				for (iHistf = 0; iHistf < nNumOfHistogramIntervals_ForCooc; iHistf++)
				{
					printf("\n After: fPercentsOfHistogramIntervalsArrf[%d] = %E", iHistf, fPercentsOfHistogramIntervalsArrf[iHistf]);
				} //for (iHistf = 0; iHistf < nNumOfHistogramIntervals_ForCooc; iHistf++)

				printf("\n\n Press any key to ontinue");	getchar();

#endif //#ifdef PRINT_HISTOGRAM_STATISTICS

#endif //#ifdef USING_HISTOGRAM_EQUALIZATION
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
				nResf = CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess(
					//the image is supposed to be grayscale
					&sColor_Imagef, //const COLOR_IMAGE *sColor_Imageff,

					nDistToCoocPointsMin, //const int nDistToCoocPointsMinf,
					nDistToCoocPointsMax, //const int nDistToCoocPointsMaxf,

					fCoocFeasOf_OneImage_By_LenWidShift_At_AllDist_Arrf); // float fCoocFeasOf_Nor_Image_By_LenWidShift_At_AllDist_Arrf[])
					//4*nCoocSizeMax*[(nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*( nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1)]/2

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PositionsOfMeaningfulFeasForRegularOrTripleCooc' by 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess'");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

				nResf = PositionsOfMeaningfulFeas_OneImage(
					nDim, //const int nDimf,

					fCoocFeasOf_OneImage_By_LenWidShift_At_AllDist_Arrf, //const float fArr_1Dimf[], //[nDimf*nNumVec1stf]

					nNumOfFeasWithMeaningfulValuesf, //int &nNumOfFeasWithMeaningfulValuesf,

					nFeaTheSameOrNotArrf); // int nFeaTheSameOrNotArrf[]); //[nDimf] // 1-- the same, 0-- not

						//////////////////////////////////////////////
				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PositionsOfMeaningfulFeasForRegularOrTripleCooc' by 'PositionsOfMeaningfulFeas_OneImage'");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

				/////////////////////////////////////////////
				for (iFeaf = 0; iFeaf < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFeaf++)
				{
					//nFeaTheSameOrNotArrf[iFeaf] == 0 means that there is a nonzero fea at 'iFeaf'
					if (nFeaTheSameOrNot_FinArrf[iFeaf] == 1 && nFeaTheSameOrNotArrf[iFeaf] == 0)
					{
						nNumOfFeasWithMeaningfulValues_Finf += 1;
						nFeaTheSameOrNot_FinArrf[iFeaf] = 0;

					} //if (nFeaTheSameOrNot_FinArrf[iFeaf] == 1 && nFeaTheSameOrNotArrf[iFeaf] == 0)
				} //for (iFeaf = 0; iFeaf < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFeaf++)

#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
				nResf = TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess(
					//the image is supposed to be grayscale
					&sColor_Imagef, //const COLOR_IMAGE *sColor_Imageff,

					nDistToCoocPointsMin, //const int nDistToCoocPointsMinf,
					nDistToCoocPointsMax, //const int nDistToCoocPointsMaxf,

					fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf); // float fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf[]);// [nNumOfTripleCoocFeasOf_ImageTot]

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PositionsOfMeaningfulFeasForRegularOrTripleCooc' by 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess'");

					printf("\n\n Press any key to exit");	getchar(); exit(1);
#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNotArrf;
					delete[] fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

				nResf = PositionsOfMeaningfulFeas_OneImage(
					nNumOfTripleCoocFeasOf_ImageTot, //const int nDimf,

					fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf, //const float fArr_1Dimf[], //[nDimf*nNumVec1stf]

					nNumOfFeasWithMeaningfulValuesf, //int &nNumOfFeasWithMeaningfulValuesf,

					nFeaTheSameOrNotArrf); // int nFeaTheSameOrNotArrf[]); //[nDimf] // 1-- the same, 0-- not

						//////////////////////////////////////////////
				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'PositionsOfMeaningfulFeasForRegularOrTripleCooc' by 'PositionsOfMeaningfulFeas_OneImage'");

					printf("\n\n Press any key to exit");	getchar(); exit(1);
#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNotArrf;
					delete[] fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

				/////////////////////////////////////////////
				for (iFeaf = 0; iFeaf < nNumOfTripleCoocFeasOf_ImageTot; iFeaf++)
				{
					//nFeaTheSameOrNotArrf[iFeaf] == 0 means that there is a nonzero fea at 'iFeaf'
					if (nFeaTheSameOrNot_FinArrf[iFeaf] == 1 && nFeaTheSameOrNotArrf[iFeaf] == 0)
					{
						nNumOfFeasWithMeaningfulValues_Finf += 1;
						nFeaTheSameOrNot_FinArrf[iFeaf] = 0;

					} //if (nFeaTheSameOrNot_FinArrf[iFeaf] == 1 && nFeaTheSameOrNotArrf[iFeaf] == 0)
				} //for (iFeaf = 0; iFeaf < nNumOfTripleCoocFeasOf_ImageTot; iFeaf++)

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

				printf("\n 'PositionsOfMeaningfulFeasForRegularOrTripleCooc': nNumOfProcessedFilesTot = %d, nNumOfFeasWithMeaningfulValuesf = %d",
					nNumOfProcessedFilesTot, nNumOfFeasWithMeaningfulValuesf);

			////////////////////////////////////////////////

				delete[] sColor_Imagef.nRed_Arr;
				delete[] sColor_Imagef.nGreen_Arr;
				delete[] sColor_Imagef.nBlue_Arr;

				delete[] sColor_Imagef.nLenObjectBoundary_Arr;
				delete[] sColor_Imagef.nIsAPixelBackground_Arr;

				//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFilesTot = %d, please press any key to switch to the next image", nNumOfProcessedFilesTot);  getchar(); //exit(1);
				printf("\n 'PositionsOfMeaningfulFeasForRegularOrTripleCooc': the end of nNumOfProcessedFilesTot = %d, nNumVecsForPositionsOfMeaningfulFeasFinal = %d, nNumOfFeasWithMeaningfulValues_Finf = %d; going to to the next image",
					nNumOfProcessedFilesTot, nNumVecsForPositionsOfMeaningfulFeasFinal, nNumOfFeasWithMeaningfulValues_Finf);

				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry_Posit->d_name, ".") != 0 && strcmp(entry_Posit->d_name, "..") != 0)
		}//while (entry_Posit = readdir(pDIR_Posit))

		closedir(pDIR_Posit);
	}//if (pDIR_Posit = opendir(""D:\\Imago\\Images\\spleen\\Genas_Images_June26\\3_ImagesForFindingMeaningfulFeas\\"))
	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n An error: the directory of images for PositionsOfMeaningfulFeas can not be opened");

		printf("\n\n Press any key to exit"); fflush(fout_PrintFeatures);	getchar(); exit(1);
		perror("");

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNotArrf;
		delete[] fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

#ifdef PRINT_ORIGINAL_FEAS
	fprintf(fout_PrintFeatures, "\n\n///////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Original feas: normal images");
	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n Normal (orig):  iVec = %d, ", iVec);
		nTempf = iVec * nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot;

		for (iFeaf = 0; iFeaf < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFeaf++)
		{

			nIndex = iFeaf + nTempf;
			fFeaCur = fCoocFeasOf_Nor_Image_By_LenWidShift_At_AllDist_Arrf[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCur);

		}//for (iFeaf = 0; iFeaf < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFeaf++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
#endif //PRINT_ORIGINAL_FEAS

	if (nNumOfProcessedFilesTot != nNumVecsForPositionsOfMeaningfulFeasFinal)
	{
		printf("\n\n An error in 'PositionsOfMeaningfulFeasForRegularOrTripleCooc': nNumOfProcessedFilesTot = %d != nNumVecsForPositionsOfMeaningfulFeasFinal = %d",
			nNumOfProcessedFilesTot,nNumVecsForPositionsOfMeaningfulFeasFinal);

		printf("\n\n Press any key to exit");	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNotArrf;
		delete[] fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfProcessedFilesTot != nNumVecsForPositionsOfMeaningfulFeasFinal)

//////////////////////////////////////////
	//printf("\n\n 'PositionsOfMeaningfulFeasForRegularOrTripleCooc': the total nNumOfFeasWithMeaningfulValuesf = %d, nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d",
		//nNumOfFeasWithMeaningfulValuesf, nNumOfFeasWith_TheSameValuesf, nDimf);
	//fprintf(fout_PrintFeatures, "\n\n 'PositionsOfMeaningfulFeasForRegularOrTripleCooc': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	delete[] nFeaTheSameOrNotArrf;
	delete[] fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

	return SUCCESSFUL_RETURN;
} // int PositionsOfMeaningfulFeasForRegularOrTripleCooc(...
///////////////////////////////////////////////////////////

int	Converting_OneFeaVec_ToAVecWithSelecFeas(
	const int nDim_1f,
	const int nDim_2f, // == nNumOfFeasWithMeaningfulValues_Finf < nDim_1f

	const float f_1Arrf[], //[nDim_1f]

	const int nFeaTheSameOrNot_FinArrf[], //[nDim_1f]

	float f_SelecArrf[]) //[nDim_2f]

{
	int
		nNumOfActualFeasTotf = 0,
		iFeaf;

	printf("\n\n 'Converting_OneFeaVec_ToAVecWithSelecFeas': nDim_1f = %d, nDim_2f = %d",
		nDim_1f, nDim_2f);

	for (iFeaf = 0; iFeaf < nDim_1f; iFeaf++)
	{
		if (nFeaTheSameOrNot_FinArrf[iFeaf] == 0)
		{
			nNumOfActualFeasTotf += 1;

			if (nNumOfActualFeasTotf > nDim_2f)
			{
				printf("\n\n An error in 'Converting_OneFeaVec_ToAVecWithSelecFeas': nNumOfActualFeasTotf = %d > nDim_2f = %d, iFeaf = %d",
					nNumOfActualFeasTotf, nDim_2f, iFeaf);

				printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nNumOfActualFeasTotf > nDim_2f)

			f_SelecArrf[nNumOfActualFeasTotf - 1] = f_1Arrf[iFeaf];

		}//if (nFeaTheSameOrNot_FinArrf[iFeaf] == 0)

	}//for (iFeaf = 0; iFeaf < nDim_1f; iFeaf++)

	if (nNumOfActualFeasTotf != nDim_2f)
	{
		printf("\n\n An error in 'Converting_OneFeaVec_ToAVecWithSelecFeas': nNumOfActualFeasTotf = %d != nDim_2f = %d",
			nNumOfActualFeasTotf, nDim_2f);

		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfActualFeasTotf != nDim_2f)

	return SUCCESSFUL_RETURN;
} // int Converting_OneFeaVec_ToAVecWithSelecFeas(...
/////////////////////////////////////////////////////////////////////////

int TripleCoocOf_Image_ByOneCouple_OfLenWidShifts(
	//the image is supposed to be grayscale
	const COLOR_IMAGE *sColor_Imagef,

	const int nLenShift_1f,
	const int nWidShift_1f,

	const int nLenShift_2f,
	const int nWidShift_2f,

	TRIPLE_COOC_OF_IMAGE_BY_ONE_COUPLE_OF_LEN_WID_SHIFTS *sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf)
{
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int
		nResf,

		nAreaOfTheWholeImagef, // = nImageWidth*nImageHeight,

		nIndexOfIntensityf,

		nIndexOfPixel_1f,
		nIndexOfPixel_2f,
		nIndexOfPixel_3f,

		nIndexOfPixelMaxf,
		iIntensity_1f,
		iIntensity_2f,
		iIntensity_3f,

		nIntensity_1f,
		nIntensity_2f,
		nIntensity_3f,

		nIntensityTempf, 
		nIndexOfIntensityMaxf = nTripleCoocSizeMax,
		nTempIntensity_1f,
		nTempIntensity_2f,

		nTempPixel_1f,
		nTempPixel_2f,
		nTempPixel_3f,

		iLenf,
		iWidf,

		nLenBeginf,
		nLenEndf,

		nWidBeginf,
		nWidEndf,

		nNumOfValidObjectPixelsf = 0,
		nRedf,

		nImageWidthf,
		nImageHeightf;
	/////////////

	sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->nLenShift_1 = nLenShift_1f;
	sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->nWidShift_1 = nWidShift_1f;

	sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->nLenShift_2 = nLenShift_2f;
	sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->nWidShift_2 = nWidShift_2f;
////////////////////////////////////

	//nTempPixel_1f = 1 * sColor_Imagef->nLength;
	//nIndexOfPixel_1f = 165 + nTempPixel_1f;
	//nIntensityTempf = sColor_Imagef->nRed_Arr[1011];
	//printf("\n\n 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' (test) 1: nIndexOfPixel_1f = %d,  nIntensityTempf = %d",nIndexOfPixel_1f, nIntensityTempf);



	for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)
	{
		//nTempIntensityf = iIntensity_1f * nNumOfGrayLevelIntensities;
		nTempIntensity_1f = iIntensity_1f * nNumOfGrayLevelIntensities*nNumOfGrayLevelIntensities;

		for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)
		{
			nTempIntensity_2f = iIntensity_2f * nNumOfGrayLevelIntensities;

			for (iIntensity_3f = 0; iIntensity_3f < nNumOfGrayLevelIntensities; iIntensity_3f++)
			{
				//nIndexOfIntensityf = iIntensity_2f + nTempIntensityf;
				nIndexOfIntensityf = iIntensity_3f + nTempIntensity_2f + nTempIntensity_1f;

				if (nIndexOfIntensityf < 0 || nIndexOfIntensityf >= nIndexOfIntensityMaxf)
				{
					printf("\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 1: nIndexOfIntensityf = %d >= nIndexOfIntensityMaxf = %d",
						nIndexOfIntensityf, nIndexOfIntensityMaxf);

					printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;

				} //if (nIndexOfIntensityf < 0 || nIndexOfIntensityf >= nTripleCoocSizeMax)

				sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf] = 0.0;
			} //for (iIntensity_3f = 0; iIntensity_3f < nNumOfGrayLevelIntensities; iIntensity_3f++)

		} //for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)

	} //for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)

//	nIntensityTempf = sColor_Imagef->nRed_Arr[1011];
	//	printf("\n\n 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' (test) 2: nIndexOfPixel_1f = %d,  nIntensityTempf = %d", 1011, nIntensityTempf);

	////////////////////////////////////////////////////////////////////////
	  // size of image
	nImageWidthf = sColor_Imagef->nWidth; //image_in.width();
	nImageHeightf = sColor_Imagef->nLength; //image_in.height();

	nAreaOfTheWholeImagef = nImageWidthf * nImageHeightf;

	nIndexOfPixelMaxf = nAreaOfTheWholeImagef;

	///////////////////////////////////////////////////////////
	if (nLenShift_1f <= 0)
	{
		if (nLenShift_2f <= 0)
		{
			if (nLenShift_2f >= nLenShift_1f)
			{
				nLenBeginf = -nLenShift_1f;
				nLenEndf = sColor_Imagef->nLength;
			} //if (nLenShift_2f >= nLenShift_1f)
			else if (nLenShift_2f < nLenShift_1f)
			{
				nLenBeginf = -nLenShift_2f; // not nLenShift_1f
				nLenEndf = sColor_Imagef->nLength;

			} //else if (nLenShift_2f < nLenShift_1f)
		} //if (nLenShift_2f <= 0)
		else if (nLenShift_2f > 0)
		{
			nLenBeginf = -nLenShift_1f;
			nLenEndf = sColor_Imagef->nLength - nLenShift_2f;

		} //else if (nLenShift_2f > 0)

	} // if (nLenShift_1f <= 0)

	else if (nLenShift_1f > 0)
	{
		//nLenBeginf = 0;
		//nLenEndf = sColor_Imagef->nLength - nLenShiftf;
		if (nLenShift_2f <= 0)
		{
			nLenBeginf = -nLenShift_2f;
			nLenEndf = sColor_Imagef->nLength - nLenShift_1f;
		} //if (nLenShift_2f <= 0)
		else if (nLenShift_2f > 0)
		{
			if (nLenShift_2f >= nLenShift_1f)
			{
				nLenBeginf = 0;
				nLenEndf = sColor_Imagef->nLength - nLenShift_2f;
			} //if (nLenShift_2f >= nLenShift_1f)
			else if (nLenShift_2f < nLenShift_1f)
			{
				nLenBeginf = 0;
				nLenEndf = sColor_Imagef->nLength - nLenShift_1f;
			} //else if (nLenShift_2f < nLenShift_1f)

		} //else if (nLenShift_2f > 0)

	} // else if (nLenShift_1f > 0)
///////////////////

	if (nWidShift_1f <= 0)
	{
		if (nWidShift_2f <= 0)
		{
			if (nWidShift_2f >= nWidShift_1f)
			{
				nWidBeginf = -nWidShift_1f;
				nWidEndf = sColor_Imagef->nWidth;
			} //if (nWidShift_2f >= nWidShift_1f)
			else if (nWidShift_2f < nWidShift_1f)
			{
				nWidBeginf = -nWidShift_2f; // not nWidShift_1f
				nWidEndf = sColor_Imagef->nWidth;

			} //else if (nWidShift_2f < nWidShift_1f)
		} //if (nWidShift_2f <= 0)
		else if (nWidShift_2f > 0)
		{
			nWidBeginf = -nWidShift_1f;
			nWidEndf = sColor_Imagef->nWidth - nWidShift_2f;

		} //else if (nWidShift_2f > 0)

	} // if (nWidShift_1f <= 0)

	else if (nWidShift_1f > 0)
	{
		//nWidBeginf = 0;
		//nWidEndf = sColor_Imagef->nWidth - nWidShiftf;
		if (nWidShift_2f <= 0)
		{
			nWidBeginf = -nWidShift_2f;
			nWidEndf = sColor_Imagef->nWidth - nWidShift_1f;
		} //if (nWidShift_2f <= 0)
		else if (nWidShift_2f > 0)
		{
			if (nWidShift_2f >= nWidShift_1f)
			{
				nWidBeginf = 0;
				nWidEndf = sColor_Imagef->nWidth - nWidShift_2f;
			} //if (nWidShift_2f >= nWidShift_1f)
			else if (nWidShift_2f < nWidShift_1f)
			{
				nWidBeginf = 0;
				nWidEndf = sColor_Imagef->nWidth - nWidShift_1f;
			} //else if (nWidShift_2f < nWidShift_1f)

		} //else if (nWidShift_2f > 0)

	} // else if (nWidShift_1f > 0)

///////////////////////////

	for (iWidf = nWidBeginf; iWidf < nWidEndf; iWidf++)
	{
		nTempPixel_1f = iWidf * sColor_Imagef->nLength;

		nTempPixel_2f = (iWidf + nWidShift_1f) * sColor_Imagef->nLength;

		nTempPixel_3f = (iWidf + nWidShift_2f) * sColor_Imagef->nLength;

		for (iLenf = nLenBeginf; iLenf < nLenEndf; iLenf++)
		{
			nIndexOfPixel_1f = iLenf + nTempPixel_1f;

			if (nIndexOfPixel_1f < 0 || nIndexOfPixel_1f >= nIndexOfPixelMaxf)
			{
				printf("\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nIndexOfPixel_1f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_1f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);

				printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

				printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nIndexOfPixel_1f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_1f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfPixel_1f < 0 || nIndexOfPixel_1f >= nIndexOfPixelMaxf)
///////////////////////////////////
//related to (nLenShift_1f, nWidShift_1f)

			nIndexOfPixel_2f = (iLenf + nLenShift_1f) + nTempPixel_2f;

			if (nIndexOfPixel_2f < 0 || nIndexOfPixel_2f >= nIndexOfPixelMaxf)
			{
				printf("\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nIndexOfPixel_2f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_2f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);

				printf("\n\n nLenShift_1f = %d, nWidShift_1f = %d", nLenShift_1f, nWidShift_1f);
				printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

				printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures);  getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nIndexOfPixel_2f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_2f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfPixel_2f < 0 || nIndexOfPixel_2f >= nIndexOfPixelMaxf)
///////////////////////////////////
//related to (nLenShift_2f, nWidShift_2f)
			nIndexOfPixel_3f = (iLenf + nLenShift_2f) + nTempPixel_3f;

			if (nIndexOfPixel_3f < 0 || nIndexOfPixel_3f >= nIndexOfPixelMaxf)
			{
				printf("\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nIndexOfPixel_3f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_3f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);

				printf("\n\n nLenShift_2f = %d, nWidShift_2f = %d", nLenShift_2f, nWidShift_2f);
				printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

				printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures);  getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nIndexOfPixel_3f = % >= nIndexOfPixelMaxf = %d, nAreaOfTheWholeImagef = %d",
					nIndexOfPixel_3f, nIndexOfPixelMaxf, nAreaOfTheWholeImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfPixel_3f < 0 || nIndexOfPixel_3f >= nIndexOfPixelMaxf)

			 //////////////////////
			nIntensity_1f = sColor_Imagef->nRed_Arr[nIndexOfPixel_1f];

			nIntensity_2f = sColor_Imagef->nRed_Arr[nIndexOfPixel_2f];
			nIntensity_3f = sColor_Imagef->nRed_Arr[nIndexOfPixel_3f];

			if (nIntensity_1f == nIntensityOfBackground || nIntensity_2f == nIntensityOfBackground || nIntensity_3f == nIntensityOfBackground)
				continue;

			nNumOfValidObjectPixelsf += 1;
/*
for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)
	{
		nTempIntensity_1f = iIntensity_1f * nNumOfGrayLevelIntensities*nNumOfGrayLevelIntensities;

		for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)
		{
			nTempIntensity_2f = iIntensity_2f * nNumOfGrayLevelIntensities;

			for (iIntensity_3f = 0; iIntensity_3f < nNumOfGrayLevelIntensities; iIntensity_3f++)
			{
				nIndexOfIntensityf = iIntensity_3f + nTempIntensity_2f + nTempIntensity_1f;
*/
/*
			nTempIntensityf = nIntensity_1f * nNumOfGrayLevelIntensities;

			nIndexOfIntensityf = iIntensity_2f + nTempIntensityf;
			sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] += 1.0;
*/
			nTempIntensity_1f = nIntensity_1f * nNumOfGrayLevelIntensities*nNumOfGrayLevelIntensities;

			nTempIntensity_2f = nIntensity_2f * nNumOfGrayLevelIntensities;

			nIndexOfIntensityf = nIntensity_3f + nTempIntensity_2f + nTempIntensity_1f;

				if (nIndexOfIntensityf < 0 || nIndexOfIntensityf >= nIndexOfIntensityMaxf)
				{
					printf("\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 2: nIndexOfIntensityf = %d >= nIndexOfIntensityMaxf = %d",
						nIndexOfIntensityf, nIndexOfIntensityMaxf);

					printf("\n nIntensity_1f = %d, nIntensity_2f = %d, nIntensity_3f = %d", nIntensity_1f, nIntensity_2f, nIntensity_3f);
					printf("\n\n nIndexOfPixel_1f = %d, nIndexOfPixel_2f = %d, nIndexOfPixel_3f = %d", nIndexOfPixel_1f, nIndexOfPixel_2f, nIndexOfPixel_3f);

					
					printf("\n\n nLenShift_1f = %d, nWidShift_1f = %d, nLenShift_2f = %d, nWidShift_2f = %d", nLenShift_1f, nWidShift_1f, nLenShift_2f, nWidShift_2f);
					printf("\n\n iLenf = %d, iWidf = %d", iLenf, iWidf);
					printf("\n\n nLenBeginf = %d, nLenEndf = %d, nWidBeginf = %d, nWidEndf = %d", nLenBeginf, nLenEndf,nWidBeginf, nWidEndf);

					nWidBeginf = 0;
					nWidEndf = sColor_Imagef->nWidth - nWidShift_1f;

					printf("\n\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

					printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;

				} //if (nIndexOfIntensityf < 0 || nIndexOfIntensityf >= nIndexOfIntensityMaxf)

			sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf] += 1.0;

		} //for (iLenf = nLenBeginf; iLenf < nLenEndf; iLenf++)

	} //for (iWidf = nWidBeginf; iWidf < nWidEndf; iWidf++)
////////////////////////////////////////
	if (nNumOfValidObjectPixelsf <= 1)
	{
		printf("\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nNumOfValidObjectPixelsf = %d, nAreaOfTheWholeImagef = %d",
			nNumOfValidObjectPixelsf, nAreaOfTheWholeImagef);

		printf("\n\n nLenShift_1f = %d, nWidShift_1f = %d, nLenShift_2f = %d, nWidShift_2f = %d", nLenShift_1f, nWidShift_1f, nLenShift_2f, nWidShift_2f);
		printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

		printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nNumOfValidObjectPixelsf = %d, nAreaOfTheWholeImagef = %d",
			nNumOfValidObjectPixelsf, nAreaOfTheWholeImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfValidPixelsTotf <= 1)
/////////////////////

	for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)
	{
		//nTempIntensityf = iIntensity_1f * nNumOfGrayLevelIntensities;
		nTempIntensity_1f = iIntensity_1f * nNumOfGrayLevelIntensities*nNumOfGrayLevelIntensities;

		for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)
		{
			nTempIntensity_2f = iIntensity_2f * nNumOfGrayLevelIntensities;

			for (iIntensity_3f = 0; iIntensity_3f < nNumOfGrayLevelIntensities; iIntensity_3f++)
			{
				//nIndexOfIntensityf = iIntensity_2f + nTempIntensityf;
				nIndexOfIntensityf = iIntensity_3f + nTempIntensity_2f + nTempIntensity_1f;

				if (nIndexOfIntensityf < 0 || nIndexOfIntensityf >= nIndexOfIntensityMaxf)
				{
					printf("\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 1: nIndexOfIntensityf = %d >= nIndexOfIntensityMaxf = %d",
						nIndexOfIntensityf, nIndexOfIntensityf);

					printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;

				} //if (nIndexOfIntensityf < 0 || nIndexOfIntensityf >= nTripleCoocSizeMax)

				sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf] = 
					sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf]/ (float)(nNumOfValidObjectPixelsf);

				if (sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf] < 0.0 ||
					sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf] > 1.0)
				{
					printf("\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf] = %E",
						sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf]);

					printf("\n\n nLenShift_1f = %d, nWidShift_1f = %d, nLenShift_2f = %d, nWidShift_2f = %d", nLenShift_1f, nWidShift_1f, nLenShift_2f, nWidShift_2f);
					printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

					printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf] = %E",
						sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf->fTripleCoocByLenWidShift_Arr[nIndexOfIntensityf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				} //if (sCoocOfImageByOne_LenWidShiftf->fCoocOfImageByLenWidShift_Arr[nIndexOfIntensityf] < 0.0 || ...
			} //for (iIntensity_3f = 0; iIntensity_3f < nNumOfGrayLevelIntensities; iIntensity_3f++)

		} //for (iIntensity_2f = 0; iIntensity_2f < nNumOfGrayLevelIntensities; iIntensity_2f++)

	} //for (iIntensity_1f = 0; iIntensity_1f < nNumOfGrayLevelIntensities; iIntensity_1f++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nLenShift_1f = %d, nWidShift_1f = %d, nLenShift_2f = %d, nWidShift_2f = %d", 
		nLenShift_1f, nWidShift_1f, nLenShift_2f, nWidShift_2f);
	printf("\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

	fprintf(fout_PrintFeatures, "\n\n The end of 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts': nLenShift_1f = %d, nWidShift_1f = %d, nLenShift_2f = %d, nWidShift_2f = %d", 
		nLenShift_1f, nWidShift_1f, nLenShift_2f, nWidShift_2f);
	fprintf(fout_PrintFeatures, "\n nImageWidthf = %d, nImageHeightf = %d", nImageWidthf, nImageHeightf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} //int TripleCoocOf_Image_ByOneCouple_OfLenWidShifts(...
//////////////////////////////////////////////////////////////////////
//int CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist(

int TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist(
	//the image is supposed to be grayscale
	const COLOR_IMAGE *sColor_Imagef,

	const int nDistToCoocPointsf,


	//		nNumOfSecondPointsFor_OneTripleCooc = ( ((2*nDistToCoocPointsMax + 1)*(2*nDistToCoocPointsMax + 1)) - 2),
//4 * iDistf*nNumOfSecondPointsFor_OneTripleCooc*nTripleCoocSizeMax;
	float fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[]) //4 * nDistToCoocPointsf*nNumOfSecondPointsFor_OneTripleCooc*nTripleCoocSizeMax
{
/*
	int CoocFeasOf_Image_ByOne_LenWidShift(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nLenShiftf,
		const int nWidShiftf,

		COOC_OF_IMAGE_BY_LEN_WID_SHIFT *sCoocOfImageByOne_LenWidShiftf);
	/////////////////
	COOC_OF_IMAGE_BY_LEN_WID_SHIFT sCoocOfImageByOne_LenWidShiftf;
*/

	int TripleCoocOf_Image_ByOneCouple_OfLenWidShifts(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nLenShift_1f,
		const int nWidShift_1f,

		const int nLenShift_2f,
		const int nWidShift_2f,

		TRIPLE_COOC_OF_IMAGE_BY_ONE_COUPLE_OF_LEN_WID_SHIFTS *sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf);

	int
		iLen_1f,
		iWid_1f,
		iLen_2f,
		iWid_2f,

		nNumOfShifts_1stCoupleCurf = 0,
		nNumOfShifts_2ndCoupleCurf = 0,
		nNumOfShifts_TotCurf = 0,
		iCoocf,
		nIndexOfCoocf,

		//#define nNumOfSecondPointsForTripleCoocTot ( ((2*nDistToCoocPointsMax + 1)*(2*nDistToCoocPointsMax + 1)) - 2)

		//nIndexOfCoocAtAFixedDistMaxf = 4 * nDistToCoocPointsf*nCoocSizeMax,
		nIndexOfTripleCoocAtAFixedDistMaxf = 4 * nDistToCoocPointsf*nNumOfSecondPointsFor_OneTripleCooc*nTripleCoocSizeMax,
		nTempf,

		nLenBeginf,
		nLenEndf,

		nWidBeginf,
		nWidEndf,

		nLenShift_1f,
		nWidShift_1f,
		nLenShift_2f,
		nWidShift_2f,

		nResf;
//////////////////////////////////////////////
	for (iCoocf = 0; iCoocf < nIndexOfTripleCoocAtAFixedDistMaxf; iCoocf++)
	{
		fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[iCoocf] = 0.0;
	} //for (iCoocf = 0; iCoocf < nIndexOfTripleCoocAtAFixedDistMaxf; iCoocf++)
///////////////////////////////////////////////////////////////////////

	TRIPLE_COOC_OF_IMAGE_BY_ONE_COUPLE_OF_LEN_WID_SHIFTS sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf;
////////////////
//the top 
	nWidShift_1f = nDistToCoocPointsf;

	nLenBeginf = -nDistToCoocPointsf;
	nLenEndf = nDistToCoocPointsf;

	for (iLen_1f = nLenBeginf; iLen_1f <= nLenEndf; iLen_1f++)
	{
		nNumOfShifts_1stCoupleCurf += 1;

		nLenShift_1f = iLen_1f;
/*
		nResf = CoocFeasOf_Image_ByOne_LenWidShift(
			//the image is supposed to be grayscale
			sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

			nLenShift_1f, //const int nLenShift_1f,
			nWidShift_1f, //const int nWidShift_1f,

			&sCoocOfImageByOne_LenWidShift_1f); // COOC_OF_IMAGE_BY_LEN_WID_SHIFT *sCoocOfImageByOne_LenWidShift_1f);
*/
		nNumOfShifts_2ndCoupleCurf = 0;

		for (iWid_2f = -nDistToCoocPointsMax; iWid_2f <= nDistToCoocPointsMax; iWid_2f++)
		{
			nWidShift_2f = iWid_2f;
			for (iLen_2f = -nDistToCoocPointsMax; iLen_2f <= nDistToCoocPointsMax; iLen_2f++)
			{
				nLenShift_2f = iLen_2f;
				if ((nWidShift_2f == 0 && nLenShift_2f == 0) || (nWidShift_2f == nWidShift_1f && nLenShift_2f == nLenShift_1f))
				{
					continue;
				}//if ((nWidShift_2f == 0 && nLenShift_2f == 0) || (nWidShift_2f == nWidShift_1f && nLenShift_2f == nLenShift_1f))

				nNumOfShifts_TotCurf += 1;

				nResf = TripleCoocOf_Image_ByOneCouple_OfLenWidShifts(
					//the image is supposed to be grayscale
					sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

					nLenShift_1f, //const int nLenShift_1f,
					nWidShift_1f, //const int nWidShift_1f,

					nLenShift_2f, //const int nLenShift_2f,
					nWidShift_2f, //const int nWidShift_2f,

					&sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf); // TRIPLE_COOC_OF_IMAGE_BY_ONE_COUPLE_OF_LEN_WID_SHIFTS *sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 1");
					printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 1");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

		//nTempf = (nNumOfShifts_1stCoupleCurf - 1)*nCoocSizeMax;
			nTempf = (nNumOfShifts_TotCurf - 1)*nTripleCoocSizeMax;
		
		//for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)
				for (iCoocf = 0; iCoocf < nTripleCoocSizeMax; iCoocf++)
				{
				nIndexOfCoocf = iCoocf + nTempf;

					if (nIndexOfCoocf >= nIndexOfTripleCoocAtAFixedDistMaxf)
					{
					printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' 1: nIndexOfCoocf = %d >= nIndexOfTripleCoocAtAFixedDistMaxf = %d",
						nIndexOfCoocf, nIndexOfTripleCoocAtAFixedDistMaxf);

					printf("\n nLenShift_1f = %d, nWidShift_1f = %d, nLenShift_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
						 nLenShift_1f, nWidShift_1f, nLenShift_2f, nWidShift_2f, nDistToCoocPointsf);

					printf("\n\n Please press any key to exit:"); getchar(); exit(1);

	#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_PrintFeatures, "\n\n  An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' 1: nIndexOfCoocf = %d >= nIndexOfTripleCoocAtAFixedDistMaxf = %d",
						nIndexOfCoocf, nIndexOfTripleCoocAtAFixedDistMaxf);

	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)

					if (sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] < -fLarge ||
						sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] > fLarge)
					{

						printf("\n\n 1: an error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]");

						printf("\n iLen_1f = %d, iWid_2f = %d, iLen_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
							iLen_1f, iWid_2f, iLen_2f, nWidShift_2f, nDistToCoocPointsf);
						
						printf("\n\n sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[%d] = %E", 
							iCoocf, sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]);

//#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_PrintFeatures, "\n\n 1: an error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]");

						fprintf(fout_PrintFeatures,"\n iLen_1f = %d, iWid_2f = %d, iLen_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
							iLen_1f, iWid_2f, iLen_2f, nWidShift_2f, nDistToCoocPointsf);

						fprintf(fout_PrintFeatures,"\n\n sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[%d] = %E",
							iCoocf, sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]);

//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures);  getchar(); exit(1);

					} //if (sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] < -fLarge ||
						
					fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[nIndexOfCoocf] = sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf];
				} //for (iCoocf = 0; iCoocf < nTripleCoocSizeMax; iCoocf++)
			
			} //for (iLen_2f = -nDistToCoocPointsMax; iLen_2f <= nDistToCoocPointsMax; iLen_2f++)
		} //for (iWid_2f = -nDistToCoocPointsMax; iWid_2f <= nDistToCoocPointsMax; iWid_2f++)

	} //for (iLen_1f = nLenBeginf; iLen_1f <= nLenEndf; iLen_1f++)

	////////////////
//the right 
	nLenShift_1f = nDistToCoocPointsf;

	nWidBeginf = 0;
	nWidEndf = nDistToCoocPointsf - 1;

	for (iWid_1f = nWidBeginf; iWid_1f <= nWidEndf; iWid_1f++)
	{
		nNumOfShifts_1stCoupleCurf += 1;

		nWidShift_1f = iWid_1f;

		nNumOfShifts_2ndCoupleCurf = 0;
		for (iWid_2f = -nDistToCoocPointsMax; iWid_2f <= nDistToCoocPointsMax; iWid_2f++)
		{
			nWidShift_2f = iWid_2f;
			for (iLen_2f = -nDistToCoocPointsMax; iLen_2f <= nDistToCoocPointsMax; iLen_2f++)
			{
				nLenShift_2f = iLen_2f;
				if ((nWidShift_2f == 0 && nLenShift_2f == 0) || (nWidShift_2f == nWidShift_1f && nLenShift_2f == nLenShift_1f))
				{
					continue;
				}//if ((nWidShift_2f == 0 && nLenShift_2f == 0) || (nWidShift_2f == nWidShift_1f && nLenShift_2f == nLenShift_1f))

				nNumOfShifts_TotCurf += 1;

				nResf = TripleCoocOf_Image_ByOneCouple_OfLenWidShifts(
					//the image is supposed to be grayscale
					sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

					nLenShift_1f, //const int nLenShift_1f,
					nWidShift_1f, //const int nWidShift_1f,

					nLenShift_2f, //const int nLenShift_2f,
					nWidShift_2f, //const int nWidShift_2f,

					&sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf); // TRIPLE_COOC_OF_IMAGE_BY_ONE_COUPLE_OF_LEN_WID_SHIFTS *sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 1");
					printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 1");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

		//nTempf = (nNumOfShifts_1stCoupleCurf - 1)*nCoocSizeMax;
				nTempf = (nNumOfShifts_TotCurf - 1)*nTripleCoocSizeMax;

				//for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)
				for (iCoocf = 0; iCoocf < nTripleCoocSizeMax; iCoocf++)
				{
					nIndexOfCoocf = iCoocf + nTempf;

					if (nIndexOfCoocf >= nIndexOfTripleCoocAtAFixedDistMaxf)
					{
						printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' 2: nIndexOfCoocf = %d >= nIndexOfTripleCoocAtAFixedDistMaxf = %d",
							nIndexOfCoocf, nIndexOfTripleCoocAtAFixedDistMaxf);

						printf("\n nLenShift_1f = %d, nWidShift_1f = %d, nLenShift_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
							nLenShift_1f, nWidShift_1f, nLenShift_2f, nWidShift_2f, nDistToCoocPointsf);

						printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_PrintFeatures, "\n\n  An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' 2: nIndexOfCoocf = %d >= nIndexOfTripleCoocAtAFixedDistMaxf = %d",
							nIndexOfCoocf, nIndexOfTripleCoocAtAFixedDistMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)

///////////////////////////////
					if (sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] < -fLarge ||
						sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] > fLarge)
					{

						printf("\n\n 2: an error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]");

						printf("\n iLen_1f = %d, iWid_2f = %d, iLen_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
							iLen_1f, iWid_2f, iLen_2f, nWidShift_2f, nDistToCoocPointsf);

						printf("\n\n sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[%d] = %E",
							iCoocf, sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]);

						//#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_PrintFeatures, "\n\n 2: an error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]");

						fprintf(fout_PrintFeatures, "\n iWid_1f = %d, iWid_2f = %d, iLen_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
							iWid_1f, iWid_2f, iLen_2f, nWidShift_2f, nDistToCoocPointsf);

						fprintf(fout_PrintFeatures, "\n\n sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[%d] = %E",
							iCoocf, sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]);

						//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures);  getchar(); exit(1);

					} //if (sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] < -fLarge ||

					fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[nIndexOfCoocf] = sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf];
				} //for (iCoocf = 0; iCoocf < nTripleCoocSizeMax; iCoocf++)

			} //for (iLen_2f = -nDistToCoocPointsMax; iLen_2f <= nDistToCoocPointsMax; iLen_2f++)
		} //for (iWid_2f = -nDistToCoocPointsMax; iWid_2f <= nDistToCoocPointsMax; iWid_2f++)

	} //for (iWid_1f = nWidBeginf; iWid_1f <= nWidEndf; iWid_1f++)

		////////////////
//the left 
	nLenShift_1f = -nDistToCoocPointsf;

	nWidBeginf = 1; // not 0; -- symmetry with the right
	nWidEndf = nDistToCoocPointsf - 1;

	for (iWid_1f = nWidBeginf; iWid_1f <= nWidEndf; iWid_1f++)
	{
		nNumOfShifts_1stCoupleCurf += 1;

		nWidShift_1f = iWid_1f;

		nNumOfShifts_2ndCoupleCurf = 0;
		for (iWid_2f = -nDistToCoocPointsMax; iWid_2f <= nDistToCoocPointsMax; iWid_2f++)
		{
			nWidShift_2f = iWid_2f;
			for (iLen_2f = -nDistToCoocPointsMax; iLen_2f <= nDistToCoocPointsMax; iLen_2f++)
			{
				nLenShift_2f = iLen_2f;
				if ((nWidShift_2f == 0 && nLenShift_2f == 0) || (nWidShift_2f == nWidShift_1f && nLenShift_2f == nLenShift_1f))
				{
					continue;
				}//if ((nWidShift_2f == 0 && nLenShift_2f == 0) || (nWidShift_2f == nWidShift_1f && nLenShift_2f == nLenShift_1f))

				nNumOfShifts_TotCurf += 1;
				nResf = TripleCoocOf_Image_ByOneCouple_OfLenWidShifts(
					//the image is supposed to be grayscale
					sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

					nLenShift_1f, //const int nLenShift_1f,
					nWidShift_1f, //const int nWidShift_1f,

					nLenShift_2f, //const int nLenShift_2f,
					nWidShift_2f, //const int nWidShift_2f,

					&sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf); // TRIPLE_COOC_OF_IMAGE_BY_ONE_COUPLE_OF_LEN_WID_SHIFTS *sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 1");
					printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by 'TripleCoocOf_Image_ByOneCouple_OfLenWidShifts' 1");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

		//nTempf = (nNumOfShiftsTotf - 1)*nCoocSizeMax;
				nTempf = (nNumOfShifts_TotCurf - 1)*nTripleCoocSizeMax;

				//for (iCoocf = 0; iCoocf < nCoocSizeMax; iCoocf++)
				for (iCoocf = 0; iCoocf < nTripleCoocSizeMax; iCoocf++)
				{
					nIndexOfCoocf = iCoocf + nTempf;

					if (nIndexOfCoocf >= nIndexOfTripleCoocAtAFixedDistMaxf)
					{
						printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' 3: nIndexOfCoocf = %d >= nIndexOfTripleCoocAtAFixedDistMaxf = %d",
							nIndexOfCoocf, nIndexOfTripleCoocAtAFixedDistMaxf);

						printf("\n nLenShift_1f = %d, nWidShift_1f = %d, nLenShift_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
							nLenShift_1f, nWidShift_1f, nLenShift_2f, nWidShift_2f, nDistToCoocPointsf);

						printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_PrintFeatures, "\n\n  An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' 3: nIndexOfCoocf = %d >= nIndexOfTripleCoocAtAFixedDistMaxf = %d",
							nIndexOfCoocf, nIndexOfTripleCoocAtAFixedDistMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfCoocf >= nIndexOfCoocAtAFixedDistMaxf)
/////////////////////////
					if (sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] < -fLarge ||
						sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] > fLarge)
					{

						printf("\n\n 3: an error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]");

						printf("\n iWid_1f = %d, iWid_2f = %d, iLen_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
							iWid_1f, iWid_2f, iLen_2f, nWidShift_2f, nDistToCoocPointsf);

						printf("\n\n sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[%d] = %E",
							iCoocf, sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]);

						//#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_PrintFeatures, "\n\n 3: an error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist' by sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]");

						fprintf(fout_PrintFeatures, "\n iWid_1f = %d, iWid_2f = %d, iLen_2f = %d, nWidShift_2f = %d, nDistToCoocPointsf = %d",
							iWid_1f, iWid_2f, iLen_2f, nWidShift_2f, nDistToCoocPointsf);

						fprintf(fout_PrintFeatures, "\n\n sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[%d] = %E",
							iCoocf, sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf]);

						//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n Please press any key to exit:"); fflush(fout_PrintFeatures);  getchar(); exit(1);

					} //if (sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf] < -fLarge ||


					fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[nIndexOfCoocf] = sTripleCoocOf_Image_ByOneCouple_OfLenWidShiftsf.fTripleCoocByLenWidShift_Arr[iCoocf];
				} //for (iCoocf = 0; iCoocf < nTripleCoocSizeMax; iCoocf++)

				} //for (iLen_2f = -nDistToCoocPointsMax; iLen_2f <= nDistToCoocPointsMax; iLen_2f++)
			} //for (iWid_2f = -nDistToCoocPointsMax; iWid_2f <= nDistToCoocPointsMax; iWid_2f++)

	} //for (iWid_1f = nWidBeginf; iWid_1f <= nWidEndf; iWid_1f++)
/////////////////////////////////
	if (nNumOfShifts_1stCoupleCurf != 4 * nDistToCoocPointsf)
	{
		printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist': nNumOfShifts_1stCoupleCurf = %d != 4 * nDistToCoocPointsf = %d",
			nNumOfShifts_1stCoupleCurf, 4 * nDistToCoocPointsf);
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist': nNumOfShiftsTotf = %d != 4 * nDistToCoocPointsf = %d",
			nNumOfShiftsTotf, 4 * nDistToCoocPointsf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfShiftsTotf != 4 * nDistToCoocPointsf)

	return SUCCESSFUL_RETURN;
} //int TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist(...
/////////////////////////////////////////////////////////////////

/*
int CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess(
	//the image is supposed to be grayscale
	const COLOR_IMAGE *sColor_Imagef,

	const int nDistToCoocPointsMinf,
	const int nDistToCoocPointsMaxf,

	float fCoocFeasOf_Image_By_LenWidShift_At_AllDist_Arrf[])
	//4*nCoocSizeMax*[(nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*( nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1)]/2

*/
int TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess(
	//the image is supposed to be grayscale
	const COLOR_IMAGE *sColor_Imagef,

	const int nDistToCoocPointsMinf,
	const int nDistToCoocPointsMaxf,

	float fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf[])// [nNumOfTripleCoocFeasOf_ImageTot]
{
/*/
	int CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nDistToCoocPointsf,

		float fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[]); //4*nDistToCoocPointsf*nCoocSizeMax
*/

	int TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nDistToCoocPointsf,

//		nNumOfSecondPointsFor_OneTripleCooc = ( ((2*nDistToCoocPointsMax + 1)*(2*nDistToCoocPointsMax + 1)) - 2),
		float fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[]); //4 * nDistToCoocPointsf*nNumOfSecondPointsFor_OneTripleCooc*nTripleCoocSizeMax

	int
		iDistf,
		iFeaf,

		nIndexOfFeaCurf,

		nSizeOfTripleCoocCurf,
		nIndexOfTripleCoocAt_AllDistMaxf = nNumOfTripleCoocFeasOf_ImageTot,

		nTempf,

		nNumOfFeaCurf = 0,
		nResf;
	////////////////

	for (iFeaf = 0; iFeaf < nIndexOfTripleCoocAt_AllDistMaxf; iFeaf++)
	{
		fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf[iFeaf] = 0.0;
	} //for (iFeaf = 0; iFeaf < nIndexOfTripleCoocAt_AllDistMaxf; iFeaf++)

///////////////////////////////////
	for (iDistf = nDistToCoocPointsMinf; iDistf <= nDistToCoocPointsMaxf; iDistf++)
	{
		//nSizeOfCoocFeasCurf = 4 * iDistf*nCoocSizeMax;
	//	float *fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf = new float[nSizeOfCoocFeasCurf]; //0 -- 1 //[nDimSelecMaxf]
		
		nSizeOfTripleCoocCurf = 4 * iDistf*nNumOfSecondPointsFor_OneTripleCooc*nTripleCoocSizeMax;

	float *fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf = new float[nSizeOfTripleCoocCurf]; //0 -- 1 //[nDimSelecMaxf]

		if (fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf == nullptr)
		{
			printf("\n\n An error in dynamic memory allocation for 'fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf'");
			printf("\n Please press any key to exit"); getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf == nullptr)
/*
		nResf = CoocFeasOf_Image_By_All_LenWidShifts_AtAFixedDist(
			//the image is supposed to be grayscale
			sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

			iDistf, //const int nDistToCoocPointsf,

			fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf); // float fCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[]); //4*nDistToCoocPointsf*nCoocSizeMax
*/

		nResf = TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist(
			//the image is supposed to be grayscale
			sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

			iDistf, //const int nDistToCoocPointsf,

			//		nNumOfSecondPointsFor_OneTripleCooc = ( ((2*nDistToCoocPointsMax + 1)*(2*nDistToCoocPointsMax + 1)) - 2),
			fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf); // float fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[]); //4 * nDistToCoocPointsf*nNumOfSecondPointsFor_OneTripleCooc*nTripleCoocSizeMax

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess' by 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist'");
			printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_PrintFeatures, "\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess' by 'TripleCoocOf_Image_By_All_LenWidShifts_AtAFixedDist'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			delete[] fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf;
			return UNSUCCESSFUL_RETURN;
		} //if (nResf == UNSUCCESSFUL_RETURN)
////////////////////////////

		for (iFeaf = 0; iFeaf < nSizeOfTripleCoocCurf; iFeaf++)
		{
			nIndexOfFeaCurf = nNumOfFeaCurf + iFeaf;
			if (nIndexOfFeaCurf < 0 || nIndexOfFeaCurf >= nIndexOfTripleCoocAt_AllDistMaxf)
			{
				printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess': nIndexOfFeaCurf = %d >= nIndexOfTripleCoocAt_AllDistMaxf = %d, iDistf = %d, nDistToCoocPointsMinf = %d, nDistToCoocPointsMaxf = %d",
					nIndexOfFeaCurf, nIndexOfTripleCoocAt_AllDistMaxf, iDistf, nDistToCoocPointsMinf, nDistToCoocPointsMaxf);
				printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n  An error in 'TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess': nIndexOfFeaCurf = %d >= nIndexOfTripleCoocAt_AllDistMaxf = %d, iDistf = %d, nDistToCoocPointsMinf = %d, nDistToCoocPointsMaxf = %d",
					nIndexOfFeaCurf, nIndexOfTripleCoocAt_AllDistMaxf, iDistf, nDistToCoocPointsMinf, nDistToCoocPointsMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				delete[] fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfFeaCurf < 0 || nIndexOfFeaCurf >= nIndexOfTripleCoocAt_AllDistMaxf)

			fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf[nIndexOfFeaCurf] = fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf[iFeaf];
		} //for (iFeaf = 0; iFeaf < nSizeOfTripleCoocCurf; iFeaf++)

		nNumOfFeaCurf += nSizeOfTripleCoocCurf;

		delete[] fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf;
	} //for (iDistf = nDistToCoocPointsMinf; iDistf <= nDistToCoocPointsMaxf; iDistf++)
//////////////////////////////////////////

	if (nNumOfFeaCurf != nIndexOfTripleCoocAt_AllDistMaxf)
	{
		printf("\n\n An error in 'TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess': nNumOfFeaCurf = %d != nIndexOfTripleCoocAt_AllDistMaxf = %d, nDistToCoocPointsMinf = %d, nDistToCoocPointsMaxf = %d",
			nNumOfFeaCurf, nIndexOfTripleCoocAt_AllDistMaxf, nDistToCoocPointsMinf, nDistToCoocPointsMaxf);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "\n\n  An error in 'TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess': nNumOfFeaCurf = %d != nIndexOfTripleCoocAt_AllDistMaxf = %d, nDistToCoocPointsMinf = %d, nDistToCoocPointsMaxf = %d",
			nNumOfFeaCurf, nIndexOfTripleCoocAt_AllDistMaxf, nDistToCoocPointsMinf, nDistToCoocPointsMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		//delete[] fTripleCoocFeas_By_LenWidShift_AtAFixedDist_Arrf;

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfFeaCurf != nIndexOfTripleCoocAt_AllDistMaxf)

	return SUCCESSFUL_RETURN;
} //int TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess(...

/*
//From Wikipedia, https://en.wikipedia.org/wiki/HSL_and_HSV
//From I.Pitas, Digital Image Processing Algorithms and Applications, 2000, p.33
// Alternative: Practical algorithms for image analysis, Michael Seul and others, p.53

int HLS_Fr_RGB(
	const COLOR_IMAGE *sColor_Imagef, //[nImageAreaMax]

	HLS_IMAGE *sHLS_Imagef)
*/

///////////////////////////////////////////////////////////////////////////////
  //printf("\n\n Please press any key:"); getchar();



