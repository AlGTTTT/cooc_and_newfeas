
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

int
	nNumOfProcessedFiles_NormalTot_Glob;
//#ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED

//#endif //#ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
#include "Cooc_And_NewFeas_function1.cpp"

int main()
{
	int Reading_AColorImage(
		const Image& image_in,

		COLOR_IMAGE *sColor_Image);

	int	PositionsOfMeaningfulFeasForRegularOrTripleCooc(
		const int nDimf,

		const int nNumVecf, //nNumVecsForPositionsOfMeaningfulFeasForRegularOrTripleCooc == 3

		int &nNumOfFeasWithMeaningfulValues_Finf,

		int nFeaTheSameOrNot_FinArrf[]);//[nDimf] // 1-- the same, 0-- not

	int	Converting_OneFeaVec_ToAVecWithSelecFeas(
		const int nDim_1f,
		const int nDim_2f, // == nNumOfFeasWithMeaningfulValues_Finf < nDim_1f

		const float f_1Arrf[], //[nDim_1f]

		const int nFeaTheSameOrNot_FinArrf[], //[nDim_1f]

		float f_SelecArrf[]); //[nDim_2f] //fActualFeasFor_OneNormalVec_CoocExtendedArr[]


#ifdef USING_HISTOGRAM_EQUALIZATION
	int HistogramEqualization_ForColorFormatOfGray(

		//GRAYSCALE_IMAGE *sGrayscale_Image)
		COLOR_IMAGE *sColor_Imagef);

	int Histogram_Statistics_ColorActuallyGrayscale_Image(

		//const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageAreaMax]
		const COLOR_IMAGE *sColor_Imagef,

		float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

		float &fMeanf,
		float &fStDevf,
		float &fSkewnessf,
		float &fKurtosisf);

#endif //#ifdef USING_HISTOGRAM_EQUALIZATION


#ifndef USING_TRIPLE_COOCURRENCE_ONLY
	//#endif // #ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED
	int CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nDistToCoocPointsMinf,
		const int nDistToCoocPointsMaxf,

		float fCoocFeasOf_Image_By_LenWidShift_At_AllDist_Arrf[]);
	//4*nCoocSizeMax*[(nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*( nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1)]/2
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

	void Initializing_AFloatVec_To_Zero(
		const int nDimf,
		float fVecArrf[]); //[nDimf]

	int ConvertingVecs_ToBest_SeparableFeas(
		const float fLargef,
		const  float fepsf,

		const int nDimf,
		const int nDimSelecMaxf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		const float fEfficBestSeparOfOneFeaAcceptMaxf,

		float fArr1stf[], //[nDimf*nNumVec1stf] // after 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
		float fArr2ndf[], //[nDimf*nNumVec2ndf] //after 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	///////////////////////////////////

		int &nDimSelecf,

		float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
		float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

		float &fRatioBestMinf,

		int &nPosOneFeaBestMaxf,

		int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparBestMaxf,
		float &fNumArr2ndSeparBestMaxf,

		float fRatioBestArrf[], //nDimSelecMaxf

		int nPosFeaSeparBestArrf[]);	//nDimSelecMaxf

	int OneBest_SeparableRatioOf_2Feas(
		const float fLargef,
		const  float fepsf,

		const int nDimf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		const float fEfficBestSeparOfOneFeaAcceptMaxf,

		float fArr1stf[], //[nDimf*nNumVec1stf] // will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
		float fArr2ndf[], //[nDimf*nNumVec2ndf] //will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	///////////////////////////////////

		float &fRatioBest_ByRatioOf_2FeasMinf);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	int TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess(
		//the image is supposed to be grayscale
		const COLOR_IMAGE *sColor_Imagef,

		const int nDistToCoocPointsMinf,
		const int nDistToCoocPointsMaxf,

		float fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf[]);// [nNumOfTripleCoocFeasOf_ImageTot]
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	///////////////////////////////////////////////////
	int
		iFea,

		nImageWidth, // = image_in.width();
		nImageHeight, // = image_in.height();
		nSizeOfImage,

		iLen,
		iWid,

		nIntensity_Read_Test_ImageMax = -nLarge,

		nIndexOfPixelCur,
		nRed, nGreen,
		nBlue,
		nIndex,

#ifdef USING_HISTOGRAM_EQUALIZATION

		iHistf,
#endif //#ifdef USING_HISTOGRAM_EQUALIZATION

		nIndex_AllVecs,
		nInitFea_ForAllVecs,
		nTempf,

		nDimSelec,
		nNumOfProcessedFiles_NormalTot = 0,
		nNumOfProcessedFiles_MalignantTot = 0,

		nNumOf_ActualFeas_ForAll_Normal_VecsTotf,
		nNumOf_ActualFeas_ForAll_Malignant_VecsTotf,

		nPosOneFeaBestMax, //
		nSeparDirectionBestMax,
		nPosFeaSeparBestArr[nDimSelecMax],

		nPosOfSelectedFeaCur,
		////////////////////////
		nNumOfFeasWithMeaningfulValues_Fin,

#ifndef USING_TRIPLE_COOCURRENCE_ONLY

		nFeaTheSameOrNot_FinArr[nDim], //[nDimf] // 1-- the same, 0-- not
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

///////////////////////
		iVec,
		nRes;

	#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	int *nFeaTheSameOrNot_FinArr;
	nFeaTheSameOrNot_FinArr = new int[nNumOfTripleCoocFeasOf_ImageTot];

	if (nFeaTheSameOrNot_FinArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'nFeaTheSameOrNot_FinArr' in 'main'");

		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaTheSameOrNot_FinArr == nullptr)

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

	float
		//fFractal_Dimension,
		fFeaCur,
////////////////////////////////////////

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
		fCoocFeasOf_Nor_Image_By_LenWidShift_At_AllDist_Arrf[nDim],
		fCoocFeasOf_Mal_Image_By_LenWidShift_At_AllDist_Arrf[nDim],

		fNumOfFeas_ForAll_Normal_VecsTotf = (float)(nNumOfVecs_Normal)*(float)(nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot),
		fNumOfFeas_ForAll_Malignant_VecsTotf = (float)(nNumOfVecs_Malignant)*(float)(nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot),
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

		fAll_SelecFeasForAll_NormalVecs_Arr[nDimSelecMax*nNumOfVecs_Normal], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
		fAll_SelecFeasForAll_MalignantVecs_Arr[nDimSelecMax*nNumOfVecs_Malignant], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

#ifdef USING_HISTOGRAM_EQUALIZATION

		fPercentsOfHistogramIntervalsArrf[nNumOfHistogramIntervals_ForCooc],//[nNumOfHistogramIntervals_ForCooc]

		fPercentagesAboveThresholds_InHistogramArrf[nNumOfHistogramIntervals_ForCooc - 2], //[nNumOfHistogramIntervals_ForCooc - 2]

		fMeanOfHistf,
		fStDevOfHistf,
		fSkewnessOfHistf,
		fKurtosisfOfHistf,

#endif //#ifdef USING_HISTOGRAM_EQUALIZATION

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		//nNumOfFirstPointsForCoocTot == (2*(nDistToCoocPointsMin + nDistToCoocPointsMax)*( nDistToCoocPointsMax - nDistToCoocPointsMin + 1))
		fDimOfTripleCoocOfOneImagef = (float)(nTripleCoocSizeMax)*(float)(nNumOfSecondPointsForTripleCoocTot),

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		fNumOf_NormalVecsSeparBestMax,
		fNumOf_MalignantVecsSeparBestMax,
		fRatioBestMin,

		fRatioBest_ByRatioOf_2FeasMin,

		fOne_SelectedCoocFea,
		fRatioBestArr[nDimSelecMax];
////////////////////////////////////////////////////////////
//https://docs.microsoft.com/en-us/cpp/cpp/data-type-ranges?view=vs-2019
	//printf("\n\n LONG_MAX = %lld", (long long)(LONG_MAX)); printf("\n\n Please press any key to exit"); getchar(); exit(1);
	//printf("\n\n (float)(LLONG_MAX) = %E", (float)(LLONG_MAX)); printf("\n\n Please press any key to exit"); getchar(); exit(1);

	//////////////////////////////////////////////////////////////
#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	if (fDimOfTripleCoocOfOneImagef >= (float)(INT_MAX))
	{
		printf("\n\n An error: fDimOfTripleCoocOfOneImagef = %E >= (float)(INT_MAX) = %E", fDimOfTripleCoocOfOneImagef, (float)(INT_MAX));
		printf("\n\n Please decrease the number of feas and/or vecs");
		printf("\n\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fDimOfTripleCoocOfOneImagef >= (float)(INT_MAX))

	printf("\nnNumOfTripleCoocFeasOf_ImageTot = %d", nNumOfTripleCoocFeasOf_ImageTot);
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY


#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		float *fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf = new float[nNumOfTripleCoocFeasOf_ImageTot];

		if (fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf == nullptr)
		{
			printf("\n\n An error in dynamic memory allocation for 'fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf' in 'main'");
			printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

			delete[] nFeaTheSameOrNot_FinArr;
			return UNSUCCESSFUL_RETURN;
		} //if (fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf == nullptr)

		float *fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
		fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf = new float[nNumOfTripleCoocFeasOf_ImageTot];

		if (fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf == nullptr)
		{
			printf("\n\n An error in dynamic memory allocation for 'fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf' in 'main'");
			printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

			delete[] nFeaTheSameOrNot_FinArr;
			delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;

			return UNSUCCESSFUL_RETURN;
		} //if (fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf == nullptr)

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		//////////////////////////////////////////////////////

		//fout_PrintFeatures = fopen("wVecsOfFeas_397Norm_76Malig.txt", "w");
	//	fout_PrintFeatures = fopen("wVecsOfFeas_3TheSameTrainNormal.txt", "w");
		//fout_PrintFeatures = fopen("wVecsOfFeas_Test.txt", "w");
		//fout_PrintFeatures = fopen("wVecsOfFeas_337Norm_84Malig_HighFreq_train.txt", "w");

		//fout_PrintFeatures = fopen("wVecsOfFeas_Victors_5TestImages.txt", "w");
		//fout_PrintFeatures = fopen("wVecsOfFeas_Annass_5TestImages.txt", "w");

		//fout_PrintFeatures = fopen("wVecsOfFeas_337Norm_84Malig_HighFreq_test.txt", "w");
	//	fout_PrintFeatures = fopen("wVecsOfFeas_Fea_11415.txt", "w");
	//	fout_PrintFeatures = fopen("wVecsOfFeas_Fea_11411_6875.txt", "w");
	////////////////////////////////////////////////////////////////////
		//fout_PrintFeatures = fopen("wVecsOfFeasSpleen_Normal313_Malignant576.txt", "w");
		//fout_PrintFeatures = fopen("wVecsOfFeasSpleen_Benign50_Malignant50_NoHistEqual.txt", "w");

		//fout_PrintFeatures = fopen("wVecsOfFeasSpleen_Benign472_Malignant575.txt", "w");
		//fout_PrintFeatures = fopen("wVecsSpleen_Ben50_Mal50_NoHistEqual.txt", "w");
		//fout_PrintFeatures = fopen("wVecsSpleen_Ben50_Mal50_WithHistEqual.txt", "w");

		//fout_PrintFeatures = fopen("wVecsSpleen_CustomProces_Ben472_Mal575.txt", "w");
		fout_PrintFeatures = fopen("wVecsWoodyBreast_298_501_Cooc2.txt", "w");

	//fout_PrintFeatures = fopen("wVecsSpleen_Ben_Mal_472_575_Cooc2.txt", "w");
	//	fout_PrintFeatures = fopen("wVecsSpleen_Ben_Mal_50_50_GaborDirectional.txt", "w");
		//fout_PrintFeatures = fopen("wVecsSpleen_Ben_Mal_50_50_CoocExtended.txt", "w");
		//fout_PrintFeatures = fopen("wVecsSpleen_Test_Wr1.txt", "w");

	if (fout_PrintFeatures == NULL)
	{
		printf("\n\n fout_PrintFeatures == NULL");
		getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		return UNSUCCESSFUL_RETURN;
	} //if (fout_PrintFeatures == NULL)
////////////////////

	nRes = PositionsOfMeaningfulFeasForRegularOrTripleCooc(

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
		nDim, //const int nDimf,
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		nNumOfTripleCoocFeasOf_ImageTot,
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		nNumVecsForPositionsOfMeaningfulFeasFinal, //const int nNumVecf, //nNumVecsForPositionsOfMeaningfulFeasForRegularOrTripleCooc == 3

		nNumOfFeasWithMeaningfulValues_Fin, //int &nNumOfFeasWithMeaningfulValues_Finf,

		nFeaTheSameOrNot_FinArr); // int nFeaTheSameOrNot_FinArrf[]);//[nDimf] // 1-- the same, 0-- not

	//nNumOfFeasWithMeaningfulValues_Fin = 50;
	//fOneDim_CoocExtendedArr[nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot];

		////////////////////////////////////////////////////////////////////////////////////////////////
	//printf("\n\n INT_MAX = %ld", INT_MAX); getchar();
#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	printf("\nnNumOfTripleCoocFeasOf_ImageTot = %d, nNumOfFeasWithMeaningfulValues_Fin = %d", 
		nNumOfTripleCoocFeasOf_ImageTot, nNumOfFeasWithMeaningfulValues_Fin);

	fprintf(fout_PrintFeatures,"\nnNumOfTripleCoocFeasOf_ImageTot = %d, nNumOfFeasWithMeaningfulValues_Fin = %d",
		nNumOfTripleCoocFeasOf_ImageTot, nNumOfFeasWithMeaningfulValues_Fin);

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

	if ((float)(nNumOfVecs_Normal) * (float)(nNumOfFeasWithMeaningfulValues_Fin) >= (float)(INT_MAX))
	{
		printf("\n\n An error in 'main': (float)(nNumOfVecs_Normal) * (float)(nNumOfFeasWithMeaningfulValues_Fin) = %E >= (float)(INT_MAX) = %E",
			(float)(nNumOfVecs_Normal) * (float)(nNumOfFeasWithMeaningfulValues_Fin), (float)(INT_MAX));

		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		return UNSUCCESSFUL_RETURN;

	} //if ((float)(nNumOfVecs_Normal) * (float)(nNumOfFeasWithMeaningfulValues_Fin) >= (float)(INT_MAX))

	if ((float)(nNumOfVecs_Malignant) * (float)(nNumOfFeasWithMeaningfulValues_Fin) >= (float)(INT_MAX))
	{
		printf("\n\n An error in 'main': (float)(nNumOfVecs_Malignant) * (float)(nNumOfFeasWithMeaningfulValues_Fin) = %E >= (float)(INT_MAX) = %E",
			(float)(nNumOfVecs_Malignant) * (float)(nNumOfFeasWithMeaningfulValues_Fin), (float)(INT_MAX));

		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		return UNSUCCESSFUL_RETURN;

	} //if ((float)(nNumOfVecs_Normal) * (float)(nNumOfFeasWithMeaningfulValues_Fin) >= (float)(INT_MAX))

////////////////////////////////////

	float *fActualFeasFor_OneNormalVec_CoocExtendedArr;
	fActualFeasFor_OneNormalVec_CoocExtendedArr = new float[nNumOfFeasWithMeaningfulValues_Fin];

	if (fActualFeasFor_OneNormalVec_CoocExtendedArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fActualFeasFor_OneNormalVec_CoocExtendedArr' in 'main'");

		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		return UNSUCCESSFUL_RETURN;
	} //if (fActualFeasFor_OneNormalVec_CoocExtendedArr == nullptr)

	float *fActualFeasFor_OneMalignantVec_CoocExtendedArr;
	fActualFeasFor_OneMalignantVec_CoocExtendedArr = new float[nNumOfFeasWithMeaningfulValues_Fin];

	if (fActualFeasFor_OneMalignantVec_CoocExtendedArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fActualFeasFor_OneMalignantVec_CoocExtendedArr' in 'main'");

		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		return UNSUCCESSFUL_RETURN;
	} //if (fActualFeasFor_OneMalignantVec_CoocExtendedArr == nullptr)

//////////////////////////////////////////////////////////////////////////////////
//	nNumOf_ActualFeas_ForAll_Normal_VecsTotf = nNumOfVecs_Normal * nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot;
	nNumOf_ActualFeas_ForAll_Normal_VecsTotf = nNumOfVecs_Normal * nNumOfFeasWithMeaningfulValues_Fin;

	float *fActualFeasForAll_NormalVecs_CoocExtendedArr;
	fActualFeasForAll_NormalVecs_CoocExtendedArr = new float[nNumOf_ActualFeas_ForAll_Normal_VecsTotf];

	if (fActualFeasForAll_NormalVecs_CoocExtendedArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fActualFeasForAll_NormalVecs_CoocExtendedArr' in 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fActualFeasForAll_NormalVecs_CoocExtendedArr' in 'main'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);
	
#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		return UNSUCCESSFUL_RETURN;
	} //if (fActualFeasForAll_NormalVecs_CoocExtendedArr == nullptr)

	//////////////////////////////
	float *fActualFeasForAll_NormalVecs_Copy_Arr;
	fActualFeasForAll_NormalVecs_Copy_Arr = new float[nNumOf_ActualFeas_ForAll_Normal_VecsTotf];

	if (fActualFeasForAll_NormalVecs_Copy_Arr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fActualFeasForAll_NormalVecs_Copy_Arr' in 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fActualFeasForAll_NormalVecs_Copy_Arr' in 'main'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		//fActualFeasForAll_NormalVecs_Copy_Arr
		delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;

		delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
		delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

		return UNSUCCESSFUL_RETURN;
	} //if (fActualFeasForAll_NormalVecs_Copy_Arr == nullptr)
//////////////////////////////////////////////////////////////////////////////////

	//nNumOf_CoocExtendedFeas_OneDimTot (nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot)
	//nNumOf_ActualFeas_ForAll_Malignant_VecsTotf = nNumOfVecs_Malignant * nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot;
	nNumOf_ActualFeas_ForAll_Malignant_VecsTotf = nNumOfVecs_Malignant * nNumOfFeasWithMeaningfulValues_Fin;

	float *fActualFeasForAll_MalignantVecs_CoocExtendedArr;
	fActualFeasForAll_MalignantVecs_CoocExtendedArr = new float[nNumOf_ActualFeas_ForAll_Malignant_VecsTotf];

	if (fActualFeasForAll_MalignantVecs_CoocExtendedArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fActualFeasForAll_MalignantVecs_CoocExtendedArr' in 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fActualFeasForAll_MalignantVecs_CoocExtendedArr' in 'main'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Press any key to exit");	fflush(fout_PrintFeatures); getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
		delete[] fActualFeasForAll_NormalVecs_Copy_Arr;

		delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
		delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

		return UNSUCCESSFUL_RETURN;
	} //if (fActualFeasForAll_MalignantVecs_CoocExtendedArr == nullptr)
///////////////////////////////////////////////////////////////

	float *fActualFeasForAll_MalignantVecs_Copy_Arr;
	fActualFeasForAll_MalignantVecs_Copy_Arr = new float[nNumOf_ActualFeas_ForAll_Malignant_VecsTotf];

	if (fActualFeasForAll_MalignantVecs_Copy_Arr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fActualFeasForAll_MalignantVecs_Copy_Arr' in 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fActualFeasForAll_MalignantVecs_Copy_Arr' in 'main'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
		delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

		delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
		delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

		delete[] fActualFeasForAll_NormalVecs_Copy_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (fActualFeasForAll_MalignantVecs_Copy_Arr == nullptr)
//////////////////////////////////////////////////////////////////
//#endif // #ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED

	printf("\n\n Orig: nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot = %d, actual: nNumOfFeasWithMeaningfulValues_Fin = %d",
		nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot, nNumOfFeasWithMeaningfulValues_Fin);

	fprintf(fout_PrintFeatures, "\n\n Orig: nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot = %d, actual: nNumOfFeasWithMeaningfulValues_Fin = %d",
		nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot, nNumOfFeasWithMeaningfulValues_Fin);

	//printf("\n\n Please press any key to continue"); 	fflush(fout_PrintFeatures);	getchar();

	///////////////////////////////////////////////////////////////////////////////////////
	Image image_in;

	char cFileName_iinDirectoryOf_Normal_Images[150];
	char cFileName_iinDirectoryOf_Malignant_Images[150];
	//image_in.read("output_ForMultifractal.png");
/////////////////////////////////////////////////////////////
	DIR *pDIR;
	struct dirent *entry;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//313
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Normal\\AOI\\"; //copy to if (pDIR = opendir(.. as well

		//472 //477; benign as normal
//	char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\"; //copy to if (pDIR = opendir(.. as well

	//473 //477; benign as normal //D:\Imago\Images\spleen\Genas_Images_June26\2_Processes_AOI_Mal_Ben\Custom\Benign_Custom
	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\Custom\\Benign_Custom\\"; //copy to if (pDIR = opendir(.. as well

 //check out '#define nNumOfVecs_Normal'!
//50
	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI_50_Selec\\"; //copy to if (pDIR = opendir(.. as well

	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AoI_2_Test\\"; //copy to if (pDIR = opendir(.. as well
//////////////////////////////////////////
//woody breast
//501
	char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\WoodyBreast\\AOIs\\Normal-Panoramic\\"; //copy to if (pDIR = opendir(.. as well
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//check out '#define nNumOfVecs_Malignant'!
//575 //576
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\"; //copy to if (pDIR = opendir(.. as well

	//D:\Imago\Images\spleen\Genas_Images_June26\2_Processes_AOI_Mal_Ben\Custom\Malignant_Custom
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\Custom\\Malignant_Custom\\"; //copy to if (pDIR = opendir(.. as well
//50
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI_50_Selec\\"; //copy to if (pDIR = opendir(.. as well
////////////////////////////////////
//woody breast
//298
	char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\WoodyBreast\\AOIs\\WB-Panoramic\\";
//////////////////////////////////////////////////////////////
	COLOR_IMAGE sColor_Image; //

/////////////////////////////////////////////////////
//Normal 
	//////////////////////////////////////////////////////////////
	//397  -- change 'nNumOfVecs_Normal' 
			//10 vecs only
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Normal_Tot_Small\\"))
	///////////////////////////////////////////////////////////////////////////
//#ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED

			//printf("\n\n Before normal images: please press any key"); fflush(fout_PrintFeatures); getchar();
	nNumOfProcessedFiles_NormalTot = 0;
	//check out '#define nNumOfVecs_Normal'!
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\"))
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI_50_Selec\\"))
		   //if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\Custom\\Benign_Custom\\"))
	if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\AOIs\\Normal-Panoramic\\"))
	{
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_NormalTot += 1;

				nNumOfProcessedFiles_NormalTot_Glob = nNumOfProcessedFiles_NormalTot;

				if (nNumOfProcessedFiles_NormalTot > nNumOfVecs_Normal)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_NormalTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_NormalTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNot_FinArr;
					delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
					delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_NormalTot > nNumOfVecs_Normal)

//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n %s\n", entry->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_iinDirectoryOf_Normal_Images, '\0', sizeof(cFileName_iinDirectoryOf_Normal_Images));

				strcpy(cFileName_iinDirectoryOf_Normal_Images, cInput_DirectoryOf_Normal_Images);

				strcat(cFileName_iinDirectoryOf_Normal_Images, entry->d_name);

				printf("\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);
				//fprintf(fout_PrintFeatures,"\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);
////////////////////////////////////////
				image_in.read(cFileName_iinDirectoryOf_Normal_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n Concatenated input file: %s\n", cFileName_iinDirectoryOf_Normal_Images);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n Press any key 1:");	fflush(fout_lr);  getchar();
//////////////////////////////////////////////////////////////

				nRes = Reading_AColorImage(
					image_in, //const Image& image_in,

					&sColor_Image); // COLOR_IMAGE *sColor_Image);

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'Reading_AColorImage' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNot_FinArr;
					delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
					delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)
				printf("\n\n So far nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);

#ifdef USING_HISTOGRAM_EQUALIZATION

#ifdef PRINT_HISTOGRAM_STATISTICS
				nRes = Histogram_Statistics_ColorActuallyGrayscale_Image(

					//const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageAreaMax]
					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					fPercentsOfHistogramIntervalsArrf, //float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

					fPercentagesAboveThresholds_InHistogramArrf, //float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

					fMeanOfHistf, //float &fMeanf,
					fStDevOfHistf, //float &fStDevf,
					fSkewnessOfHistf, //float &fSkewnessf,
					fKurtosisfOfHistf); // float &fKurtosisf);

				printf("\n\n Histogram statistics before 'HistogramEqualization': fMeanOfHistf = %E, fStDevOfHistf = %E", fMeanOfHistf, fStDevOfHistf);

				for (iHistf = 0; iHistf < nNumOfHistogramIntervals_ForCooc; iHistf++)
				{
					printf("\n Before: fPercentsOfHistogramIntervalsArrf[%d] = %E", iHistf, fPercentsOfHistogramIntervalsArrf[iHistf]);
				} //for (iHistf = 0; iHistf < nNumOfHistogramIntervals_ForCooc; iHistf++)

#endif //#ifdef PRINT_HISTOGRAM_STATISTICS

				nRes = HistogramEqualization_ForColorFormatOfGray(

					//GRAYSCALE_IMAGE *sGrayscale_Image)
					&sColor_Image);// COLOR_IMAGE *sColor_Imagef);

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'HistogramEqualization_ForColorFormatOfGray' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

#ifdef PRINT_HISTOGRAM_STATISTICS

				nRes = Histogram_Statistics_ColorActuallyGrayscale_Image(

					//const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageAreaMax]
					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					fPercentsOfHistogramIntervalsArrf, //float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

					fPercentagesAboveThresholds_InHistogramArrf, //float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

					fMeanOfHistf, //float &fMeanf,
					fStDevOfHistf, //float &fStDevf,
					fSkewnessOfHistf, //float &fSkewnessf,
					fKurtosisfOfHistf); // float &fKurtosisf);

				printf("\n\n Histogram statistics after 'HistogramEqualization': fMeanOfHistf = %E, fStDevOfHistf = %E", fMeanOfHistf, fStDevOfHistf);

				for (iHistf = 0; iHistf < nNumOfHistogramIntervals_ForCooc; iHistf++)
				{
					printf("\n After: fPercentsOfHistogramIntervalsArrf[%d] = %E", iHistf, fPercentsOfHistogramIntervalsArrf[iHistf]);
				} //for (iHistf = 0; iHistf < nNumOfHistogramIntervals_ForCooc; iHistf++)

				printf("\n\n Press any key to ontinue");	getchar();

#endif //#ifdef PRINT_HISTOGRAM_STATISTICS

#endif //#ifdef USING_HISTOGRAM_EQUALIZATION

				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef USING_TRIPLE_COOCURRENCE_ONLY

				nRes = CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess(
					//the image is supposed to be grayscale
					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					nDistToCoocPointsMin, //const int nDistToCoocPointsMinf,
					nDistToCoocPointsMax, //const int nDistToCoocPointsMaxf,

					fCoocFeasOf_Nor_Image_By_LenWidShift_At_AllDist_Arrf); // float fCoocFeasOf_Nor_Image_By_LenWidShift_At_AllDist_Arrf[])
					//4*nCoocSizeMax*[(nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*( nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1)]/2

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
				nRes = TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess(
					//the image is supposed to be grayscale
					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					nDistToCoocPointsMin, //const int nDistToCoocPointsMinf,
					nDistToCoocPointsMax, //const int nDistToCoocPointsMaxf,

					fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf); // float fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf[]);// [nNumOfTripleCoocFeasOf_ImageTot]

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					delete[] nFeaTheSameOrNot_FinArr;
					delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
					delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

////////////////////////////////////////

				nRes = Converting_OneFeaVec_ToAVecWithSelecFeas(

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
					nDim, //const int nDimf,
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					nNumOfTripleCoocFeasOf_ImageTot,
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					nNumOfFeasWithMeaningfulValues_Fin, //const int nDim_2f, // == nNumOfFeasWithMeaningfulValues_Finf < nDim_1f

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
					fCoocFeasOf_Nor_Image_By_LenWidShift_At_AllDist_Arrf, //const float f_1Arrf[], //[nDim_1f]
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf,
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					nFeaTheSameOrNot_FinArr, //const int nFeaTheSameOrNot_FinArrf[], //[nDim_1f]

					fActualFeasFor_OneNormalVec_CoocExtendedArr); // float f_SelecArrf[]); //[nDim_2f] //fActualFeasFor_OneNormalVec_CoocExtendedArr[]

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'Converting_OneFeaVec_ToAVecWithSelecFeas' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNot_FinArr;
					delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
					delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

						//////////////////////////////////////////////
				//nTempf = (nNumOfProcessedFiles_NormalTot - 1)* nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot;
				nTempf = (nNumOfProcessedFiles_NormalTot - 1)* nNumOfFeasWithMeaningfulValues_Fin;

				//for (iFea = 0; iFea < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFea++)
				for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
				{
					nIndex = iFea + nTempf;
					if (nIndex < 0 || nIndex >= nNumOf_ActualFeas_ForAll_Normal_VecsTotf)
					{
						printf("\n\n An error in 'main' (normal): nIndex = %d >= nNumOf_ActualFeas_ForAll_Normal_VecsTotf = %d", nIndex, nNumOf_ActualFeas_ForAll_Normal_VecsTotf);

						printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n An error in 'main': nIndex = %d >= nNumOf_ActualFeas_ForAll_Normal_VecsTotf = %d", nIndex, nNumOf_ActualFeas_ForAll_Normal_VecsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
						delete[] nFeaTheSameOrNot_FinArr;
						delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
						delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

						delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
						delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

						delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
						delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

						delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
						delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

						return UNSUCCESSFUL_RETURN; //
					} //if (nIndex < 0 || nIndex >= nNumOf_ActualFeas_ForAll_Normal_VecsTotf)

					fActualFeasForAll_NormalVecs_CoocExtendedArr[nIndex] = fActualFeasFor_OneNormalVec_CoocExtendedArr[iFea];
				} //for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
		////////////////////////////////////////////////

				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTot = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTot);  getchar(); //exit(1);
				printf("\n Normal: the end of nNumOfProcessedFiles_NormalTot = %d, nNumOfVecs_Normal = %d; going to to the next image", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);

				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR))

		closedir(pDIR);
	}//if (pDIR = opendir("C:\\Images_WoodyBreast_Normal\\") //C:\\Malignant_cases\\"))

	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n An error: the directory of normal images can not be opened");

		printf("\n\n Press any key to exit"); fflush(fout_PrintFeatures);	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
		delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

		delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
		delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

		delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
		delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

		perror("");
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

//printf("\n\n The end of normal inages: press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);
	//printf("\n\n The end of normal images: press any key to continue to the malignant ones");	fflush(fout_PrintFeatures);  getchar();

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//Malignant 

//check out '#define nNumOfVecs_Malignant'!
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\"))
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI_50_Selec\\"))

		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\Custom\\Malignant_Custom\\"))
	if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\AOIs\\WB-Panoramic\\"))
	{
		nNumOfProcessedFiles_MalignantTot = 0;
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_MalignantTot += 1;

				if (nNumOfProcessedFiles_MalignantTot > nNumOfVecs_Malignant)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_MalignantTot = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_MalignantTot = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNot_FinArr;
					delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
					delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_MalignantTot > nNumOfVecs_Malignant)

				//Initializing_AFloatVec_To_Zero(
					//nNumOf_CoocExtendedFeas_OneDimTot, //const int nDimf,
					//fOneDim_CoocExtendedArr); // float fVecArrf[]); //[nDimf]

//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n %s\n", entry->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_iinDirectoryOf_Malignant_Images, '\0', sizeof(cFileName_iinDirectoryOf_Malignant_Images));

				strcpy(cFileName_iinDirectoryOf_Malignant_Images, cInput_DirectoryOf_Malignant_Images);

				strcat(cFileName_iinDirectoryOf_Malignant_Images, entry->d_name);

				printf("\n Concatenated input file: %s, nNumOfProcessedFiles_MalignantTot = %d\n", cFileName_iinDirectoryOf_Malignant_Images, nNumOfProcessedFiles_MalignantTot);

				//fprintf(fout_PrintFeatures, "\n\n Concatenated input file: %s\n", cFileName_iinDirectoryOf_Malignant_Images);

				//printf("\n\n Press any key 1:");	fflush(fout_PrintFeatures);  getchar();
////////////////////////////////////////////////
				image_in.read(cFileName_iinDirectoryOf_Malignant_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After reading the input image");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//#ifndef COMMENT_OUT_ALL_PRINTS

				nRes = Reading_AColorImage(
					image_in, //const Image& image_in,

					&sColor_Image); // COLOR_IMAGE *sColor_Image);

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'Reading_AColorImage' 2");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNot_FinArr;
					delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
					delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

#ifdef USING_HISTOGRAM_EQUALIZATION
				nRes = HistogramEqualization_ForColorFormatOfGray(

					//GRAYSCALE_IMAGE *sGrayscale_Image)
					&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'HistogramEqualization_ForColorFormatOfGray' 2");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

#endif //#ifdef USING_HISTOGRAM_EQUALIZATION

				printf("\n\n So far nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
				nRes = CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess(
					//the image is supposed to be grayscale
					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					nDistToCoocPointsMin, //const int nDistToCoocPointsMinf,
					nDistToCoocPointsMax, //const int nDistToCoocPointsMaxf,

					fCoocFeasOf_Mal_Image_By_LenWidShift_At_AllDist_Arrf); // float fCoocFeasOf_Image_By_LenWidShift_At_AllDist_Arrf[])
					//4*nCoocSizeMax*[(nDistToCoocPointsMinf + nDistToCoocPointsMaxf)*( nDistToCoocPointsMaxf - nDistToCoocPointsMinf + 1)]/2

				//////////////////////////////////////////////
				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess' 2");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
				nRes = TripleCoocOf_Image_By_All_LenWidShifts_At_AllDistancess(
					//the image is supposed to be grayscale
					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					nDistToCoocPointsMin, //const int nDistToCoocPointsMinf,
					nDistToCoocPointsMax, //const int nDistToCoocPointsMaxf,

					fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf); // float fTripleCoocOf_Image_By_All_LenWidShift_At_AllDist_Arrf[]);// [nNumOfTripleCoocFeasOf_ImageTot]

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'CoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancess' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					delete[] nFeaTheSameOrNot_FinArr;
					delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
					delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

///////////////////////////////////////////////////////////

				nRes = Converting_OneFeaVec_ToAVecWithSelecFeas(

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
					nDim, //const int nDimf,
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					nNumOfTripleCoocFeasOf_ImageTot,
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					nNumOfFeasWithMeaningfulValues_Fin, //const int nDim_2f, // == nNumOfFeasWithMeaningfulValues_Finf < nDim_1f

#ifndef USING_TRIPLE_COOCURRENCE_ONLY
					fCoocFeasOf_Nor_Image_By_LenWidShift_At_AllDist_Arrf, //const float f_1Arrf[], //[nDim_1f]
#endif //#ifndef USING_TRIPLE_COOCURRENCE_ONLY

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf,
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					nFeaTheSameOrNot_FinArr, //const int nFeaTheSameOrNot_FinArrf[], //[nDim_1f]

					fActualFeasFor_OneMalignantVec_CoocExtendedArr); // float f_SelecArrf[]); //[nDim_2f] //fActualFeasFor_OneNormalVec_CoocExtendedArr[]

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'Converting_OneFeaVec_ToAVecWithSelecFeas' 2");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
					delete[] nFeaTheSameOrNot_FinArr;
					delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
					delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

					delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
					delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

					delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
					delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

					delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
					delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

						//////////////////////////////////////////////
				//nTempf = (nNumOfProcessedFiles_MalignantTot - 1)* nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot;
				nTempf = (nNumOfProcessedFiles_MalignantTot - 1)* nNumOfFeasWithMeaningfulValues_Fin;

				//for (iFea = 0; iFea < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFea++)
				for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
				{
					nIndex = iFea + nTempf;
					if (nIndex < 0 || nIndex >= nNumOf_ActualFeas_ForAll_Malignant_VecsTotf)
					{
						printf("\n\n An error in 'main' (malignant): nIndex = %d >= nNumOf_ActualFeas_ForAll_Normal_VecsTotf = %d", nIndex, nNumOf_ActualFeas_ForAll_Normal_VecsTotf);

						printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
						delete[] nFeaTheSameOrNot_FinArr;
						delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
						delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

						delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
						delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

						delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
						delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

						delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
						delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

						return UNSUCCESSFUL_RETURN; //
					} //if (nIndex < 0 || nIndex >= nNumOf_ActualFeas_ForAll_Malignant_VecsTotf)

					fActualFeasForAll_MalignantVecs_CoocExtendedArr[nIndex] = fActualFeasFor_OneMalignantVec_CoocExtendedArr[iFea];
				} //for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)				////////////////////////////////////////////////

				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTot = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTot);  getchar(); //exit(1);
				printf("\n Malignant: the end of nNumOfProcessedFiles_MalignantTot = %d, nNumOfVecs_Malignant = %d; going to to the next image",
					nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);

				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR))

		closedir(pDIR);
	}//if (pDIR = opendir("C:\\Images_WoodyBreast_Malignant\\") //C:\\Malignant_cases\\"))
	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n The directory of malignant images can not be opened");

		printf("\n\n Press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
		delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

		delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
		delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

		delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
		delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

		//perror("");
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

////////////////////////////////////////////////////////////////////

#ifdef PRINT_ORIGINAL_FEAS
	fprintf(fout_PrintFeatures, "\n\n///////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Original feas: normal images");
	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n Normal (actual):  iVec = %d, ", iVec);
		fprintf(fout_PrintFeatures, "\n1, ");
		nTempf = iVec * nNumOfFeasWithMeaningfulValues_Fin;

		for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fActualFeasForAll_NormalVecs_CoocExtendedArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)

	fprintf(fout_PrintFeatures, "\n\n Original feas: images with malignancies");

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n Malig (actual):  iVec = %d, ", iVec);;

		fprintf(fout_PrintFeatures, "\n0, ");

		nTempf = iVec * nNumOfFeasWithMeaningfulValues_Fin;
		for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fActualFeasForAll_MalignantVecs_CoocExtendedArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
#endif //#ifdef PRINT_ORIGINAL_FEAS
		/////////////////////////////////////////////////////////////////////////

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n Copying malig (actualactualactual):  iVec = %d, ", iVec);;

		//nTempf = iVec * nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot;
		nTempf = iVec * nNumOfFeasWithMeaningfulValues_Fin;

		//for (iFea = 0; iFea < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFea++)
		for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fActualFeasForAll_NormalVecs_CoocExtendedArr[nIndex];
			//fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

			fActualFeasForAll_NormalVecs_Copy_Arr[nIndex] = fFeaCur;
		}//for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
/////////////////////////////////////////////////////////////////////////

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n Copying malig (actual):  iVec = %d, ", iVec);;

	//	nTempf = iVec * nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot;
		nTempf = iVec * nNumOfFeasWithMeaningfulValues_Fin;

		//for (iFea = 0; iFea < nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot; iFea++)
		for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fActualFeasForAll_MalignantVecs_CoocExtendedArr[nIndex];
			//fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

			fActualFeasForAll_MalignantVecs_Copy_Arr[nIndex] = fFeaCur;
		}//for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)

//////////////////////////////////////////////////////////////////////////////////////////////////
/*// A selected fea

	fprintf(fout_PrintFeatures, "\n\n///////////////////////////////////////////////////////////////////////////");

	fprintf(fout_PrintFeatures, "\n\n Original fea for nPositionOf_TotFeaToPrint = %d (normal)", nPositionOf_TotFeaToPrint);

	iFea = nPositionOf_TotFeaToPrint;

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n Normal (actual) for nPositionOf_TotFeaToPrint = %d:  iVec = %d, ", nPositionOf_TotFeaToPrint, iVec);

		nTempf = iVec * nNumOfFeasWithMeaningfulValues_Fin;

		//for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fActualFeasForAll_NormalVecs_CoocExtendedArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)

	fprintf(fout_PrintFeatures, "\n\n Original fea for nPositionOf_TotFeaToPrint = %d (malignant)", nPositionOf_TotFeaToPrint);

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n Malignant (actual) for nPositionOf_TotFeaToPrint = %d:  iVec = %d, ", nPositionOf_TotFeaToPrint,iVec);

		nTempf = iVec * nNumOfFeasWithMeaningfulValues_Fin;
		//for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fActualFeasForAll_MalignantVecs_CoocExtendedArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOfFeasWithMeaningfulValues_Fin; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
*/

//printf("\n\n The end of actual feas for nPositionOf_TotFeaToPrint = %d, nNumOfProcessedFiles_MalignantTot = %d",
	//nPositionOf_TotFeaToPrint, nNumOfProcessedFiles_MalignantTot);  //getchar(); //exit(1);

	printf("\n\n nNumOfFeasWithMeaningfulValues_Fin = %d, nDimSelecMax = %d",
		nNumOfFeasWithMeaningfulValues_Fin, nDimSelecMax);

	printf("\n\n Before 'ConvertingVecs_ToBest_SeparableFeas', nNumOfProcessedFiles_NormalTot = %d, nNumOfProcessedFiles_MalignantTot = %d",
		nNumOfProcessedFiles_NormalTot, nNumOfProcessedFiles_MalignantTot);

	fprintf(fout_PrintFeatures, "\n\n nNumOfFeasWithMeaningfulValues_Fin = %d, nDimSelecMax = %d",
		nNumOfFeasWithMeaningfulValues_Fin, nDimSelecMax);

	fprintf(fout_PrintFeatures, "\n\n Before 'ConvertingVecs_ToBest_SeparableFeas', nNumOfProcessedFiles_NormalTot = %d, nNumOfProcessedFiles_MalignantTot = %d",
		nNumOfProcessedFiles_NormalTot, nNumOfProcessedFiles_MalignantTot);
	fflush(fout_PrintFeatures);

	//printf("\n\n Please press any key:"); getchar();

	///////////////////////////////////////////////////////////////////////////
	nRes = ConvertingVecs_ToBest_SeparableFeas(
		fLarge, //const float fLargef,
		feps, //const  float fepsf,

		//nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot, //const int nDimf,
		nNumOfFeasWithMeaningfulValues_Fin,
		nDimSelecMax, //const int nDimSelecMaxf, //20

		nNumOfVecs_Normal, //const int nNumVec1stf,
		nNumOfVecs_Malignant, //const int nNumVec2ndf,

		fPrecisionOf_G_Const_Search, //const float fPrecisionOf_G_Const_Searchf,

		0.0, //const  float fBorderBelf,
		1.0, //const  float fBorderAbof,

		nNumIterMaxForFindingBestSeparByRatioOfOneFea, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		fEfficBestSeparOfOneFeaAcceptMax, //const float fEfficBestSeparOfOneFeaAcceptMaxf,

		fActualFeasForAll_NormalVecs_CoocExtendedArr, //float fArr1stf[], //[nNumOfFeasWithMeaningfulValues_Fin*nNumVec1stf] // will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
		fActualFeasForAll_MalignantVecs_CoocExtendedArr, //float fArr2ndf[], //[nNumOfFeasWithMeaningfulValues_Fin*nNumVec2ndf] //will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	///////////////////////////////////

		nDimSelec, //int &nDimSelecf,

		fAll_SelecFeasForAll_NormalVecs_Arr, //float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
		fAll_SelecFeasForAll_MalignantVecs_Arr, //float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

		fRatioBestMin, //float &fRatioBestMinf,

		nPosOneFeaBestMax, //int &nPosOneFeaBestMaxf,

		nSeparDirectionBestMax, //int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		fNumOf_NormalVecsSeparBestMax, //float &fNumArr1stSeparBestMaxf,
		fNumOf_MalignantVecsSeparBestMax, //float &fNumArr2ndSeparBestMaxf,

		fRatioBestArr, //float fRatioBestArrf[], //nDimSelecMaxf

		nPosFeaSeparBestArr); // int nPosFeaSeparBestArrf[])	//nDimSelecMaxf

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'main' for 'ConvertingVecs_ToBest_SeparableFeas' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'main' for 'ConvertingVecs_ToBest_SeparableFeas' ");
		//printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
		delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

		delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
		delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

		delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
		delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

		printf("\n\n Press any key to exit");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (nRes == UNSUCCESSFUL_RETURN)
	///////////////////////////////////////////////////////////////////

	printf("\n\n After 'ConvertingVecs_ToBest_SeparableFeas', fRatioBestMin = %E", fRatioBestMin);
	fprintf(fout_PrintFeatures, "\n\n After 'ConvertingVecs_ToBest_SeparableFeas', fRatioBestMin = %E", fRatioBestMin);
	fflush(fout_PrintFeatures);

	printf("\n\n Please press any key:"); getchar();

#ifdef PRINT_BEST_FEAS
	fprintf(fout_PrintFeatures, "\n\n///////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Best feas (normalized) according to separability: normal");
	printf("\n\n Best feas (normalized) according to separability: normal");

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n1 (best feas, normalized):  iVec = %d, ", iVec);
		fprintf(fout_PrintFeatures, "\n1, ");

		nTempf = iVec * nDimSelecMax;
		for (iFea = 0; iFea < nDimSelecMax; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAll_SelecFeasForAll_NormalVecs_Arr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nDimSelecMax; iFea++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	/////////////////////////////////////////////////////////////////////////////////////////////

	fprintf(fout_PrintFeatures, "\n\n/////////////////////////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Best feas (actual) according to separability: normal");

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n1, ");

		nTempf = iVec * nNumOfFeasWithMeaningfulValues_Fin;
		for (iFea = 0; iFea < nDimSelecMax; iFea++)
		{
			nPosOfSelectedFeaCur = nPosFeaSeparBestArr[iFea];

			//if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot - 1)
			if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOfFeasWithMeaningfulValues_Fin - 1)
			{
				printf("\n\n An error in 'main' (normal cases): nPosOfSelectedFeaCur = %d, iFea = %d", nPosOfSelectedFeaCur, iFea);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'main' (malignant data): nPosOfSelectedFeaCur = %d, iFea = %d", nPosOfSelectedFeaCur, iFea);
				//printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Press any key to exit");	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
				delete[] nFeaTheSameOrNot_FinArr;
				delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
				delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

				delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
				delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

				delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
				delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

				delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
				delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

				return UNSUCCESSFUL_RETURN;
			} //if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOfFeasWithMeaningfulValues_Fin - 1)

			nIndex = nPosOfSelectedFeaCur + nTempf;

			fFeaCur = fActualFeasForAll_NormalVecs_Copy_Arr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nDimSelecMax; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)

	fprintf(fout_PrintFeatures, "\n\n/////////////////////////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Best feas (normalized) according to separability: malignant");
	printf("\n\n Best feas (normalized) according to separability: malignant");

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n2 (best feas, normalized ):  iVec = %d, ", iVec);
		fprintf(fout_PrintFeatures, "\n0, ");

		//nTempf = iVec * nNumOf_CoocExtendedFeas_OneDimTot;
		nTempf = iVec * nDimSelecMax;
		for (iFea = 0; iFea < nDimSelecMax; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAll_SelecFeasForAll_MalignantVecs_Arr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nDimSelecMax; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)

	fflush(fout_PrintFeatures);

	/////////////////////////////////////////////////////////////////////////////////////////////////
	fprintf(fout_PrintFeatures, "\n\n/////////////////////////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Best feas (actual) according to separability: malignant");

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n0, ");

		//nTempf = iVec * nNumOfCoocFeasOf_Image_By_All_LenWidShifts_At_AllDistancesTot;
		nTempf = iVec * nNumOfFeasWithMeaningfulValues_Fin;

		for (iFea = 0; iFea < nDimSelecMax; iFea++)
		{
			nPosOfSelectedFeaCur = nPosFeaSeparBestArr[iFea];
			if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOfFeasWithMeaningfulValues_Fin - 1)
			{
				printf("\n\n An error in 'main' (malignant cases): nPosOfSelectedFeaCur = %d, iFea = %d", nPosOfSelectedFeaCur, iFea);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'main' (malignant data): nPosOfSelectedFeaCur = %d, iFea = %d", nPosOfSelectedFeaCur, iFea);
				//printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Press any key to exit");	getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
				delete[] nFeaTheSameOrNot_FinArr;
				delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
				delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

				delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
				delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

				delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
				delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

				delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
				delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

				return UNSUCCESSFUL_RETURN;
			} //if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOfFeasWithMeaningfulValues_Fin - 1)

			nIndex = nPosOfSelectedFeaCur + nTempf;

			fFeaCur = fActualFeasForAll_MalignantVecs_Copy_Arr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nDimSelecMax; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)

#endif //#ifdef PRINT_BEST_FEAS

	printf("\n\n fRatioBestMin = %E", fRatioBestMin);
	fprintf(fout_PrintFeatures, "\n\n fRatioBestMin = %E", fRatioBestMin);
	fflush(fout_PrintFeatures);

#ifdef INCLUDE_OneBest_SeparableRatioOf_2Feas

	nRes = OneBest_SeparableRatioOf_2Feas(
		fLarge, //const float fLargef,
		feps, //const  float fepsf,

		nNumOfFeasWithMeaningfulValues_Fin, //const int nDimf,

		nNumOfVecs_Normal, //const int nNumVec1stf,
		nNumOfVecs_Malignant, //const int nNumVec2ndf,

		fPrecisionOf_G_Const_Search, //const float fPrecisionOf_G_Const_Searchf,

		0.0, //const  float fBorderBelf,
		1.0, //const  float fBorderAbof,

		nNumIterMaxForFindingBestSeparByRatioOfOneFea, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		fEfficBestSeparOfOneFeaAcceptMax, //const float fEfficBestSeparOfOneFeaAcceptMaxf,

		fActualFeasForAll_NormalVecs_CoocExtendedArr, //float fArr1stf[], //[nNumOfFeasWithMeaningfulValues_Fin*nNumVec1stf] // will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
		fActualFeasForAll_MalignantVecs_CoocExtendedArr, //float fArr2ndf[], //[nNumOfFeasWithMeaningfulValues_Fin*nNumVec2ndf] //will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
///////////////////////////////////

fRatioBest_ByRatioOf_2FeasMin); // float &fRatioBest_ByRatioOf_2FeasMinf);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'main' for 'OneBest_SeparableRatioOf_2Feas' ");

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
		delete[] nFeaTheSameOrNot_FinArr;
		delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
		delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

		delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
		delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

		delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
		delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

		delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
		delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

		printf("\n\n Press any key to exit");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (nRes == UNSUCCESSFUL_RETURN)

	printf("\n\n The end of 'OneBest_SeparableRatioOf_2Feas': fRatioBest_ByRatioOf_2FeasMin = %E", fRatioBest_ByRatioOf_2FeasMin);
	fprintf(fout_PrintFeatures, "\n\n The end of 'OneBest_SeparableRatioOf_2Feas': fRatioBest_ByRatioOf_2FeasMin = %E",
		fRatioBest_ByRatioOf_2FeasMin);
	fflush(fout_PrintFeatures);

#endif //#ifdef INCLUDE_OneBest_SeparableRatioOf_2Feas

	///////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fclose(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'main', nNumOfProcessedFiles_NormalTot = %d, nNumOfProcessedFiles_MalignantTot = %d, please press any key to exit",
		nNumOfProcessedFiles_NormalTot, nNumOfProcessedFiles_MalignantTot);
	fflush(fout_PrintFeatures);  getchar(); exit(1);

#ifdef USING_TRIPLE_COOCURRENCE_ONLY
	delete[] nFeaTheSameOrNot_FinArr;
	delete[] fTripleCoocOf_Nor_Image_By_All_LenWidShift_At_AllDist_Arrf;
	delete[] fTripleCoocOf_Mal_Image_By_All_LenWidShift_At_AllDist_Arrf;
#endif //#ifdef USING_TRIPLE_COOCURRENCE_ONLY

	delete[] fActualFeasForAll_NormalVecs_CoocExtendedArr;
	delete[] fActualFeasForAll_MalignantVecs_CoocExtendedArr;

	delete[] fActualFeasFor_OneNormalVec_CoocExtendedArr;
	delete[] fActualFeasFor_OneMalignantVec_CoocExtendedArr;

	delete[] fActualFeasForAll_NormalVecs_Copy_Arr;
	delete[] fActualFeasForAll_MalignantVecs_Copy_Arr;

	return SUCCESSFUL_RETURN;
} //int main()
/////////////////////////////////////
/////////////////////////////////////

//printf("\n\n Please press any key:"); getchar();